//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZeonPropertiesListing.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class L_Area_tb
    {
        public int AreaID { get; set; }
        public string AreaName { get; set; }
        public int StateID { get; set; }
    
        public virtual L_State_tb L_State_tb { get; set; }
    }
}
