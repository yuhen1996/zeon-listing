﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZeonPropertiesListing.Models;
namespace ZeonPropertiesListing.ViewModel
{
    public class vviewModel
    {
        //only for Edit(). Below are not in List form
        public L_ListingTbl listingTbl { get; set; }

        public List<L_AttachmentTbl> fileAttach;//Controller will return searched result frm DB, to here and display in cshtml
    }
}