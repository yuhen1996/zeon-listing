﻿! function (e, t, i) {
    "use strict";
    var o = t.documentElement,
        s = {
            create: function (e) {
                return t.createElement(e)
            },
            old: !!/(Android\s(1.|2.))|(Silk\/1.)/i.test(navigator.userAgent),
            pfx: function () {
                var e = t.createElement("dummy").style,
                    i = ["Webkit", "Moz", "O", "ms"],
                    o = {};
                return function (t) {
                    if (void 0 === o[t]) {
                        var s = t.charAt(0).toUpperCase() + t.substr(1),
                            n = (t + " " + i.join(s + " ") + s).split(" ");
                        o[t] = null;
                        for (var r in n)
                            if (void 0 !== e[n[r]]) {
                                o[t] = n[r];
                                break
                            }
                    }
                    return o[t]
                }
            }()
        },
        n = {
            css3Dtransform: function () {
                return !(s.old || null === s.pfx("perspective"))
            }(),
            cssTransform: function () {
                return !(s.old || null === s.pfx("transformOrigin"))
            }(),
            cssTransition: function () {
                return !(null === s.pfx("transition"))
            }(),
            cssFlexbox: function () {
                return !(null === s.pfx("flexBasis"))
            }(),
            addEventListener: !!e.addEventListener,
            querySelectorAll: !!t.querySelectorAll,
            matchMedia: !!e.matchMedia,
            deviceMotion: "DeviceMotionEvent" in e,
            deviceOrientation: "DeviceOrientationEvent" in e,
            contextMenu: "contextMenu" in o && "HTMLMenuItemElement" in e,
            classList: "classList" in o,
            placeholder: "placeholder" in s.create("input"),
            localStorage: function () {
                var e = "x";
                try {
                    return localStorage.setItem("x", "x"), localStorage.removeItem("x"), !0
                } catch (e) {
                    return !1
                }
            }(),
            historyAPI: e.history && "pushState" in e.history,
            serviceWorker: "serviceWorker" in navigator,
            viewportUnit: function (e) {
                try {
                    e.style.width = "1vw";
                    return !!("" !== e.style.width)
                } catch (e) {
                    return !1
                }
            }(s.create("dummy")),
            remUnit: function (e) {
                try {
                    e.style.width = "1rem";
                    return !!("" !== e.style.width)
                } catch (e) {
                    return !1
                }
            }(s.create("dummy")),
            canvas: function (e) {
                return !(!e.getContext || !e.getContext("2d"))
            }(s.create("canvas")),
            svg: !!t.createElementNS && !!t.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect,
            webGL: function (t) {
                try {
                    return !(!e.WebGLRenderingContext || !t.getContext("webgl") && !t.getContext("experimental-webgl"))
                } catch (e) {
                    return !1
                }
            }(s.create("canvas")),
            cors: "XMLHttpRequest" in e && "withCredentials" in new XMLHttpRequest,
            touch: !!("ontouchstart" in e || e.navigator && e.navigator.msPointerEnabled && e.MSGesture || e.DocumentTouch && t instanceof DocumentTouch),
            async: "async" in s.create("script"),
            defer: "defer" in s.create("script"),
            geolocation: "geolocation" in navigator,
            srcset: "srcset" in s.create("img"),
            sizes: "sizes" in s.create("img"),
            pictureElement: "HTMLPictureElement" in e,
            testAll: function () {
                var e = " js";
                for (var t in this) "testAll" !== t && "constructor" !== t && this[t] && (e += " " + t);
                o.className += e.toLowerCase()
            }
        };
    e.feature = n
}(window, document),
    function () {
        var e = [].indexOf || function (e) {
            for (var t = 0, i = this.length; t < i; t++)
                if (t in this && this[t] === e) return t;
            return -1
        },
            t = [].slice;
        ! function (e, t) {
            "function" == typeof define && define.amd ? define("waypoints", ["jquery"], function ($) {
                return t($, e)
            }) : t(e.jQuery, e)
        }(window, function ($, i) {
            var o, s, n, r, l, a, d, c, p, u, f, h, v, g, y, m;
            return o = $(i), c = e.call(i, "ontouchstart") >= 0, r = {
                horizontal: {},
                vertical: {}
            }, l = 1, d = {}, a = "waypoints-context-id", f = "resize.waypoints", h = "scroll.waypoints", v = 1, g = "waypoints-waypoint-ids", y = "waypoint", m = "waypoints", s = function () {
                function e(e) {
                    var t = this;
                    this.$element = e, this.element = e[0], this.didResize = !1, this.didScroll = !1, this.id = "context" + l++ , this.oldScroll = {
                        x: e.scrollLeft(),
                        y: e.scrollTop()
                    }, this.waypoints = {
                        horizontal: {},
                        vertical: {}
                    }, this.element[a] = this.id, d[this.id] = this, e.bind(h, function () {
                        var e;
                        if (!t.didScroll && !c) return t.didScroll = !0, e = function () {
                            return t.doScroll(), t.didScroll = !1
                        }, i.setTimeout(e, $[m].settings.scrollThrottle)
                    }), e.bind(f, function () {
                        var e;
                        if (!t.didResize) return t.didResize = !0, e = function () {
                            return $[m]("refresh"), t.didResize = !1
                        }, i.setTimeout(e, $[m].settings.resizeThrottle)
                    })
                }
                return e.prototype.doScroll = function () {
                    var e, t = this;
                    return e = {
                        horizontal: {
                            newScroll: this.$element.scrollLeft(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left"
                        },
                        vertical: {
                            newScroll: this.$element.scrollTop(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up"
                        }
                    }, !c || e.vertical.oldScroll && e.vertical.newScroll || $[m]("refresh"), $.each(e, function (e, i) {
                        var o, s, n;
                        return n = [], s = i.newScroll > i.oldScroll, o = s ? i.forward : i.backward, $.each(t.waypoints[e], function (e, t) {
                            var o, s;
                            return i.oldScroll < (o = t.offset) && o <= i.newScroll ? n.push(t) : i.newScroll < (s = t.offset) && s <= i.oldScroll ? n.push(t) : void 0
                        }), n.sort(function (e, t) {
                            return e.offset - t.offset
                        }), s || n.reverse(), $.each(n, function (e, t) {
                            if (t.options.continuous || e === n.length - 1) return t.trigger([o])
                        })
                    }), this.oldScroll = {
                        x: e.horizontal.newScroll,
                        y: e.vertical.newScroll
                    }
                }, e.prototype.refresh = function () {
                    var e, t, i, o = this;
                    return i = $.isWindow(this.element), t = this.$element.offset(), this.doScroll(), e = {
                        horizontal: {
                            contextOffset: i ? 0 : t.left,
                            contextScroll: i ? 0 : this.oldScroll.x,
                            contextDimension: this.$element.width(),
                            oldScroll: this.oldScroll.x,
                            forward: "right",
                            backward: "left",
                            offsetProp: "left"
                        },
                        vertical: {
                            contextOffset: i ? 0 : t.top,
                            contextScroll: i ? 0 : this.oldScroll.y,
                            contextDimension: i ? $[m]("viewportHeight") : this.$element.height(),
                            oldScroll: this.oldScroll.y,
                            forward: "down",
                            backward: "up",
                            offsetProp: "top"
                        }
                    }, $.each(e, function (e, t) {
                        return $.each(o.waypoints[e], function (e, i) {
                            var o, s, n, r, l;
                            if (o = i.options.offset, n = i.offset, s = $.isWindow(i.element) ? 0 : i.$element.offset()[t.offsetProp], $.isFunction(o) ? o = o.apply(i.element) : "string" == typeof o && (o = parseFloat(o), i.options.offset.indexOf("%") > -1 && (o = Math.ceil(t.contextDimension * o / 100))), i.offset = s - t.contextOffset + t.contextScroll - o, (!i.options.onlyOnScroll || null == n) && i.enabled) return null !== n && n < (r = t.oldScroll) && r <= i.offset ? i.trigger([t.backward]) : null !== n && n > (l = t.oldScroll) && l >= i.offset ? i.trigger([t.forward]) : null === n && t.oldScroll >= i.offset ? i.trigger([t.forward]) : void 0
                        })
                    })
                }, e.prototype.checkEmpty = function () {
                    if ($.isEmptyObject(this.waypoints.horizontal) && $.isEmptyObject(this.waypoints.vertical)) return this.$element.unbind([f, h].join(" ")), delete d[this.id]
                }, e
            }(), n = function () {
                function e(e, t, i) {
                    var o, s;
                    "bottom-in-view" === i.offset && (i.offset = function () {
                        var e;
                        return e = $[m]("viewportHeight"), $.isWindow(t.element) || (e = t.$element.height()), e - $(this).outerHeight()
                    }), this.$element = e, this.element = e[0], this.axis = i.horizontal ? "horizontal" : "vertical", this.callback = i.handler, this.context = t, this.enabled = i.enabled, this.id = "waypoints" + v++ , this.offset = null, this.options = i, t.waypoints[this.axis][this.id] = this, r[this.axis][this.id] = this, o = null != (s = this.element[g]) ? s : [], o.push(this.id), this.element[g] = o
                }
                return e.prototype.trigger = function (e) {
                    if (this.enabled) return null != this.callback && this.callback.apply(this.element, e), this.options.triggerOnce ? this.destroy() : void 0
                }, e.prototype.disable = function () {
                    return this.enabled = !1
                }, e.prototype.enable = function () {
                    return this.context.refresh(), this.enabled = !0
                }, e.prototype.destroy = function () {
                    return delete r[this.axis][this.id], delete this.context.waypoints[this.axis][this.id], this.context.checkEmpty()
                }, e.getWaypointsByElement = function (e) {
                    var t, i;
                    return (i = e[g]) ? (t = $.extend({}, r.horizontal, r.vertical), $.map(i, function (e) {
                        return t[e]
                    })) : []
                }, e
            }(), u = {
                init: function (e, t) {
                    var i;
                    return t = $.extend({}, $.fn[y].defaults, t), null == (i = t.handler) && (t.handler = e), this.each(function () {
                        var e, i, o, r;
                        return e = $(this), o = null != (r = t.context) ? r : $.fn[y].defaults.context, $.isWindow(o) || (o = e.closest(o)), o = $(o), i = d[o[0][a]], i || (i = new s(o)), new n(e, i, t)
                    }), $[m]("refresh"), this
                },
                disable: function () {
                    return u._invoke.call(this, "disable")
                },
                enable: function () {
                    return u._invoke.call(this, "enable")
                },
                destroy: function () {
                    return u._invoke.call(this, "destroy")
                },
                prev: function (e, t) {
                    return u._traverse.call(this, e, t, function (e, t, i) {
                        if (t > 0) return e.push(i[t - 1])
                    })
                },
                next: function (e, t) {
                    return u._traverse.call(this, e, t, function (e, t, i) {
                        if (t < i.length - 1) return e.push(i[t + 1])
                    })
                },
                _traverse: function (e, t, o) {
                    var s, n;
                    return null == e && (e = "vertical"), null == t && (t = i), n = p.aggregate(t), s = [], this.each(function () {
                        var t;
                        return t = $.inArray(this, n[e]), o(s, t, n[e])
                    }), this.pushStack(s)
                },
                _invoke: function (e) {
                    return this.each(function () {
                        var t;
                        return t = n.getWaypointsByElement(this), $.each(t, function (t, i) {
                            return i[e](), !0
                        })
                    }), this
                }
            }, $.fn[y] = function () {
                var e, i;
                return i = arguments[0], e = 2 <= arguments.length ? t.call(arguments, 1) : [], u[i] ? u[i].apply(this, e) : $.isFunction(i) ? u.init.apply(this, arguments) : $.isPlainObject(i) ? u.init.apply(this, [null, i]) : i ? $.error("The " + i + " method does not exist in jQuery Waypoints.") : $.error("jQuery Waypoints needs a callback function or handler option.")
            }, $.fn[y].defaults = {
                context: i,
                continuous: !0,
                enabled: !0,
                horizontal: !1,
                offset: 0,
                triggerOnce: !1
            }, p = {
                refresh: function () {
                    return $.each(d, function (e, t) {
                        return t.refresh()
                    })
                },
                viewportHeight: function () {
                    var e;
                    return null != (e = i.innerHeight) ? e : o.height()
                },
                aggregate: function (e) {
                    var t, i, o;
                    return t = r, e && (t = null != (o = d[$(e)[0][a]]) ? o.waypoints : void 0), t ? (i = {
                        horizontal: [],
                        vertical: []
                    }, $.each(i, function (e, o) {
                        return $.each(t[e], function (e, t) {
                            return o.push(t)
                        }), o.sort(function (e, t) {
                            return e.offset - t.offset
                        }), i[e] = $.map(o, function (e) {
                            return e.element
                        }), i[e] = $.unique(i[e])
                    }), i) : []
                },
                above: function (e) {
                    return null == e && (e = i), p._filter(e, "vertical", function (e, t) {
                        return t.offset <= e.oldScroll.y
                    })
                },
                below: function (e) {
                    return null == e && (e = i), p._filter(e, "vertical", function (e, t) {
                        return t.offset > e.oldScroll.y
                    })
                },
                left: function (e) {
                    return null == e && (e = i), p._filter(e, "horizontal", function (e, t) {
                        return t.offset <= e.oldScroll.x
                    })
                },
                right: function (e) {
                    return null == e && (e = i), p._filter(e, "horizontal", function (e, t) {
                        return t.offset > e.oldScroll.x
                    })
                },
                enable: function () {
                    return p._invoke("enable")
                },
                disable: function () {
                    return p._invoke("disable")
                },
                destroy: function () {
                    return p._invoke("destroy")
                },
                extendFn: function (e, t) {
                    return u[e] = t
                },
                _invoke: function (e) {
                    var t;
                    return t = $.extend({}, r.vertical, r.horizontal), $.each(t, function (t, i) {
                        return i[e](), !0
                    })
                },
                _filter: function (e, t, i) {
                    var o, s;
                    return (o = d[$(e)[0][a]]) ? (s = [], $.each(o.waypoints[t], function (e, t) {
                        if (i(o, t)) return s.push(t)
                    }), s.sort(function (e, t) {
                        return e.offset - t.offset
                    }), $.map(s, function (e) {
                        return e.element
                    })) : []
                }
            }, $[m] = function () {
                var e, i;
                return i = arguments[0], e = 2 <= arguments.length ? t.call(arguments, 1) : [], p[i] ? p[i].apply(null, e) : p.aggregate.call(null, i)
            }, $[m].settings = {
                resizeThrottle: 100,
                scrollThrottle: 30
            }, o.on("load.waypoints", function () {
                return $[m]("refresh")
            })
        })
    }.call(this),
    function (e) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function ($) {
        "use strict";
        var e = window.Slick || {};
        e = function () {
            function e(e, i) {
                var o = this,
                    s;
                o.defaults = {
                    accessibility: !0,
                    adaptiveHeight: !1,
                    appendArrows: $(e),
                    appendDots: $(e),
                    arrows: !0,
                    asNavFor: null,
                    prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                    nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                    autoplay: !1,
                    autoplaySpeed: 3e3,
                    centerMode: !1,
                    centerPadding: "50px",
                    cssEase: "ease",
                    customPaging: function (e, t) {
                        return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + (t + 1) + "</button>"
                    },
                    dots: !1,
                    dotsClass: "slick-dots",
                    draggable: !0,
                    easing: "linear",
                    edgeFriction: .35,
                    fade: !1,
                    focusOnSelect: !1,
                    infinite: !0,
                    initialSlide: 0,
                    lazyLoad: "ondemand",
                    mobileFirst: !1,
                    pauseOnHover: !0,
                    pauseOnDotsHover: !1,
                    respondTo: "window",
                    responsive: null,
                    rows: 1,
                    rtl: !1,
                    slide: "",
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 500,
                    swipe: !0,
                    swipeToSlide: !1,
                    touchMove: !0,
                    touchThreshold: 5,
                    useCSS: !0,
                    variableWidth: !1,
                    vertical: !1,
                    verticalSwiping: !1,
                    waitForAnimate: !0,
                    zIndex: 1e3
                }, o.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1,
                    unslicked: !1
                }, $.extend(o, o.initials), o.activeBreakpoint = null, o.animType = null, o.animProp = null, o.breakpoints = [], o.breakpointSettings = [], o.cssTransitions = !1, o.hidden = "hidden", o.paused = !1, o.positionProp = null, o.respondTo = null, o.rowCount = 1, o.shouldClick = !0, o.$slider = $(e), o.$slidesCache = null, o.transformType = null, o.transitionType = null, o.visibilityChange = "visibilitychange", o.windowWidth = 0, o.windowTimer = null, s = $(e).data("slick") || {}, o.options = $.extend({}, o.defaults, s, i), o.currentSlide = o.options.initialSlide, o.originalSettings = o.options, void 0 !== document.mozHidden ? (o.hidden = "mozHidden", o.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (o.hidden = "webkitHidden", o.visibilityChange = "webkitvisibilitychange"), o.autoPlay = $.proxy(o.autoPlay, o), o.autoPlayClear = $.proxy(o.autoPlayClear, o), o.changeSlide = $.proxy(o.changeSlide, o), o.clickHandler = $.proxy(o.clickHandler, o), o.selectHandler = $.proxy(o.selectHandler, o), o.setPosition = $.proxy(o.setPosition, o), o.swipeHandler = $.proxy(o.swipeHandler, o), o.dragHandler = $.proxy(o.dragHandler, o), o.keyHandler = $.proxy(o.keyHandler, o), o.autoPlayIterator = $.proxy(o.autoPlayIterator, o), o.instanceUid = t++ , o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, o.registerBreakpoints(), o.init(!0), o.checkResponsive(!0)
            }
            var t = 0;
            return e
        }(), e.prototype.addSlide = e.prototype.slickAdd = function (e, t, i) {
            var o = this;
            if ("boolean" == typeof t) i = t, t = null;
            else if (t < 0 || t >= o.slideCount) return !1;
            o.unload(), "number" == typeof t ? 0 === t && 0 === o.$slides.length ? $(e).appendTo(o.$slideTrack) : i ? $(e).insertBefore(o.$slides.eq(t)) : $(e).insertAfter(o.$slides.eq(t)) : !0 === i ? $(e).prependTo(o.$slideTrack) : $(e).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each(function (e, t) {
                $(t).attr("data-slick-index", e)
            }), o.$slidesCache = o.$slides, o.reinit()
        }, e.prototype.animateHeight = function () {
            var e = this;
            if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
                var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
                e.$list.animate({
                    height: t
                }, e.options.speed)
            }
        }, e.prototype.animateSlide = function (e, t) {
            var i = {},
                o = this;
            o.animateHeight(), !0 === o.options.rtl && !1 === o.options.vertical && (e = -e), !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
                left: e
            }, o.options.speed, o.options.easing, t) : o.$slideTrack.animate({
                top: e
            }, o.options.speed, o.options.easing, t) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft), $({
                animStart: o.currentLeft
            }).animate({
                animStart: e
            }, {
                    duration: o.options.speed,
                    easing: o.options.easing,
                    step: function (e) {
                        e = Math.ceil(e), !1 === o.options.vertical ? (i[o.animType] = "translate(" + e + "px, 0px)", o.$slideTrack.css(i)) : (i[o.animType] = "translate(0px," + e + "px)", o.$slideTrack.css(i))
                    },
                    complete: function () {
                        t && t.call()
                    }
                })) : (o.applyTransition(), e = Math.ceil(e), !1 === o.options.vertical ? i[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : i[o.animType] = "translate3d(0px," + e + "px, 0px)", o.$slideTrack.css(i), t && setTimeout(function () {
                    o.disableTransition(), t.call()
                }, o.options.speed))
        }, e.prototype.asNavFor = function (e) {
            var t = this,
                i = t.options.asNavFor;
            i && null !== i && (i = $(i).not(t.$slider)), null !== i && "object" == typeof i && i.each(function () {
                var t = $(this).slick("getSlick");
                t.unslicked || t.slideHandler(e, !0)
            })
        }, e.prototype.applyTransition = function (e) {
            var t = this,
                i = {};
            !1 === t.options.fade ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
        }, e.prototype.autoPlay = function () {
            var e = this;
            e.autoPlayTimer && clearInterval(e.autoPlayTimer), e.slideCount > e.options.slidesToShow && !0 !== e.paused && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
        }, e.prototype.autoPlayClear = function () {
            var e = this;
            e.autoPlayTimer && clearInterval(e.autoPlayTimer)
        }, e.prototype.autoPlayIterator = function () {
            var e = this;
            !1 === e.options.infinite ? 1 === e.direction ? (e.currentSlide + 1 === e.slideCount - 1 && (e.direction = 0), e.slideHandler(e.currentSlide + e.options.slidesToScroll)) : (e.currentSlide - 1 == 0 && (e.direction = 1), e.slideHandler(e.currentSlide - e.options.slidesToScroll)) : e.slideHandler(e.currentSlide + e.options.slidesToScroll)
        }, e.prototype.buildArrows = function () {
            var e = this;
            !0 === e.options.arrows && (e.$prevArrow = $(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = $(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
                "aria-disabled": "true",
                tabindex: "-1"
            }))
        }, e.prototype.buildDots = function () {
            var e = this,
                t, i;
            if (!0 === e.options.dots && e.slideCount > e.options.slidesToShow) {
                for (i = '<ul class="' + e.options.dotsClass + '">', t = 0; t <= e.getDotCount(); t += 1) i += "<li>" + e.options.customPaging.call(this, e, t) + "</li>";
                i += "</ul>", e.$dots = $(i).appendTo(e.options.appendDots), e.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
            }
        }, e.prototype.buildOut = function () {
            var e = this;
            e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
                $(t).attr("data-slick-index", e).data("originalStyling", $(t).attr("style") || "")
            }), e.$slidesCache = e.$slides, e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? $('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), $("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
        }, e.prototype.buildRows = function () {
            var e = this,
                t, i, o, s, n, r, l;
            if (s = document.createDocumentFragment(), r = e.$slider.children(), e.options.rows > 1) {
                for (l = e.options.slidesPerRow * e.options.rows, n = Math.ceil(r.length / l), t = 0; t < n; t++) {
                    var a = document.createElement("div");
                    for (i = 0; i < e.options.rows; i++) {
                        var d = document.createElement("div");
                        for (o = 0; o < e.options.slidesPerRow; o++) {
                            var c = t * l + (i * e.options.slidesPerRow + o);
                            r.get(c) && d.appendChild(r.get(c))
                        }
                        a.appendChild(d)
                    }
                    s.appendChild(a)
                }
                e.$slider.html(s), e.$slider.children().children().children().css({
                    width: 100 / e.options.slidesPerRow + "%",
                    display: "inline-block"
                })
            }
        }, e.prototype.checkResponsive = function (e, t) {
            var i = this,
                o, s, n, r = !1,
                l = i.$slider.width(),
                a = window.innerWidth || $(window).width();
            if ("window" === i.respondTo ? n = a : "slider" === i.respondTo ? n = l : "min" === i.respondTo && (n = Math.min(a, l)), i.options.responsive && i.options.responsive.length && null !== i.options.responsive) {
                s = null;
                for (o in i.breakpoints) i.breakpoints.hasOwnProperty(o) && (!1 === i.originalSettings.mobileFirst ? n < i.breakpoints[o] && (s = i.breakpoints[o]) : n > i.breakpoints[o] && (s = i.breakpoints[o]));
                null !== s ? null !== i.activeBreakpoint ? (s !== i.activeBreakpoint || t) && (i.activeBreakpoint = s, "unslick" === i.breakpointSettings[s] ? i.unslick(s) : (i.options = $.extend({}, i.originalSettings, i.breakpointSettings[s]), !0 === e && (i.currentSlide = i.options.initialSlide), i.refresh(e)), r = s) : (i.activeBreakpoint = s, "unslick" === i.breakpointSettings[s] ? i.unslick(s) : (i.options = $.extend({}, i.originalSettings, i.breakpointSettings[s]), !0 === e && (i.currentSlide = i.options.initialSlide), i.refresh(e)), r = s) : null !== i.activeBreakpoint && (i.activeBreakpoint = null, i.options = i.originalSettings, !0 === e && (i.currentSlide = i.options.initialSlide), i.refresh(e), r = s), e || !1 === r || i.$slider.trigger("breakpoint", [i, r])
            }
        }, e.prototype.changeSlide = function (e, t) {
            var i = this,
                o = $(e.target),
                s, n, r;
            switch (o.is("a") && e.preventDefault(), o.is("li") || (o = o.closest("li")), r = i.slideCount % i.options.slidesToScroll != 0, s = r ? 0 : (i.slideCount - i.currentSlide) % i.options.slidesToScroll, e.data.message) {
                case "previous":
                    n = 0 === s ? i.options.slidesToScroll : i.options.slidesToShow - s, i.slideCount > i.options.slidesToShow && i.slideHandler(i.currentSlide - n, !1, t);
                    break;
                case "next":
                    n = 0 === s ? i.options.slidesToScroll : s, i.slideCount > i.options.slidesToShow && i.slideHandler(i.currentSlide + n, !1, t);
                    break;
                case "index":
                    var l = 0 === e.data.index ? 0 : e.data.index || o.index() * i.options.slidesToScroll;
                    i.slideHandler(i.checkNavigable(l), !1, t), o.children().trigger("focus");
                    break;
                default:
                    return
            }
        }, e.prototype.checkNavigable = function (e) {
            var t = this,
                i, o;
            if (i = t.getNavigableIndexes(), o = 0, e > i[i.length - 1]) e = i[i.length - 1];
            else
                for (var s in i) {
                    if (e < i[s]) {
                        e = o;
                        break
                    }
                    o = i[s]
                }
            return e
        }, e.prototype.cleanUpEvents = function () {
            var e = this;
            e.options.dots && null !== e.$dots && ($("li", e.$dots).off("click.slick", e.changeSlide), !0 === e.options.pauseOnDotsHover && !0 === e.options.autoplay && $("li", e.$dots).off("mouseenter.slick", $.proxy(e.setPaused, e, !0)).off("mouseleave.slick", $.proxy(e.setPaused, e, !1))), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), $(document).off(e.visibilityChange, e.visibility), e.$list.off("mouseenter.slick", $.proxy(e.setPaused, e, !0)), e.$list.off("mouseleave.slick", $.proxy(e.setPaused, e, !1)), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && $(e.$slideTrack).children().off("click.slick", e.selectHandler), $(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), $(window).off("resize.slick.slick-" + e.instanceUid, e.resize), $("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), $(window).off("load.slick.slick-" + e.instanceUid, e.setPosition), $(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, e.prototype.cleanUpRows = function () {
            var e = this,
                t;
            e.options.rows > 1 && (t = e.$slides.children().children(), t.removeAttr("style"), e.$slider.html(t))
        }, e.prototype.clickHandler = function (e) {
            !1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
        }, e.prototype.destroy = function (e) {
            var t = this;
            t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), $(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), !0 === t.options.arrows && (t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove())), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
                $(this).attr("style", $(this).data("originalStyling"))
            }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
        }, e.prototype.disableTransition = function (e) {
            var t = this,
                i = {};
            i[t.transitionType] = "", !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
        }, e.prototype.fadeSlide = function (e, t) {
            var i = this;
            !1 === i.cssTransitions ? (i.$slides.eq(e).css({
                zIndex: i.options.zIndex
            }), i.$slides.eq(e).animate({
                opacity: 1
            }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
                opacity: 1,
                zIndex: i.options.zIndex
            }), t && setTimeout(function () {
                i.disableTransition(e), t.call()
            }, i.options.speed))
        }, e.prototype.fadeSlideOut = function (e) {
            var t = this;
            !1 === t.cssTransitions ? t.$slides.eq(e).animate({
                opacity: 0,
                zIndex: t.options.zIndex - 2
            }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
                opacity: 0,
                zIndex: t.options.zIndex - 2
            }))
        }, e.prototype.filterSlides = e.prototype.slickFilter = function (e) {
            var t = this;
            null !== e && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
        }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
            return this.currentSlide
        }, e.prototype.getDotCount = function () {
            var e = this,
                t = 0,
                i = 0,
                o = 0;
            if (!0 === e.options.infinite)
                for (; t < e.slideCount;)++o, t = i + e.options.slidesToShow, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            else if (!0 === e.options.centerMode) o = e.slideCount;
            else
                for (; t < e.slideCount;)++o, t = i + e.options.slidesToShow, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            return o - 1
        }, e.prototype.getLeft = function (e) {
            var t = this,
                i, o, s = 0,
                n;
            return t.slideOffset = 0, o = t.$slides.first().outerHeight(!0), !0 === t.options.infinite ? (t.slideCount > t.options.slidesToShow && (t.slideOffset = t.slideWidth * t.options.slidesToShow * -1, s = o * t.options.slidesToShow * -1), t.slideCount % t.options.slidesToScroll != 0 && e + t.options.slidesToScroll > t.slideCount && t.slideCount > t.options.slidesToShow && (e > t.slideCount ? (t.slideOffset = (t.options.slidesToShow - (e - t.slideCount)) * t.slideWidth * -1, s = (t.options.slidesToShow - (e - t.slideCount)) * o * -1) : (t.slideOffset = t.slideCount % t.options.slidesToScroll * t.slideWidth * -1, s = t.slideCount % t.options.slidesToScroll * o * -1))) : e + t.options.slidesToShow > t.slideCount && (t.slideOffset = (e + t.options.slidesToShow - t.slideCount) * t.slideWidth, s = (e + t.options.slidesToShow - t.slideCount) * o), t.slideCount <= t.options.slidesToShow && (t.slideOffset = 0, s = 0), !0 === t.options.centerMode && !0 === t.options.infinite ? t.slideOffset += t.slideWidth * Math.floor(t.options.slidesToShow / 2) - t.slideWidth : !0 === t.options.centerMode && (t.slideOffset = 0, t.slideOffset += t.slideWidth * Math.floor(t.options.slidesToShow / 2)), i = !1 === t.options.vertical ? e * t.slideWidth * -1 + t.slideOffset : e * o * -1 + s, !0 === t.options.variableWidth && (n = t.slideCount <= t.options.slidesToShow || !1 === t.options.infinite ? t.$slideTrack.children(".slick-slide").eq(e) : t.$slideTrack.children(".slick-slide").eq(e + t.options.slidesToShow), i = n[0] ? -1 * n[0].offsetLeft : 0, !0 === t.options.centerMode && (n = !1 === t.options.infinite ? t.$slideTrack.children(".slick-slide").eq(e) : t.$slideTrack.children(".slick-slide").eq(e + t.options.slidesToShow + 1), i = n[0] ? -1 * n[0].offsetLeft : 0, i += (t.$list.width() - n.outerWidth()) / 2)), i
        }, e.prototype.getOption = e.prototype.slickGetOption = function (e) {
            return this.options[e]
        }, e.prototype.getNavigableIndexes = function () {
            var e = this,
                t = 0,
                i = 0,
                o = [],
                s;
            for (!1 === e.options.infinite ? s = e.slideCount : (t = -1 * e.options.slidesToScroll, i = -1 * e.options.slidesToScroll, s = 2 * e.slideCount); t < s;) o.push(t), t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
            return o
        }, e.prototype.getSlick = function () {
            return this
        }, e.prototype.getSlideCount = function () {
            var e = this,
                t, i, o;
            return o = !0 === e.options.centerMode ? e.slideWidth * Math.floor(e.options.slidesToShow / 2) : 0, !0 === e.options.swipeToSlide ? (e.$slideTrack.find(".slick-slide").each(function (t, s) {
                if (s.offsetLeft - o + $(s).outerWidth() / 2 > -1 * e.swipeLeft) return i = s, !1
            }), t = Math.abs($(i).attr("data-slick-index") - e.currentSlide) || 1) : e.options.slidesToScroll
        }, e.prototype.goTo = e.prototype.slickGoTo = function (e, t) {
            this.changeSlide({
                data: {
                    message: "index",
                    index: parseInt(e)
                }
            }, t)
        }, e.prototype.init = function (e) {
            var t = this;
            $(t.$slider).hasClass("slick-initialized") || ($(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA()
        }, e.prototype.initArrowEvents = function () {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.on("click.slick", {
                message: "previous"
            }, e.changeSlide), e.$nextArrow.on("click.slick", {
                message: "next"
            }, e.changeSlide))
        }, e.prototype.initDotEvents = function () {
            var e = this;
            !0 === e.options.dots && e.slideCount > e.options.slidesToShow && $("li", e.$dots).on("click.slick", {
                message: "index"
            }, e.changeSlide), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && !0 === e.options.autoplay && $("li", e.$dots).on("mouseenter.slick", $.proxy(e.setPaused, e, !0)).on("mouseleave.slick", $.proxy(e.setPaused, e, !1))
        }, e.prototype.initializeEvents = function () {
            var e = this;
            e.initArrowEvents(), e.initDotEvents(), e.$list.on("touchstart.slick mousedown.slick", {
                action: "start"
            }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
                action: "move"
            }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
                action: "end"
            }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), $(document).on(e.visibilityChange, $.proxy(e.visibility, e)), e.$list.on("mouseenter.slick", $.proxy(e.setPaused, e, !0)), e.$list.on("mouseleave.slick", $.proxy(e.setPaused, e, !1)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && $(e.$slideTrack).children().on("click.slick", e.selectHandler), $(window).on("orientationchange.slick.slick-" + e.instanceUid, $.proxy(e.orientationChange, e)), $(window).on("resize.slick.slick-" + e.instanceUid, $.proxy(e.resize, e)), $("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), $(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), $(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition)
        }, e.prototype.initUI = function () {
            var e = this;
            !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show(), !0 === e.options.autoplay && e.autoPlay()
        }, e.prototype.keyHandler = function (e) {
            var t = this;
            e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({
                data: {
                    message: "previous"
                }
            }) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({
                data: {
                    message: "next"
                }
            }))
        }, e.prototype.lazyLoad = function () {
            function e(e) {
                $("img[data-lazy]", e).each(function () {
                    var e = $(this),
                        t = $(this).attr("data-lazy"),
                        i = document.createElement("img");
                    i.onload = function () {
                        e.animate({
                            opacity: 0
                        }, 100, function () {
                            e.attr("src", t).animate({
                                opacity: 1
                            }, 200, function () {
                                e.removeAttr("data-lazy").removeClass("slick-loading")
                            })
                        })
                    }, i.src = t
                })
            }
            var t = this,
                i, o, s, n;
            !0 === t.options.centerMode ? !0 === t.options.infinite ? (s = t.currentSlide + (t.options.slidesToShow / 2 + 1), n = s + t.options.slidesToShow + 2) : (s = Math.max(0, t.currentSlide - (t.options.slidesToShow / 2 + 1)), n = t.options.slidesToShow / 2 + 1 + 2 + t.currentSlide) : (s = t.options.infinite ? t.options.slidesToShow + t.currentSlide : t.currentSlide, n = s + t.options.slidesToShow, !0 === t.options.fade && (s > 0 && s-- , n <= t.slideCount && n++)), i = t.$slider.find(".slick-slide").slice(s, n), e(i), t.slideCount <= t.options.slidesToShow ? (o = t.$slider.find(".slick-slide"), e(o)) : t.currentSlide >= t.slideCount - t.options.slidesToShow ? (o = t.$slider.find(".slick-cloned").slice(0, t.options.slidesToShow), e(o)) : 0 === t.currentSlide && (o = t.$slider.find(".slick-cloned").slice(-1 * t.options.slidesToShow), e(o))
        }, e.prototype.loadSlider = function () {
            var e = this;
            e.setPosition(), e.$slideTrack.css({
                opacity: 1
            }), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
        }, e.prototype.next = e.prototype.slickNext = function () {
            this.changeSlide({
                data: {
                    message: "next"
                }
            })
        }, e.prototype.orientationChange = function () {
            var e = this;
            e.checkResponsive(), e.setPosition()
        }, e.prototype.pause = e.prototype.slickPause = function () {
            var e = this;
            e.autoPlayClear(), e.paused = !0
        }, e.prototype.play = e.prototype.slickPlay = function () {
            var e = this;
            e.paused = !1, e.autoPlay()
        }, e.prototype.postSlide = function (e) {
            var t = this;
            t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.setPosition(), t.swipeLeft = null, !0 === t.options.autoplay && !1 === t.paused && t.autoPlay(), !0 === t.options.accessibility && t.initADA()
        },
            e.prototype.prev = e.prototype.slickPrev = function () {
                this.changeSlide({
                    data: {
                        message: "previous"
                    }
                })
            }, e.prototype.preventDefault = function (e) {
                e.preventDefault()
            }, e.prototype.progressiveLazyLoad = function () {
                var e = this,
                    t, i;
                (t = $("img[data-lazy]", e.$slider).length) > 0 && (i = $("img[data-lazy]", e.$slider).first(), i.attr("src", i.attr("data-lazy")).removeClass("slick-loading").load(function () {
                    i.removeAttr("data-lazy"), e.progressiveLazyLoad(), !0 === e.options.adaptiveHeight && e.setPosition()
                }).error(function () {
                    i.removeAttr("data-lazy"), e.progressiveLazyLoad()
                }))
            }, e.prototype.refresh = function (e) {
                var t = this,
                    i = t.currentSlide;
                t.destroy(!0), $.extend(t, t.initials, {
                    currentSlide: i
                }), t.init(), e || t.changeSlide({
                    data: {
                        message: "index",
                        index: i
                    }
                }, !1)
            }, e.prototype.registerBreakpoints = function () {
                var e = this,
                    t, i, o, s = e.options.responsive || null;
                if ("array" === $.type(s) && s.length) {
                    e.respondTo = e.options.respondTo || "window";
                    for (t in s)
                        if (o = e.breakpoints.length - 1, i = s[t].breakpoint, s.hasOwnProperty(t)) {
                            for (; o >= 0;) e.breakpoints[o] && e.breakpoints[o] === i && e.breakpoints.splice(o, 1), o--;
                            e.breakpoints.push(i), e.breakpointSettings[i] = s[t].settings
                        }
                    e.breakpoints.sort(function (t, i) {
                        return e.options.mobileFirst ? t - i : i - t
                    })
                }
            }, e.prototype.reinit = function () {
                var e = this;
                e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && $(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses(0), e.setPosition(), e.$slider.trigger("reInit", [e]), !0 === e.options.autoplay && e.focusHandler()
            }, e.prototype.resize = function () {
                var e = this;
                $(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
                    e.windowWidth = $(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
                }, 50))
            }, e.prototype.removeSlide = e.prototype.slickRemove = function (e, t, i) {
                var o = this;
                if ("boolean" == typeof e ? (t = e, e = !0 === t ? 0 : o.slideCount - 1) : e = !0 === t ? --e : e, o.slideCount < 1 || e < 0 || e > o.slideCount - 1) return !1;
                o.unload(), !0 === i ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(e).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, o.reinit()
            }, e.prototype.setCSS = function (e) {
                var t = this,
                    i = {},
                    o, s;
                !0 === t.options.rtl && (e = -e), o = "left" == t.positionProp ? Math.ceil(e) + "px" : "0px", s = "top" == t.positionProp ? Math.ceil(e) + "px" : "0px", i[t.positionProp] = e, !1 === t.transformsEnabled ? t.$slideTrack.css(i) : (i = {}, !1 === t.cssTransitions ? (i[t.animType] = "translate(" + o + ", " + s + ")", t.$slideTrack.css(i)) : (i[t.animType] = "translate3d(" + o + ", " + s + ", 0px)", t.$slideTrack.css(i)))
            }, e.prototype.setDimensions = function () {
                var e = this;
                !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
                    padding: "0px " + e.options.centerPadding
                }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({
                    padding: e.options.centerPadding + " 0px"
                })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
                var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
                !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
            }, e.prototype.setFade = function () {
                var e = this,
                    t;
                e.$slides.each(function (i, o) {
                    t = e.slideWidth * i * -1, !0 === e.options.rtl ? $(o).css({
                        position: "relative",
                        right: t,
                        top: 0,
                        zIndex: e.options.zIndex - 2,
                        opacity: 0
                    }) : $(o).css({
                        position: "relative",
                        left: t,
                        top: 0,
                        zIndex: e.options.zIndex - 2,
                        opacity: 0
                    })
                }), e.$slides.eq(e.currentSlide).css({
                    zIndex: e.options.zIndex - 1,
                    opacity: 1
                })
            }, e.prototype.setHeight = function () {
                var e = this;
                if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
                    var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
                    e.$list.css("height", t)
                }
            }, e.prototype.setOption = e.prototype.slickSetOption = function (e, t, i) {
                var o = this,
                    s, n;
                if ("responsive" === e && "array" === $.type(t))
                    for (n in t)
                        if ("array" !== $.type(o.options.responsive)) o.options.responsive = [t[n]];
                        else {
                            for (s = o.options.responsive.length - 1; s >= 0;) o.options.responsive[s].breakpoint === t[n].breakpoint && o.options.responsive.splice(s, 1), s--;
                            o.options.responsive.push(t[n])
                        }
                else o.options[e] = t;
                !0 === i && (o.unload(), o.reinit())
            }, e.prototype.setPosition = function () {
                var e = this;
                e.setDimensions(), e.setHeight(), !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
            }, e.prototype.setProps = function () {
                var e = this,
                    t = document.body.style;
                e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = null !== e.animType && !1 !== e.animType
            }, e.prototype.setSlideClasses = function (e) {
                var t = this,
                    i, o, s, n;
                o = t.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), t.$slides.eq(e).addClass("slick-current"), !0 === t.options.centerMode ? (i = Math.floor(t.options.slidesToShow / 2), !0 === t.options.infinite && (e >= i && e <= t.slideCount - 1 - i ? t.$slides.slice(e - i, e + i + 1).addClass("slick-active").attr("aria-hidden", "false") : (s = t.options.slidesToShow + e, o.slice(s - i + 1, s + i + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? o.eq(o.length - 1 - t.options.slidesToShow).addClass("slick-center") : e === t.slideCount - 1 && o.eq(t.options.slidesToShow).addClass("slick-center")), t.$slides.eq(e).addClass("slick-center")) : e >= 0 && e <= t.slideCount - t.options.slidesToShow ? t.$slides.slice(e, e + t.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : o.length <= t.options.slidesToShow ? o.addClass("slick-active").attr("aria-hidden", "false") : (n = t.slideCount % t.options.slidesToShow, s = !0 === t.options.infinite ? t.options.slidesToShow + e : e, t.options.slidesToShow == t.options.slidesToScroll && t.slideCount - e < t.options.slidesToShow ? o.slice(s - (t.options.slidesToShow - n), s + n).addClass("slick-active").attr("aria-hidden", "false") : o.slice(s, s + t.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === t.options.lazyLoad && t.lazyLoad()
            }, e.prototype.setupInfinite = function () {
                var e = this,
                    t, i, o;
                if (!0 === e.options.fade && (e.options.centerMode = !1), !0 === e.options.infinite && !1 === e.options.fade && (i = null, e.slideCount > e.options.slidesToShow)) {
                    for (o = !0 === e.options.centerMode ? e.options.slidesToShow + 1 : e.options.slidesToShow, t = e.slideCount; t > e.slideCount - o; t -= 1) i = t - 1, $(e.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - e.slideCount).prependTo(e.$slideTrack).addClass("slick-cloned");
                    for (t = 0; t < o; t += 1) i = t, $(e.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + e.slideCount).appendTo(e.$slideTrack).addClass("slick-cloned");
                    e.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                        $(this).attr("id", "")
                    })
                }
            }, e.prototype.setPaused = function (e) {
                var t = this;
                !0 === t.options.autoplay && !0 === t.options.pauseOnHover && (t.paused = e, e ? t.autoPlayClear() : t.autoPlay())
            }, e.prototype.selectHandler = function (e) {
                var t = this,
                    i = $(e.target).is(".slick-slide") ? $(e.target) : $(e.target).parents(".slick-slide"),
                    o = parseInt(i.attr("data-slick-index"));
                if (o || (o = 0), t.slideCount <= t.options.slidesToShow) return t.setSlideClasses(o), void t.asNavFor(o);
                t.slideHandler(o)
            }, e.prototype.slideHandler = function (e, t, i) {
                var o, s, n, r, l = null,
                    a = this;
                if (t = t || !1, (!0 !== a.animating || !0 !== a.options.waitForAnimate) && !(!0 === a.options.fade && a.currentSlide === e || a.slideCount <= a.options.slidesToShow)) {
                    if (!1 === t && a.asNavFor(e), o = e, l = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, !1 === a.options.infinite && !1 === a.options.centerMode && (e < 0 || e > a.getDotCount() * a.options.slidesToScroll)) return void (!1 === a.options.fade && (o = a.currentSlide, !0 !== i ? a.animateSlide(r, function () {
                        a.postSlide(o)
                    }) : a.postSlide(o)));
                    if (!1 === a.options.infinite && !0 === a.options.centerMode && (e < 0 || e > a.slideCount - a.options.slidesToScroll)) return void (!1 === a.options.fade && (o = a.currentSlide, !0 !== i ? a.animateSlide(r, function () {
                        a.postSlide(o)
                    }) : a.postSlide(o)));
                    if (!0 === a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll != 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll != 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.updateDots(), a.updateArrows(), !0 === a.options.fade) return !0 !== i ? (a.fadeSlideOut(n), a.fadeSlide(s, function () {
                        a.postSlide(s)
                    })) : a.postSlide(s), void a.animateHeight();
                    !0 !== i ? a.animateSlide(l, function () {
                        a.postSlide(s)
                    }) : a.postSlide(s)
                }
            }, e.prototype.startLoad = function () {
                var e = this;
                !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
            }, e.prototype.swipeDirection = function () {
                var e, t, i, o, s = this;
                return e = s.touchObject.startX - s.touchObject.curX, t = s.touchObject.startY - s.touchObject.curY, i = Math.atan2(t, e), o = Math.round(180 * i / Math.PI), o < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && o >= 315 ? !1 === s.options.rtl ? "left" : "right" : o >= 135 && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? o >= 35 && o <= 135 ? "left" : "right" : "vertical"
            }, e.prototype.swipeEnd = function (e) {
                var t = this,
                    i;
                if (t.dragging = !1, t.shouldClick = !(t.touchObject.swipeLength > 10), void 0 === t.touchObject.curX) return !1;
                if (!0 === t.touchObject.edgeHit && t.$slider.trigger("edge", [t, t.swipeDirection()]), t.touchObject.swipeLength >= t.touchObject.minSwipe) switch (t.swipeDirection()) {
                    case "left":
                        i = t.options.swipeToSlide ? t.checkNavigable(t.currentSlide + t.getSlideCount()) : t.currentSlide + t.getSlideCount(), t.slideHandler(i), t.currentDirection = 0, t.touchObject = {}, t.$slider.trigger("swipe", [t, "left"]);
                        break;
                    case "right":
                        i = t.options.swipeToSlide ? t.checkNavigable(t.currentSlide - t.getSlideCount()) : t.currentSlide - t.getSlideCount(), t.slideHandler(i), t.currentDirection = 1, t.touchObject = {}, t.$slider.trigger("swipe", [t, "right"]);
                        break
                } else t.touchObject.startX !== t.touchObject.curX && (t.slideHandler(t.currentSlide), t.touchObject = {})
            }, e.prototype.swipeHandler = function (e) {
                var t = this;
                if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
                    case "start":
                        t.swipeStart(e);
                        break;
                    case "move":
                        t.swipeMove(e);
                        break;
                    case "end":
                        t.swipeEnd(e);
                        break
                }
            }, e.prototype.swipeMove = function (e) {
                var t = this,
                    i = !1,
                    o, s, n, r, l;
                return l = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!t.dragging || l && 1 !== l.length) && (o = t.getLeft(t.currentSlide), t.touchObject.curX = void 0 !== l ? l[0].pageX : e.clientX, t.touchObject.curY = void 0 !== l ? l[0].pageY : e.clientY, t.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(t.touchObject.curX - t.touchObject.startX, 2))), !0 === t.options.verticalSwiping && (t.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(t.touchObject.curY - t.touchObject.startY, 2)))), "vertical" !== (s = t.swipeDirection()) ? (void 0 !== e.originalEvent && t.touchObject.swipeLength > 4 && e.preventDefault(), r = (!1 === t.options.rtl ? 1 : -1) * (t.touchObject.curX > t.touchObject.startX ? 1 : -1), !0 === t.options.verticalSwiping && (r = t.touchObject.curY > t.touchObject.startY ? 1 : -1), n = t.touchObject.swipeLength, t.touchObject.edgeHit = !1, !1 === t.options.infinite && (0 === t.currentSlide && "right" === s || t.currentSlide >= t.getDotCount() && "left" === s) && (n = t.touchObject.swipeLength * t.options.edgeFriction, t.touchObject.edgeHit = !0), !1 === t.options.vertical ? t.swipeLeft = o + n * r : t.swipeLeft = o + n * (t.$list.height() / t.listWidth) * r, !0 === t.options.verticalSwiping && (t.swipeLeft = o + n * r), !0 !== t.options.fade && !1 !== t.options.touchMove && (!0 === t.animating ? (t.swipeLeft = null, !1) : void t.setCSS(t.swipeLeft))) : void 0)
            }, e.prototype.swipeStart = function (e) {
                var t = this,
                    i;
                if (1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow) return t.touchObject = {}, !1;
                void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (i = e.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== i ? i.pageX : e.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== i ? i.pageY : e.clientY, t.dragging = !0
            }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
                var e = this;
                null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
            }, e.prototype.unload = function () {
                var e = this;
                $(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
            }, e.prototype.unslick = function (e) {
                var t = this;
                t.$slider.trigger("unslick", [t, e]), t.destroy()
            }, e.prototype.updateArrows = function () {
                var e = this,
                    t;
                t = Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
            }, e.prototype.updateDots = function () {
                var e = this;
                null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
            }, e.prototype.visibility = function () {
                var e = this;
                document[e.hidden] ? (e.paused = !0, e.autoPlayClear()) : !0 === e.options.autoplay && (e.paused = !1, e.autoPlay())
            }, e.prototype.initADA = function () {
                var e = this;
                e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
                    "aria-hidden": "true",
                    tabindex: "-1"
                }).find("a, input, button, select").attr({
                    tabindex: "-1"
                }), e.$slideTrack.attr("role", "listbox"), e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (t) {
                    $(this).attr({
                        role: "option",
                        "aria-describedby": "slick-slide" + e.instanceUid + t
                    })
                }), null !== e.$dots && e.$dots.attr("role", "tablist").find("li").each(function (t) {
                    $(this).attr({
                        role: "presentation",
                        "aria-selected": "false",
                        "aria-controls": "navigation" + e.instanceUid + t,
                        id: "slick-slide" + e.instanceUid + t
                    })
                }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), e.activateADA()
            }, e.prototype.activateADA = function () {
                var e = this,
                    t = e.$slider.find("*").is(":focus");
                e.$slideTrack.find(".slick-active").attr({
                    "aria-hidden": "false",
                    tabindex: "0"
                }).find("a, input, button, select").attr({
                    tabindex: "0"
                }), t && e.$slideTrack.find(".slick-active").focus()
            }, e.prototype.focusHandler = function () {
                var e = this;
                e.$slider.on("focus.slick blur.slick", "*", function (t) {
                    t.stopImmediatePropagation();
                    var i = $(this);
                    setTimeout(function () {
                        e.isPlay && (i.is(":focus") ? (e.autoPlayClear(), e.paused = !0) : (e.paused = !1, e.autoPlay()))
                    }, 0)
                })
            }, $.fn.slick = function () {
                var t = this,
                    i = arguments[0],
                    o = Array.prototype.slice.call(arguments, 1),
                    s = t.length,
                    n = 0,
                    r;
                for (n; n < s; n++)
                    if ("object" == typeof i || void 0 === i ? t[n].slick = new e(t[n], i) : r = t[n].slick[i].apply(t[n].slick, o), void 0 !== r) return r;
                return t
            }
    }),
    function ($) {
        $.fn.unveil = function (e, t) {
            function i() {
                var e = l.filter(function () {
                    var e = $(this);
                    if (!e.is(":hidden")) {
                        var t = o.scrollTop(),
                            i = t + o.height(),
                            n = e.offset().top;
                        return n + e.height() >= t - s && n <= i + s
                    }
                });
                a = e.trigger("unveil"), l = l.not(a)
            }
            var o = $(window),
                s = e || 0,
                n = window.devicePixelRatio > 1,
                r = n ? "data-src-retina" : "data-src",
                l = this,
                a;
            return this.one("unveil", function () {
                var e = this.getAttribute(r);
                (e = e || this.getAttribute("data-src")) && ("IMG" === this.tagName ? (this.setAttribute("src", e), this.parentElement.classList.add("is-loaded")) : (this.style.backgroundImage = "url(" + e + ")", this.parentElement.classList.add("is-loaded")), "function" == typeof t && t.call(this))
            }), o.on("scroll.unveil resize.unveil lookup.unveil", i), i(), this
        }
    }(window.jQuery || window.Zepto),
    function ($) {
        var e = $.fn.addClass;
        $.fn.addClass = function (t) {
            for (var i = e.apply(this, arguments), o, s = 0, n = this.length; s < n; s++)
                if ((o = this[s]) instanceof SVGElement) {
                    var r = $(o).attr("class");
                    if (r) {
                        var l = r.indexOf(t); - 1 === l && (r = r + " " + t, $(o).attr("class", r))
                    } else $(o).attr("class", t)
                }
            return i
        };
        var t = $.fn.removeClass;
        $.fn.removeClass = function (e) {
            for (var i = t.apply(this, arguments), o, s = 0, n = this.length; s < n; s++)
                if ((o = this[s]) instanceof SVGElement) {
                    var r = $(o).attr("class");
                    if (r) {
                        var l = r.indexOf(e); - 1 !== l && (r = r.substring(0, l) + r.substring(l + e.length, r.length), $(o).attr("class", r))
                    }
                }
            return i
        };
        var i = $.fn.hasClass;
        $.fn.hasClass = function (e) {
            for (var t = i.apply(this, arguments), o, s = 0, n = this.length; s < n; s++)
                if ((o = this[s]) instanceof SVGElement) {
                    var r = $(o).attr("class");
                    return !!r && -1 !== r.indexOf(e)
                }
            return t
        }
    }(jQuery), $.easing.jswing = $.easing.swing, $.extend($.easing, {
        def: "easeInExpo",
        swing: function (e, t, i, o, s) {
            return jQuery.easing[jQuery.easing.def](e, t, i, o, s)
        },
        easeInSine: function (e, t, i, o, s) {
            return -o * Math.cos(t / s * (Math.PI / 2)) + o + i
        },
        easeOutSine: function (e, t, i, o, s) {
            return o * Math.sin(t / s * (Math.PI / 2)) + i
        },
        easeInOutSine: function (e, t, i, o, s) {
            return -o / 2 * (Math.cos(Math.PI * t / s) - 1) + i
        },
        easeInExpo: function (e, t, i, o, s) {
            return 0 === t ? i : o * Math.pow(2, 10 * (t / s - 1)) + i
        },
        easeOutExpo: function (e, t, i, o, s) {
            return t === s ? i + o : o * (1 - Math.pow(2, -10 * t / s)) + i
        }
    }),
    function ($) {
        ({
            init: function () {
                var e = "https://" + top.location.host.toString(),
                    t = $("a[href^='" + e + "'], a[href^='/'], a[href^='./'], a[href^='../']"),
                    i = t.not(".no-trans");
                setTimeout(function () {
                    $("body").removeClass("fade-in-page")
                }, 5100), t.click(function (e) {
                    function t() {
                        window.location = i
                    }
                    if (!(e.metaKey || e.ctrlKey || e.metaKey || e.shiftKey)) {
                        e.preventDefault();
                        var i = this.href;
                        $("body").addClass("is-exiting"), $(".site-menu, main").animate({
                            opacity: "0"
                        }, {
                                duration: 600,
                                complete: function () {
                                    $("body").fadeOut(600, t)
                                }
                            })
                    }
                }), $(window).unload(function () {
                    $(window).unbind("unload")
                }), $(window).bind("pageshow", function (e) {
                    e.originalEvent.persisted && window.location.reload()
                })
            }
        }).init()
    }(jQuery),
    function ($) {
        scrollDirection = {
            init: function () {
                function e() {
                    var e = $(this).scrollTop();
                    Math.abs(i - e) <= o || (e > i && e > s ? $("body").removeClass("at-top close-to-top").removeClass("scrolling-up").addClass("scrolling-down") : e + $(window).height() < $(document).height() && $("body").removeClass("scrolling-down").addClass("scrolling-up"), $("body").hasClass("scrolling-up") && e <= 225 && $("body").addClass("close-to-top"), e <= 0 && $("body").removeClass("scrolling-down scrolling-up close-to-top").addClass("at-top"), i = e)
                }
                var t, i = 0,
                    o = 15,
                    s = $(".header-main").outerHeight();
                $(window).scroll(function () {
                    t = !0
                }), setInterval(function () {
                    t && (e(), t = !1)
                }, 250)
            }
        }, scrollDirection.init()
    }(jQuery),
    function ($) {
        var e, t = {
            settings: {
                body: $("body"),
                menuToggle: $(".js-menu-toggle")
            },
            init: function () {
                e = this.settings, this.bindEvents()
            },
            bindEvents: function () {
                e.menuToggle.click(function (e) {
                    e.preventDefault(), t.toggleMenu()
                }), $(document).keyup(function (e) {
                    $("body").hasClass("js-menu--is-open") && 27 === e.which && t.toggleMenu()
                })
            },
            toggleMenu: function () {
                e.body.toggleClass("js-menu--is-open")
            },
            closeMenu: function () {
                $(".js-menu--is-open").removeClass("js-menu--is-open")
            }
        };
        t.init()
    }(jQuery),
    function ($) {
        ({
            init: function (e) {
                $("[data-scroll]").each(function () {
                    var t = $(this);
                    (e || t).waypoint(function () {
                        t.addClass("animated")
                    }, {
                            triggerOnce: !0,
                            offset: "90%"
                        })
                })
            }
        }).init()
    }(jQuery), $(".js-network-map").waypoint(function () {
        function e() {
            i != t.length && (t.eq(i++).addClass("is-animated"), window.setTimeout(e, 800))
        }
        var t = $(".network-map__layer"),
            i = 2;
        $(".network-map__stats").addClass("is-animated"), setTimeout(function () {
            e()
        }, 800)
    }, {
            triggerOnce: !0,
            offset: "110%"
        }),
    function ($) {
        $.fn.scrollAnchor = function (e, t) {
            var i = $.extend({
                offset: "0"
            }, e);
            return this.each(function () {
                $("[data-scrollto]").click(function (t) {
                    t.preventDefault();
                    var i = $(this),
                        o = e.offset,
                        s = "#" + $(this).data("scrollto"),
                        n = $(s);
                    e.addActive && $("a[data-scrollto]").removeClass("active"), $("html, body").stop().animate({
                        scrollTop: n.offset().top - o
                    }, {
                            duration: 600,
                            easing: "easeInExpo",
                            complete: e.complete
                        }), $.isFunction(e.setup)
                })
            })
        }
    }(jQuery), $("html").scrollAnchor({
        offset: "150"
    }),
    function ($) {
        ({
            init: function () {
                window.location.hash && setTimeout(function () {
                    $("html, body").scrollTop(0).show(), $("html, body").animate({
                        scrollTop: $(window.location.hash).offset().top - 60
                    }, 1200, "swing")
                }, 100)
            }
        }).init()
    }(jQuery),
    function ($) {
        var e = {
            init: function () {
                var e = $(".js-sticky"),
                    t = (e.offset() || {
                        top: -100
                    }).top;
                $(document).on("scroll resize", e, function () {
                    $(document).scrollTop() >= t ? e.addClass("is-sticky") : e.removeClass("is-sticky")
                })
            }
        };
        $(".js-sticky").length && e.init()
    }(jQuery);
var PopItUp = function () {
    var e, t = {
        autoOpen: $("[data-popup-init]"),
        openLink: $("[data-popup]"),
        closeLink: $(".js-close-popup"),
        body: $(document.body),
        videoHolder: $(".popup__vid"),
        self: null
    };
    return {
        init: function () {
            e = t, this.bindEvents()
        },
        bindEvents: function () {
            void 0 !== e.autoOpen.data("popup-init") && PopItUp.autoOpenPopUp(), e.openLink.on("click", function (t) {
                t.preventDefault(), t.stopPropagation(), e.self = this, PopItUp.openPopUp(), $(this).data("vimeo-id") && PopItUp.playVideo()
            }), e.closeLink.on("click", function (e) {
                e.preventDefault(), PopItUp.closePopUp(), PopItUp.stopVideo()
            }), e.body.on("keyup click", function (t) {
                e.body.hasClass("popup--is-open") && 27 === t.which && (PopItUp.closePopUp(), PopItUp.stopVideo())
            })
        },
        autoOpenPopUp: function () {
            e.autoOpen.addClass("is-open"), e.body.addClass("popup--auto-open")
        },
        openPopUp: function () {
            var t = $(e.self).data("popup");
            e.autoOpen.removeClass("is-open"), $("#" + t).addClass("is-open").attr("aria-hidden", "false"), e.body.addClass("popup--is-open")
        },
        closePopUp: function () {
            var t = $(e.self).attr("data-popup");
            $("#" + t).removeClass("is-open").attr("aria-hidden", "true"), e.body.removeClass("popup--is-open popup--auto-open")
        },
        playVideo: function () {
            var t = $(e.self).data("vimeo-id"),
                i = "https://player.vimeo.com/video/",
                o = "https://player.vimeo.com/video/" + t,
                s = $(e.self).data("vimeo-color");
            $.getJSON("http://www.vimeo.com/api/oembed.json?url=" + encodeURIComponent(o) + "&title=0&byline=0&color=" + s + "&autoplay=1&callback=?", function (t) {
                e.videoHolder.html(t.html)
            })
        },
        stopVideo: function () {
            $(".popup__vid").empty()
        }
    }
}();
PopItUp.init(),
    function ($) {
        var e = $(window),
            t = e.height();
        e.resize(function () {
            t = e.height()
        }), $.fn.parallax = function (i, o, s) {
            function n(e, t) {
                var i = "translate3d(0px," + t + "px, 0";
                a.css({
                    "-ms-transform": i,
                    "-moz-transform": i,
                    "-webkit-transform": i,
                    transform: i
                })
            }

            function r() {
                n($(this), scrollY / 3)
            }

            function l() {
                var i = e.scrollTop();
                a.each(function () {
                    var e = $(this),
                        o = e.offset().top;
                    o + d(e) < i || o > i + t || window.requestAnimationFrame(r)
                })
            }
            var a = $(this),
                d, c;
            a.each(function () {
                c = a.offset().top
            }), d = s ? function (e) {
                return e.outerHeight(!0)
            } : function (e) {
                return e.height()
            }, (arguments.length < 1 || null === o) && (o = .1), e.bind("scroll", l).resize(l), l()
        }
    }(jQuery);
var Spanizer = function () {
    var e;
    return {
        settings: {
            letters: $(".js-letters"),
            words: $(".js-words")
        },
        init: function () {
            e = this.settings, this.bindEvents()
        },
        bindEvents: function () {
            Spanizer.letters(), Spanizer.words()
        },
        letters: function () {
            e.letters.html(function (e, t) {
                return "<span>" + $.trim(t).split("").join("</span><span>") + "</span>"
            })
        },
        words: function () {
            e.words.html(function (e, t) {
                var i = t.split(" "),
                    o = [];
                return "<span>" + i.join("</span> <span>") + "</span>"
            })
        }
    }
}();
($(".js-letters").length || $(".js-words").length) && Spanizer.init(), $(".js-locations-map").length && function ($) {
    function e(e) {
        var n = e.find(".marker"),
            r = {
                zoom: 16,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: s,
                scrollwheel: !1
            },
            l = new google.maps.Map(e[0], r);
        l.markers = [], n.each(function () {
            t($(this), l)
        }), i(l), o(l)
    }

    function t(e, t) {
        var i = new google.maps.LatLng(e.attr("data-lat"), e.attr("data-lng")),
            o = new google.maps.Marker({
                position: i,
                map: t,
                icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABIRJREFUeNqsV0lMZGUQrl5otrBvYUkA2cMWYOAAGJEOnjh7MCbGiyflPpmYaMwYjwYzGuegJ28zp0nsCNixBQFliYEgIDTYgDSEtVm76Qb8vpc8fOnp9Q2VVLrfe//766uqr6r+Z7i9vZVQMjAwIAaDQfx+vxwdHcnFxYWcnZ2Zjo+PH2xvb1tPTk5ew7tmLA1kZGS48vPzfy4oKPgjMTExkJqaKllZWWKxWIT7Dw4OSjgxSwySkJAgHo+nZWFh4Quv12vNzs421dXVKQaurq7k4OBA1tbWPgEwB+4/BKDfCT4WMcdifH19/b2ZmZmnFRUVlo6ODsnNzRWz+f9XA4GAHB4eGqempt6cnp7+DVH7qL29/ZtXBkDjc3Nz78P4d1arVZqamuTm5kaur68Vz1WhtwTV398viJJpaGjo6+TkZGNra+sT7bq4ANDQ5ubmg7GxsW/7+voU4z6fT0JxhvcYBQJraGgQo9EoNpvtK/BgrqioaDQSAGO4ByTdxMTE48rKyoTGxkZB7iUcYbVAuI78qK2tNUxOTn5+enpq1gVgY2PjdZDrLeacnsUjjAbfA3G7wR+rrhSsrKy8kZOTo+Q2GMDu7i5zTXJKeXm51NfXC0rw7jnXo1IEpcl9unDrp7gjsLe3V8pNTCbTS6Gn8eXlZYWI/OV1sJCYdAB9o0hXChgdVkGoeqbnka5VYZ+AmPQC8IVjPcMe6VqVy8tL/vh1cQDls7q/v68QilHQAmHOVc9VDgSHnzzg++CBSxeA6upqu8PhuHK73ZaSkhJlJqhCwmlJFyzkDYmKKrrt6upy6EoBCDRbWFj4fHx8/M6rWITr2IjQQ+j9CwAd0wUgMzNTOjs7P4YnR2goKqGiCtdhHrCPnOH9h9xHFwB6AQ+cvb29bwOAl5ti1EbcjM8xO2R0dDTQ09PzDiL4F/fRPYyY96qqqhEQ6l273f6M3mAiSqgBw5LF7JCRkRHp7u7+AO34RbRBFDECZDGVvb2mpuY5htGnw8PDgt6ukCw47zSGKci1T7D2e16zcqLND2Okaagq6xmj9bOkpKRJTMeXANB7jGw2jrW2trZH5+fnCiBGUFs9cQGg51pFNK5bWloeLS0tyc7Ozt2BhDlmVGZnZ6W5ufkx7nvYwAhAVV0AuIlWcR7kYLJDf6S3KrkIZHFxkVH5G/3iB45xNi+t6gLAMAcrjYJcXzqdTg4Z5R5DPD8/Lzg3PEUqfGretaoLAPMerIxCenr6LzDsRJ0rNc+OhxR4S0tLn5EvJGSwvlIVaJXhRBT86A+21dVVJQIul0vS0tKm4b2LXNHmPhYOmCP1gHDVQR7gHPAh2c7ax7WNhqLlOy4A4XLHSMDjP2HsAt8BKTh2SXFx8a/qofTeANC7CAPHDfZvIvw1AOpJSUlZpPFohIuLA6HYrCqJBQL+u7W1xTLcgh4wNeHW33sESD6A2MfXkOA70M3aJwA9YtbzEr2i1yQqDJ/w4/XeAeTl5YV/Cd0PEdrhkQsT0l1WVqarAiICiHQAIQAQ7x/+x+9SqKN7rPKfAAMAeEr6Wg15E0MAAAAASUVORK5CYII="
            });
        t.markers.push(o), e.html() && (locationsToggle = $(".js-locations-toggle").find("[data-lat='" + e.attr("data-lat") + "']"), locationsLink = $("locations-toc__link"), $(locationsToggle).click(function () {
            n.setContent(e.html()), n.open(t, o), $(".locations-toc__link.active").removeClass("active"), $(this).addClass("active")
        }), google.maps.event.addListener(o, "click", function () {
            n.setContent(e.html()), n.open(t, o)
        }), google.maps.event.addListener(t, "click", function (e) {
            n && n.close()
        }))
    }

    function i(e) {
        var t = new google.maps.LatLngBounds;
        $.each(e.markers, function (e, i) {
            var o = new google.maps.LatLng(i.position.lat(), i.position.lng());
            t.extend(o)
        }), 1 == e.markers.length ? (e.setCenter(t.getCenter()), e.setZoom(16)) : e.fitBounds(t)
    }

    function o(e) {
        google.maps.event.addDomListener(window, "resize", function () {
            var t = e.getCenter();
            google.maps.event.trigger(e, "resize"), e.setCenter(t)
        })
    }
    var s = [{
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [{
            color: "#d3d3d3"
        }]
    }, {
        featureType: "transit",
        stylers: [{
            color: "#808080"
        }, {
            visibility: "off"
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{
            visibility: "on"
        }, {
            color: "#b3b3b3"
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{
            color: "#ffffff"
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#ffffff"
        }, {
            weight: 1.8
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry.stroke",
        stylers: [{
            color: "#d7d7d7"
        }]
    }, {
        featureType: "poi",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#ebebeb"
        }]
    }, {
        featureType: "administrative",
        elementType: "geometry",
        stylers: [{
            color: "#a7a7a7"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.fill",
        stylers: [{
            color: "#ffffff"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.fill",
        stylers: [{
            color: "#ffffff"
        }]
    }, {
        featureType: "landscape",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#efefef"
        }]
    }, {
        featureType: "road",
        elementType: "labels.text.fill",
        stylers: [{
            color: "#696969"
        }]
    }, {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#737373"
        }]
    }, {
        featureType: "poi",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "poi",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.stroke",
        stylers: [{
            color: "#d6d6d6"
        }]
    }, {
        featureType: "road",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {}, {
        featureType: "poi",
        elementType: "geometry.fill",
        stylers: [{
            color: "#dadada"
        }]
    }],
        n = new google.maps.InfoWindow({
            content: ""
        });
    $(function () {
        $(".js-locations-map").each(function () {
            e($(this))
        }), $(".js-hq-map").each(function () {
            e($(this))
        })
    })
}(jQuery),
    function ($) {
        $.everydayImInstagrammin = {
            defaults: {
                clientID: "null",
                accessToken: "null",
                numberPics: "12",
                imgClass: "insta__img",
                sequenceFadeIn: "true",
                sequenceDuration: "220",
                captions: "false",
                captionAlign: "bottom",
                instaType: "instaUser"
            }
        }, $.fn.extend({
            everydayImInstagrammin: function (e) {
                return $.extend($.everydayImInstagrammin.defaults, e), this.each(function () {
                    var t = $(this),
                        i = e.clientID,
                        o = e.accessToken,
                        s = e.numberPics,
                        n = e.imgClass,
                        r = e.sequenceFadeIn,
                        l = e.sequenceDuration,
                        a = e.captionAlign,
                        d = e.hashTag,
                        c = "https://api.instagram.com/v1/users/" + i + "/media/recent/?access_token=" + o + "&callback=?";
                    "byHash" === e.instaType && (c = "https://api.instagram.com/v1/tags/" + d + "/media/recent/?access_token=" + o + "&callback=?"), $.ajax({
                        type: "GET",
                        dataType: "jsonp",
                        cache: !1,
                        url: c,
                        success: function (i) {
                            for (var o = 0; o < e.numberPics; o++)
                                if (i.data[o].hasOwnProperty("caption")) {
                                    var s = "";
                                    null !== i.data[o].caption && (s = i.data[o].caption.text), t.append("<li class='insta__item'><a target='_blank' href='" + i.data[o].link + "'><img class='" + e.imgClass + "' src='" + i.data[o].images.standard_resolution.url + "' /><div class='insta__caption'><p>" + s + "</p></div></a></li>")
                                } else t.append("<li class='insta__item'><a target='_blank' href='" + i.data[o].link + "'><img class='" + e.imgClass + "' src='" + i.data[o].images.standard_resolution.url + "'  /></a></li>");
                            if ("bottom" === e.captionAlign && $(".insta__caption p").css({
                                position: "absolute",
                                bottom: "1.5em"
                            }), "top" === e.captionAlign && $(".insta__caption p").css({
                                position: "absolute",
                                top: "1.5em"
                            }), "true" === e.sequenceFadeIn) {
                                var r = 0;
                                $("." + n).hide().each(function () {
                                    $(this).delay(r).fadeIn(500), r += l
                                })
                            }
                            $(".insta__item a").on({
                                mouseenter: function () {
                                    $(".insta__caption", this).filter(":not(:animated)").fadeIn(400)
                                },
                                mouseleave: function () {
                                    $(".insta__caption", this).fadeOut(400)
                                }
                            })
                        }
                    })
                })
            }
        })
    }(jQuery), $(".insta").everydayImInstagrammin({
        clientID: "2254353059",
        accessToken: "2254353059.1677ed0.cbed4ac58bd34d9b9bdc29301e804627",
        numberPics: "6",
        imgClass: "insta__img",
        instaUser: "sprecorp",
        captions: "true",
        captionAlign: "bottom",
        sequenceFadeIn: "true",
        sequenceDuration: 300
    }),
    function (e, t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : e.jQuery)
    }(this, function ($) {
        "use strict";

        function e(e) {
            var t = {},
                i, o, s, n, r, l, a;
            for (r = e.replace(/\s*:\s*/g, ":").replace(/\s*,\s*/g, ",").split(","), a = 0, l = r.length; a < l && (o = r[a], -1 === o.search(/^(http|https|ftp):\/\//) && -1 !== o.search(":")); a++) i = o.indexOf(":"), s = o.substring(0, i), n = o.substring(i + 1), n || (n = void 0), "string" == typeof n && (n = "true" === n || "false" !== n && n), "string" == typeof n && (n = isNaN(n) ? n : +n), t[s] = n;
            return null == s && null == n ? e : t
        }

        function t(e) {
            e = "" + e;
            var t = e.split(/\s+/),
                i = "50%",
                o = "50%",
                s, n, r;
            for (r = 0, s = t.length; r < s; r++) n = t[r], "left" === n ? i = "0%" : "right" === n ? i = "100%" : "top" === n ? o = "0%" : "bottom" === n ? o = "100%" : "center" === n ? 0 === r ? i = "50%" : o = "50%" : 0 === r ? i = n : o = n;
            return {
                x: i,
                y: o
            }
        }

        function i(e, t) {
            var i = function () {
                t(this.src)
            };
            $('<img src="' + e + '.jpg">').load(i)
        }

        function o(t, i, o) {
            if (this.$element = $(t), "string" == typeof i && (i = e(i)), o ? "string" == typeof o && (o = e(o)) : o = {}, "string" == typeof i) i = i.replace(/\.\w*$/, "");
            else if ("object" == typeof i)
                for (var s in i) i.hasOwnProperty(s) && (i[s] = i[s].replace(/\.\w*$/, ""));
            this.settings = $.extend({}, n, o), this.path = i;
            try {
                this.init()
            } catch (e) {
                if (e.message !== r) throw e
            }
        }
        var s = "vide",
            n = {
                volume: 1,
                playbackRate: 1,
                muted: !0,
                loop: !0,
                autoplay: !0,
                position: "50% 50%",
                posterType: "detect",
                resizing: !0,
                bgColor: "transparent",
                className: ""
            },
            r = "Not implemented";
        o.prototype.init = function () {
            var e = this,
                o = e.path,
                s = o,
                n = "",
                l = e.$element,
                a = e.settings,
                d = t(a.position),
                c = a.posterType,
                p, u;
            u = e.$wrapper = $("<div>").addClass(a.className).css({
                position: "absolute",
                "z-index": -1,
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                overflow: "hidden",
                "-webkit-background-size": "cover",
                "-moz-background-size": "cover",
                "-o-background-size": "cover",
                "background-size": "cover",
                "background-color": a.bgColor,
                "background-repeat": "no-repeat",
                "background-position": d.x + " " + d.y
            }), "object" == typeof o && (o.poster ? s = o.poster : o.mp4 ? s = o.mp4 : o.webm ? s = o.webm : o.ogv && (s = o.ogv)), "detect" === c ? i(s, function (e) {
                u.css("background-image", "url(" + e + ")")
            }) : "none" !== c && u.css("background-image", "url(" + s + "." + c + ")"), "static" === l.css("position") && l.css("position", "relative"), l.prepend(u), "object" == typeof o ? (o.mp4 && (n += '<source src="' + o.mp4 + '.mp4" type="video/mp4">'), o.webm && (n += '<source src="' + o.webm + '.webm" type="video/webm">'), o.ogv && (n += '<source src="' + o.ogv + '.ogv" type="video/ogg">'), p = e.$video = $("<video>" + n + "</video>")) : p = e.$video = $('<video><source src="' + o + '.mp4" type="video/mp4"><source src="' + o + '.webm" type="video/webm"><source src="' + o + '.ogv" type="video/ogg"></video>');
            try {
                p.prop({
                    autoplay: a.autoplay,
                    loop: a.loop,
                    volume: a.volume,
                    muted: a.muted,
                    defaultMuted: a.muted,
                    playbackRate: a.playbackRate,
                    defaultPlaybackRate: a.playbackRate
                })
            } catch (e) {
                throw new Error(r)
            }
            p.css({
                margin: "auto",
                position: "absolute",
                "z-index": -1,
                top: d.y,
                left: d.x,
                "-webkit-transform": "translate(-" + d.x + ", -" + d.y + ")",
                "-ms-transform": "translate(-" + d.x + ", -" + d.y + ")",
                "-moz-transform": "translate(-" + d.x + ", -" + d.y + ")",
                transform: "translate(-" + d.x + ", -" + d.y + ")",
                visibility: "hidden",
                opacity: 0
            }).one("canplaythrough.vide", function () {
                e.resize()
            }).one("playing.vide", function () {
                p.css({
                    visibility: "visible",
                    opacity: 1
                }), u.css("background-image", "none")
            }), l.on("resize.vide", function () {
                a.resizing && e.resize()
            }), u.append(p)
        }, o.prototype.getVideoObject = function () {
            return this.$video[0]
        }, o.prototype.resize = function () {
            if (this.$video) {
                var e = this.$wrapper,
                    t = this.$video,
                    i = t[0],
                    o = i.videoHeight,
                    s = i.videoWidth,
                    n = e.height(),
                    r = e.width();
                r / s > n / o ? t.css({
                    width: r + 2,
                    height: "auto"
                }) : t.css({
                    width: "auto",
                    height: n + 2
                })
            }
        }, o.prototype.destroy = function () {
            delete $.vide.lookup[this.index], this.$video && this.$video.off("vide"), this.$element.off("vide").removeData("vide"), this.$wrapper.remove()
        }, $.vide = {
            lookup: []
        }, $.fn.vide = function (e, t) {
            var i;
            return this.each(function () {
                i = $.data(this, "vide"), i && i.destroy(), i = new o(this, e, t), i.index = $.vide.lookup.push(i) - 1, $.data(this, "vide", i)
            }), this
        }, $(document).ready(function () {
            var e = $(window);
            e.on("resize.vide", function () {
                for (var e = $.vide.lookup.length, t = 0, i; t < e; t++)(i = $.vide.lookup[t]) && i.settings.resizing && i.resize()
            }), e.on("unload.vide", function () {
                return !1
            }), $(document).find("[data-vide-bg]").each(function (e, t) {
                var i = $(t),
                    o = i.data("vide-options"),
                    s = i.data("vide-bg");
                i.vide(s, o)
            })
        })
    });
var MCSignUp = function () {
    var e = $("#mc-embedded-subscribe-form"),
        t = $("#mc-embedded-subscribe"),
        i = $("body"),
        o = $(".signup-notice"),
        s = 8150,
        n = "Thank you for signing up. To complete the process, please click the 'opt-in' link in the email we've just sent you.";
    return {
        init: function () {
            this.bind()
        },
        bind: function () {
            t.on("click", function (e) {
                e && e.preventDefault(), MCSignUp.registerForm()
            })
        },
        registerForm: function () {
            $.ajax({
                type: e.attr("method"),
                url: e.attr("action"),
                data: e.serialize(),
                timeout: 5e3,
                cache: !1,
                dataType: "jsonp",
                contentType: "application/json; charset=utf-8",
                error: function (e) {
                    $(".signup-notice").html('<span class="alert">Sorry, please try again.</span>')
                },
                success: function (e) {
                    var t;
                    "success" !== e.result ? (t = e.msg.substring(), i.addClass("signup--error"), $(".signup-notice").html('<span class="signup-notice__message">' + t + "</span>"), $(".signup-form__input").addClass("error")) : (t = "Thank you for signing up. To complete the process, please click the 'opt-in' link in the email we've just sent you.", i.addClass("signup--success"), o.html('<span class="signup-notice__message">' + t + "</span>"), setTimeout(function () {
                        i.removeClass("signup--error signup--success"), PopItUp.closePopUp()
                    }, 8150))
                }
            })
        }
    }
}();
MCSignUp.init();
var site = {
    featureJS: function () {
        $("html").removeClass("no-js"), feature.svg || $("html").addClass("no-svg"), feature.cssFlexbox || $("html").addClass("no-flexbox")
    },
    plax: function () {
        $(".js-parallax").parallax(6, "false")
    },
    mastVids: function () {
        $(".bg_vid video").fadeIn(400)
    },
    sliders: function () {
        $(".js-slider-content").slick({
            infinite: !0,
            autoplay: !0,
            fade: !0,
            autoplaySpeed: 5e3,
            speed: 1e3,
            arrows: !0,
            dots: !0,
            prevArrow: "<span class='slick-arrow slick-arrow--prev'><i class='icon-left-chev'></i></span>",
            nextArrow: "<span class='slick-arrow slick-arrow--next'><i class='icon-right-chev'></i></span>"
        }), $(".js-slider-imgs").slick({
            infinite: !0,
            autoplay: !0,
            fade: !1,
            autoplaySpeed: 3e3,
            arrows: !0,
            dots: !0,
            prevArrow: "<span class='slick-arrow slick-arrow--prev'><i class='icon-left-chev'></i></span>",
            nextArrow: "<span class='slick-arrow slick-arrow--next'><i class='icon-right-chev'></i></span>"
        })
    },
    lazy: function () {
        $(".js-lazy").unveil(10, function () {
            $(this).load(function () {
                $(this).parent().addClass("loaded")
            })
        })
    }
};
$(function () {
    site.featureJS(), $(".js-parallax").length && site.plax(), $(".bg_vid").length && site.mastVids(), $('[class*="js-slider"]').length && site.sliders(), $(".js-lazy").length && site.lazy()
});