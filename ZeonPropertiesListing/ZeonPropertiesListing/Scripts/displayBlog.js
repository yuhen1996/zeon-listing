﻿$("#btnSubmit").click(function () {

    var urlurl = $("#delBlog").val();//get url
    var blogId = $("#blogIdd").val();//get id

    $.ajax({
        type: "POST",
        url: urlurl,
        data: { id: blogId },
        success: function () {
            $("#myModal").modal("hide");
            console.log("Success delete");
            location.reload();//reload page
        },
        error: function (x, e) { //xhr, status, p3, p4

            if (x.status === 0) {
                console.log('You are offline!!\n Please Check Your Network.');
            } else if (x.status === 404) {
                console.log('Requested URL not found.');
            } else if (x.status === 500) {
                console.log('Internel Server Error.');
            } else if (e === 'parsererror') {
                console.logt('Error.\nParsing JSON Request failed.');
            } else if (e === 'timeout') {
                console.log('Request Time out.');
            } else {
                console.log('Unknow Error.\n' + x.responseText);
            }
        }

    })
})//end btnSubmit

function passToModal(lnk) {
    var val = lnk.getAttribute('value');//get vaue from delete link
    document.getElementById('blogIdd').value = val;//display in textbox in modal
}