﻿//--------------------------------------------------CHECK/UNCHECK  CHECKBOXES

function SetAllCheckBoxes(obj) {
    var c = new Array();
    c = document.getElementsByName('cond_chck');
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox') {
            c[i].checked = obj.checked;
        }
    }
}

//-------------------------------------------------------CLEAR ERR MESSAGE WHEN LOAD dropdown
$("#State").change(function () {
    document.getElementById('stateerr').innerHTML = "";//clear err
});
$("#PropertyType").change(function () {
    document.getElementById('propertyerr').innerHTML = "";//clear err
});
//-------------------------------------------Populate area, on select state dropdown
$(document).ready(function () {
    $('#State').on('change',
        function () {
            var populateArea = $("#populateArea").val();
          
            var stateName = $('#State option:selected').val();
            $.ajax({
                type: 'GET',
                data: { stateName: stateName },
                url: populateArea,
                success: function (result) {
                    //var area = JSON.parse(result);
                    var a = '<option value="">---Select Area--- </option>';
                    for (var i = 0; i < result.length; i++) {

                        a += '<option value = "' + result[i].aName + '">' + result[i].aName + '</option>';
                    }
                    $('#comboBoxArea').html(a);
                }
            });
        });
});


//-------------------------------------------------------VALIDATION + Comma
//$('#UnitNo').keyup(function (event) {
//    // skip for arrow keys
//    if (event.which >= 37 && event.which <= 40) return;
//    // format number
//    $(this).val(function (index, value) {
//        return value
//            .replace(/[^A-Za-z 0-9-]/g, "")// ALPHABET AND NUMBERS only
//            ;
//    });
//});
//insert comma when user typing price related txtbox (using ID)
$('#ListingPrice, #Maintenance, #Sinking').keyup(function (event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    $(this).val(function (index, value) {
        return value
            // .replace(/[^0-9.]/g, "") to allow number only, no -ve, with decimal
            //.replace(/[^A-Za-z']/g, "") allow only alphbet
            //.replace(/[^A-Za-z0-9]/g, "") ALPHABET AND NUMBERS only

            .replace(/[^0-9.]/g, "")//numbers only, with decimal
            .replace(/(\..*)\./g, '$1') //no more than 1 decimal point
            .replace(/(\.[\d]{2})./g, '$1')//no more than 2 deciaml places
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")//thousand separator
            ;
    });
});
$('#room, #bathroom, #carpark').keyup(function (event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    $(this).val(function (index, value) {
        return value
            .replace(/[^0-9]/g, "")// to allow number only, no -ve, no decimal
            ;
    });
}); 

$('#ownerNme').keyup(function (event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    $(this).val(function (index, value) {
        return value
             .replace(/[^A-Za-z ]/g, "") //allow only alphbet and spaces
            ;
    });
}); 
$('#ownerContact, #OwnerContact2').keyup(function (event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    $(this).val(function (index, value) {
        return value
          .replace(/[^0-9- ]/g, "")// to allow number only, and dash
            ;
    });
}); 
// ownerMail , 
$('#BuildUp, #LandArea').keyup(function (event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;
    // format number
    $(this).val(function (index, value) {
        return value
            .replace(/[^0-9.]/g, "")// to allow number only, and dot only
            .replace(/(\..*)\./g, '$1') //no more than 1 decimal point
            .replace(/(\.[\d]{2})./g, '$1')//no more than 2 deciaml places
            ;
    });
}); 
//-------------------------------------------------------ON CLICK PROPRTY TYPE
$("#PropertyType").change(function () {
   
    var whole = document.getElementById("hideshow");// get div's id
    whole.style.display = "block";//change display -> block
    //clear all textbox, files input
    document.getElementById('UnitNo').value = "";
    document.getElementById('AdsTitle').value = "";
    document.getElementById('fileimg').value = "";//upload img file control
    if (fileBuffer.length > 0) {
        fileBuffer = []; //clear filebuffer array
        console.log("=>"+fileBuffer.length);
    }
   
    document.getElementById('fList').innerHTML = "";
    document.getElementById('attach').value = "";
    if (docBuffer.length > 0) {
        docBuffer = []; //clear filebuffer array
        console.log("=>" + docBuffer.length);
    }
    document.getElementById('attacherr').innerHTML = "";
    document.getElementById('ownerNme').value = "";
    document.getElementById('ownerContact').value = "";
    document.getElementById('ownerMail').value = "";
    document.getElementById('address').value = "";
    document.getElementById('ListingPrice').value = "";
    document.getElementById('room').value = "";
    document.getElementById('bathroom').value = "";
    document.getElementById('carpark').value = "";
    document.getElementById('Maintenance').value = "";
    document.getElementById('Sinking').value = "";
    document.getElementById('completionDate').value = "";
    document.getElementById('BuildUp').value = "";
    document.getElementById('LandArea').value = "";
    CKEDITOR.instances['description'].setData('');
    document.getElementById('remarks').value = ""; document.getElementById('ConditionErr').innerHTML = "";
    document.getElementById('AddressErr').innerHTML = ""; document.getElementById('UnitNoErr').innerHTML = "";
    document.getElementById('TenureErr').innerHTML = ""; document.getElementById('TitleErr').innerHTML = "";
    document.getElementById('areaerr').innerHTML = "";

    //dropdown
    $("#State")[0].selectedIndex = 0; $("#comboBoxArea")[0].selectedIndex = 0;

    $("#ListingType")[0].selectedIndex = 0;
    $("#other")[0].selectedIndex = 0;

    //checkbox
    $('input[type=checkbox]').prop('checked', false);

    //radio button
    $('input[type=radio]').prop('checked', false);
    //clear all err message
    document.getElementById('LandErr').innerHTML = ""; document.getElementById('BuildupErr').innerHTML = "";
    document.getElementById('CarparkErr').innerHTML = ""; document.getElementById('BathroomErr').innerHTML = "";
    document.getElementById('RoomErr').innerHTML = ""; document.getElementById('propertyerr').innerHTML = "";
    document.getElementById('AdsTitleErr').innerHTML = ""; document.getElementById('fileerr').innerHTML = "";
    document.getElementById('stateerr').innerHTML = ""; document.getElementById('listingterr').innerHTML = "";
    document.getElementById('ListingPriceErr').innerHTML = ""; document.getElementById('MaintenanceErr').innerHTML = "";
    document.getElementById('SinkingErr').innerHTML = ""; document.getElementById('ownerNmeErr').innerHTML = ""; 
 document.getElementById('ownerContactErr').innerHTML = "";   document.getElementById('ownerMailErr').innerHTML = ""; 


    var select = $('#PropertyType').val();//get dropdown selected value
    var x = document.getElementById("propertyType2div");//get div Id (to display or hide)

    var isLand = document.getElementById("isLand");// get div's id
    var notLand = document.getElementById("notLandrow");// get div's id
    var buildupdiv = document.getElementById("buildupdiv");// get div's id
    var attachmentdiv = document.getElementById("attachmentdiv");// get div's id
    var populateUrl = $("#populateUrl").val();
    //hide or show PropertyType2
    if (select == "Bungalow" || select == "Semi Detached"
        || select == "Terrace House" || select == "Shop House") {
        //display
        x.style.display = "block";

        $("#PropertyType2").empty();
        $.ajax({
            type: "post",
            dataType: "json",
           // url: '@Url.Action("populate", "Listing")', // "/Listing/Create" 
            url: populateUrl,
            data: { selected: select },//insert formdata???
            success: function (sel) {
                $.each(sel, function (i, sels) {
                    console.log("Success ");
                    $("#PropertyType2").append('<option value="' + sels.Value + '">' +
                        sels.Text + '</option>');
                    // here we are adding option for States
                });
            },
            error: function (data) { //xhr, status, p3, p4
                console.log("Err: ");

            }
        });//end $ajax
    }//end if
    else {
        x.style.display = "none";
        //$("#PropertyType2").empty();//This cause error
        $("PropertyType2").each(function () { this.selectedIndex = 0 });
    }//end else

    //hide or show Build up area or room no, bathroom no etc..
    if (select == "Industrial Land" || select == "Development Land" || select == "Agriculture Land") {
        //hide not-land related input
        buildupdiv.style.display = "none";
        notLandrow.style.display = "none";//change display -> block
        maintenancediv.style.display = "none";//hde maintenance div
        sinkingdiv.style.display = "none";//hide sinking div

        //display
        attachmentdiv.style.display = "";//show attachment div
        isLand.style.display = "";//show LandArea div
    }
    else {
        //hide land related input
        attachmentdiv.style.display = "none";//hide attachment div
        isLand.style.display = "none";//change display -> block

        //display
        buildupdiv.style.display = "";
        notLandrow.style.display = "";//change display -> block
        maintenancediv.style.display = "";//display maintenance div
        sinkingdiv.style.display = "";//display sinking div
       

    }


});
//   when click UPLOAD button
function loadimage() {
    document.getElementById('fileimg').click();
    document.getElementById('fileerr').innerHTML = "";//clear err
}
//   when click ATTACHMENT button
function attachFileBtn() {
    document.getElementById('attach').click();
    document.getElementById('attacherr').innerHTML = "";//clear err
}
//-------------------------------------------------------when UPLOAD ATTACHMENT input
var docBuffer = [];//for file/attachment
//this is upoad file control onchange
updateAttach = function () {
    var valAttach = [];//empty valAttach[] onload and again
    var input = document.getElementById('attach');// <input type="file" />
    var re = /(?:\.([^.]+))?$/;

    Array.prototype.push.apply(valAttach, input.files);//push bunch of files into valAttach[]
    for (var i = 0; i < valAttach.length; ++i) {//loop out each file
        var nam = valAttach[i].name;//get each file name and assign to var
        var extension = re.exec(nam)[1];//extract extension name
        //if file is jpg and png
        if (extension == "jpg" || extension == "png" || extension == "gif" || extension == "tif"
            || extension == "jpeg") {
            //do nothing
        }//end if
        else {
            docBuffer.push(valAttach[i]);//push not-img-file into docBuffer arr
        }


    }
    // Append/Add the whole file list into an array

    document.getElementById('docList').innerHTML = "";//clear previousdoc details
    for (var i = 0; i < docBuffer.length; ++i) {
        ReadDoc(docBuffer[i], i);//Read document details
    }
}//end updateList()

//-------------------------------------------------------Display file/attachment
var ReadDoc = function (file, i) {

    var span = document.createElement('span');
    span.innerHTML = ['<div class="row"><div>', file.name, '</div>',
        '<span id="', i, '" class="remove_file_preview"></span></div>'].join('');
    document.getElementById('docList').insertBefore(span, null);

}
//-------------------------------------------------------REMOVE File/Attachment
$('#docList').on('click', '.remove_file_preview', function () {
    console.log("deleted file frm array");
    //clear previous added image
    document.getElementById('docList').innerHTML = "";
    //get Id
    var indexToDel = $(this).attr('id');//id is string

    var b = parseInt(indexToDel);
    docBuffer.splice(indexToDel, 1);
    for (var i = 0; i < docBuffer.length; ++i) {
        ReadDoc(docBuffer[i], i);//ReadDoc() again
    }

});
//-------------------------------------------------------when UPLOAD IMG file input
//declare array
var fileBuffer = [];//for img

updateList = function () {
    var valImg = [];//empty valImg[] onload and again
    var input = document.getElementById('fileimg');// <input type="file" />
    var output = document.getElementById('fileList');// <output>
    /*validate files type*/
    var re = /(?:\.([^.]+))?$/;
    Array.prototype.push.apply(valImg, input.files);//push bunch of files into valImg[]

    for (var i = 0; i < valImg.length; ++i) {//loop out each file
        var nam = valImg[i].name;//get each file name and assign to var
        var extension = re.exec(nam)[1];//extract extension name

        //if file is jpg and png
        if (extension == "jpg" || extension == "png" || extension == "gif" || extension == "tif"
            || extension == "jpeg") {
            fileBuffer.push(valImg[i]);//push to fileBuffer[]
        }//end if
    }
    // Append/Add the whole file list into an array
    //Array.prototype.push.apply(fileBuffer, input.files);

    document.getElementById('fList').innerHTML = "";//clear previous added image
    for (var i = 0; i < fileBuffer.length; ++i) {
        ReadImage2(fileBuffer[i], i);//ReadImage2() ; read image
    }
}//end updateList()

//-------------------------------------------------------Display img function
var ReadImage2 = function (file, i) {
    var reader = new FileReader;
    var image = new Image;

    reader.readAsDataURL(file);
    reader.onload = function (_file) {

        image.src = _file.target.result;
        image.onload = function () {

            var span = document.createElement('span');
            span.innerHTML = ['<img class="thumb" src="', _file.target.result
                , '" /><span id="', i, '" class="remove_img_preview"></span>'].join('');
            document.getElementById('fList').insertBefore(span, null);
        }//end image.onload
    }
}

//-------------------------------------------------------REMOVE IMAGE
$('#fList').on('click', '.remove_img_preview', function () {

    //clear previous added image
    document.getElementById('fList').innerHTML = "";
    //get Id
    var indexToDel = $(this).attr('id');//id is string

    var b = parseInt(indexToDel);
    fileBuffer.splice(indexToDel, 1);
    for (var i = 0; i < fileBuffer.length; ++i) {
        console.log("IMG deleted frm array");
        ReadImage2(fileBuffer[i], i);//ReadImage2()
    }
    
});

//-------------------------------------------------------CRAETE BUTTON -> ASSIGN IMAGE, validate
$("#createbtn").click(function () {

    var c = fileBuffer.length;//upload img input
    var docInput = docBuffer.length;//upload file input
    var token = 0;
    var landproperty = 0;//check if land property is selected or not

    var select = $('#PropertyType').val();//get dropdown selected value
    if (select == "Industrial Land" || select == "Development Land" || select == "Agriculture Land") {

        landproperty = 1;

        //if ($('#LandArea').val() == "") {//
        //    document.getElementById('LandErr').innerHTML = "Require Land Area";
        //    token = 1;
        //} else { document.getElementById('LandErr').innerHTML = ""; }

        if (docInput == 0) {//check atcchement 
            document.getElementById('attacherr').innerHTML = "You need to upload attachment!!!";
            token = 1;
        } else { document.getElementById('attacherr').innerHTML = ""; }
    } else {//validate if not-land  are selected -> display error or clear error

      
        //if ($('#BuildUp').val() == "") {//
        //    document.getElementById('BuildupErr').innerHTML = "Require Build Up Area";
        //    token = 1;
        //} else { document.getElementById('BuildupErr').innerHTML = ""; }
         //disable empty validation in carpark, bathroom, room, sinking, maintenance
        //if ($('#carpark').val() == "") {
        //    document.getElementById('CarparkErr').innerHTML = "Require Carpark No";
        //    token = 1;
        //} else { document.getElementById('CarparkErr').innerHTML = ""; }
        //if ($('#bathroom').val() == "") {
        //    document.getElementById('BathroomErr').innerHTML = "Require Bathroom No";
        //    token = 1;
        //} else { document.getElementById('BathroomErr').innerHTML = ""; }
        //if ($('#room').val() == "") {
        //    document.getElementById('RoomErr').innerHTML = "Require Room No";
        //    token = 1;
        //} else { document.getElementById('RoomErr').innerHTML = ""; }
        //if ($('#Sinking').val() == "") {
        //    document.getElementById('SinkingErr').innerHTML = "Require Sinking Fund";
        //    token = 1;
        //} else { document.getElementById('SinkingErr').innerHTML = ""; }
        //if ($('#Maintenance').val() == "") {
        //    document.getElementById('MaintenanceErr').innerHTML = "Require Maintenance Fee";
        //    token = 1;
        //} else { document.getElementById('MaintenanceErr').innerHTML = ""; }

    }//end else

    if (c == 0) {//check img file
        document.getElementById('fileerr').innerHTML = "You need to insert image!!!";
        token = 1;
    } else { document.getElementById('fileerr').innerHTML = ""; }
    
    //display error if have error else clear error message
    if ($('#PropertyType').val() == "") {
        document.getElementById('propertyerr').innerHTML = "Require Property Type";
        token = 1;
    } else { document.getElementById('propertyerr').innerHTML = ""; }
    if ($('#State').val() == "") {
        document.getElementById('stateerr').innerHTML = "Require State";
        token = 1;
    } else { document.getElementById('stateerr').innerHTML = ""; }
    if ($('#comboBoxArea').val() == "") {
        document.getElementById('areaerr').innerHTML = "Require Area";
        token = 1;
    } else { document.getElementById('areaerr').innerHTML = ""; }
   
    if ($('#ListingType').val() == "") {//radio butoon, validation not working
        document.getElementById('listingterr').innerHTML = "Require Listing Type";
        token = 1;
    } else { document.getElementById('listingterr').innerHTML = ""; }
    
    if ($('#ListingPrice').val() == "") {
        document.getElementById('ListingPriceErr').innerHTML = "Require Price";
        token = 1;
    } else { document.getElementById('ListingPriceErr').innerHTML = ""; }


    if ($('#AdsTitle').val() == "" || $('#AdsTitle').val().trim() == "") {
        document.getElementById('AdsTitleErr').innerHTML = "Require AdsTitle";
        token = 1;
    } else { document.getElementById('AdsTitleErr').innerHTML = ""; }
    if ($('#ownerNme').val() == "" || $('#ownerNme').val().trim() == "") {
        document.getElementById('ownerNmeErr').innerHTML = "Require Owner Name";
        token = 1;
    } else { document.getElementById('ownerNmeErr').innerHTML = ""; }
    if ($('#ownerContact').val() == "" || $('#ownerContact').val().trim() == "") {
        document.getElementById('ownerContactErr').innerHTML = "Require Owner's Contact";
        token = 1;
    } else { document.getElementById('ownerContactErr').innerHTML = ""; }
    //if ($('#ownerMail').val() == "" || $('#ownerMail').val().trim() == "") {
    //    document.getElementById('ownerMailErr').innerHTML = "Require Owner's Contact";
    //    token = 1;
    //} else {
    //    document.getElementById('ownerMailErr').innerHTML = "";
    //}
    if ($('#UnitNo').val() == "" || $('#UnitNo').val().trim() == "") {
        document.getElementById('UnitNoErr').innerHTML = "Require Unit Number";
        token = 1;
    } else {
        document.getElementById('UnitNoErr').innerHTML = "";
    }
    if ($('#address').val() == "" || $('#address').val().trim() == "") {
        document.getElementById('AddressErr').innerHTML = "Require Owner's Address";
        token = 1;
    } else {
        document.getElementById('AddressErr').innerHTML = "";
    }
    /*validate checkbox*/
    var chks = document.getElementsByName('cond_chck');
    var hasChecked = false;
    for (var i = 0; i < chks.length; i++) {
        if (chks[i].checked) {
            hasChecked = true; document.getElementById('ConditionErr').innerHTML = "";
        }
    }
    if (hasChecked == false) {
        document.getElementById('ConditionErr').innerHTML = "Please select at least one Condition";
        token = 1;
    }
    //end validate checkbox
    /*validate radiobutton (TENURE)*/
        //var rbtDetail = document.getElementsByName("tenure");
        //var isValid = false;
        //for (var i = 0; i < rbtDetail.length; i++) {
        //    if (rbtDetail[i].checked) {
        //        isValid = true;
               
        //    }
        //}
        //if (isValid == true) {
        //    document.getElementById('TenureErr').innerHTML = "";
           
        //}
        //else {
        //    document.getElementById('TenureErr').innerHTML = "Require Tenure";
        //    token = 1;
        //}
    //end validate radiobutton
    //validate radiobutton (TITLE)
        //var rbtDetail = document.getElementsByName("title");
        //var isValid = false;
        //for (var i = 0; i < rbtDetail.length; i++) {
        //    if (rbtDetail[i].checked) {
        //        isValid = true;

        //    }
        //}
        //if (isValid == true) {
        //    document.getElementById('TitleErr').innerHTML = "";

        //}
        //else {
        //    document.getElementById('TitleErr').innerHTML = "Require title";
        //    token = 1;
        //}
    //end validate radiobutton

    if (token == 1) {

        document.documentElement.scrollTop = 0;//jump back to top of page. This gave user the illusion tat error is detected
       
        return false;
    } else{

        if (landproperty == 1) {

        
        //Upload file 
        var formData2 = new FormData();

        for (var i = 0; i < docBuffer.length; i++) {
            formData2.append("attachment", docBuffer[i]);//append docBuffer to formdata
        }
        var uploadDoc = $("#uploadDoc").val();
        //-------------------
       
         //-------------------
        $.ajax({
            type: "post",
            dataType: "json",
            url: uploadDoc,//upload document url
            traditional: true,
            data: formData2,//insert formdata2
            processData: false,
            contentType: false,
            success: function (data) {
                //read array image -> display
                //document.getElementById('fList').innerHTML = data;
                console.log("file success ->");
                
            },
            error: function (x, e) { //xhr, status, p3, p4

                if (x.status == 0) {
                    console.log('UploadFile: You are offline!!\n Please Check Your Network.');
                } else if (x.status == 404) {
                    console.log('UploadFile: Requested URL not found.');
                } else if (x.status == 500) {
                    console.log('UploadFile: Internel Server Error.');
                } else if (e == 'parsererror') {
                    console.log('UploadFile: Error.\nParsing JSON Request failed.');
                } else if (e == 'timeout') {
                    console.log('UploadFile: Request Time out.');
                } else {
                    console.log('UploadFile: Unknow Error.\n' + x.responseText);
                }
            }
        });//end upload file $ajax
        }//end if landproperty

    //BELOW: pass image array to controller
    var formData = new FormData();

    for (var i = 0; i < c; i++) {
        formData.append("image", fileBuffer[i]);//append to formdata
    }
    var ImgToController = $("#passToController").val();
    $.ajax({
        type: "post",
        dataType: "json",
       // url: '@Url.Action("saveImage", "Listing")', 
        url: ImgToController,
        traditional: true,
        data: formData,//insert formdata???
        processData: false,
        contentType: false,
        success: function (data) {
            console.log("Image is Success");
        },
        error: function (x, e) { //xhr, status, p3, p4

            if (x.status == 0) {
                console.log('You are offline!!\n Please Check Your Network.');
            } else if (x.status == 404) {
                console.log('Requested URL not found.');
            } else if (x.status == 500) {
                console.log('Internel Server Error.');
            } else if (e == 'parsererror') {
                console.log('Error.\nParsing JSON Request failed.');
            } else if (e == 'timeout') {
                console.log('Request Time out.');
            } else {
                console.log('Unknow Error.\n' + x.responseText);
            }
        }
    });//end upload image $ajax
   
    /*
        if (window.FormData !== undefined) {

        } else {
        alert("This browser doesn't support HTML5 file uploads!");
        }
       
    */
    }//end else
});//end create button
