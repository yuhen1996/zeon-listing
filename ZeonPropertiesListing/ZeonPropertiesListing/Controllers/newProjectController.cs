﻿using ZeonPropertiesListing.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.Drawing;
using ZeonPropertiesListing.Controllers;

namespace FilterList.Controllers
{
    [SessionExpire]
    public class newProjectController : Controller
    {
        // dBContext
        Overriding_DBEntities dbModel = new Overriding_DBEntities();

        //This is New Project Dashboard. This display all new project+Filter+Search
        public ActionResult Index()
        {
            //get dropdown values from custom method
            ViewBag.DropDownCategories = GetProjectName();
            //retrieve data from database
            var alldata = dbModel.M_PropertyUnit.OrderByDescending(x => x.ID).ToList();

            //return data to view
            return View(alldata);
        }
        //This is a Delete action
        [HttpPost]
        public ActionResult Index(string txtID)//txtID is pass from modal's hidden textbox
        {
            //get dropdown values
            ViewBag.DropDownCategories = GetProjectName();

            var item = dbModel.M_PropertyUnit.Where(x => x.ID == txtID).FirstOrDefault();

            if (item != null)
            {
                dbModel.M_PropertyUnit.Remove(item);
                dbModel.SaveChanges();
            }
            dbModel.SaveChanges();

            //repull from databse again
            var list = dbModel.M_PropertyUnit.OrderByDescending(x => x.ID).ToList();

            return View(list);
        }
        //Export to Excel
        public ActionResult ExportToExcel()
        {
            var propertyNmeVal = "";

            var searchString = "";
            //this session was assign in Filter()
            if (!string.IsNullOrEmpty(Session["propertyName"] as string))
            {
                propertyNmeVal = Session["propertyName"].ToString();
            }
            if (!string.IsNullOrEmpty(Session["searchString"] as string))
            {
                searchString = Session["searchString"].ToString();
            }

            //retrieve ALL data from database
            var projDashboard = from s in dbModel.M_PropertyUnit
                                select s;

            if (!string.IsNullOrEmpty(propertyNmeVal))
            {

                projDashboard = projDashboard.Where(x => x.PropertyName == propertyNmeVal.ToString());

            }
            //if display all property Name
            if (propertyNmeVal == "all" || propertyNmeVal == "")
            {
                projDashboard = dbModel.M_PropertyUnit;
            }

            if (!string.IsNullOrEmpty(searchString))
            {

                projDashboard = projDashboard.Where(x => x.UnitNo.Contains(searchString.ToString()) ||
                                                    x.UnitStatus.Contains(searchString.ToString()) ||
                                                    x.Floor.Contains(searchString.ToString()) ||
                                                    x.Block.Contains(searchString.ToString()));
            }

            //after filtered -> save to a list
            List<M_PropertyUnit> tblVal = projDashboard.OrderBy(x => x.ID).ToList();

            //Start Exporting to Excel
            var totalColumn = 6;

            //declare Excel
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

            //Note: .Cells[FromRow, FromColumn, ToRow, ToColumn]
            //This will style the cells in excel
            ws.Cells[2, 1, 2, totalColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            ws.Cells[2, 1, 2, totalColumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("darkred")));
            ws.Cells[2, 1, 2, totalColumn].Style.Font.Bold = true;

            ws.Cells["A2"].Value = "Property Name";
            ws.Cells["B2"].Value = "Unit Number";
            ws.Cells["C2"].Value = "Unit Status";
            ws.Cells["D2"].Value = "Floor";
            ws.Cells["E2"].Value = "Block";
            ws.Cells["F2"].Value = "Price";


            //style font color of the header. 5 = number of column. 2 = 2nd row (which is the header)
            for (var i = 1; i <= totalColumn; i++)
            {
                ws.Cells[2, i].Style.Font.Color.SetColor(Color.White);//set color
            }

            //rowstart: specifies start at which row in Excel
            int rowstart = 3;




            foreach (var item in tblVal)
            {
                if (item.UnitStatus == "Occupied")
                {
                    //fill the row background with color
                    ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("salmon")));
                }
                else
                {
                    //fill the row background with color
                    ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("lightgreen")));
                }

                //start write in Excel cells
                ws.Cells[string.Format("A{0}", rowstart)].Value = item.PropertyName;
                ws.Cells[string.Format("B{0}", rowstart)].Value = item.UnitNo;
                ws.Cells[string.Format("C{0}", rowstart)].Value = item.UnitStatus;
                ws.Cells[string.Format("D{0}", rowstart)].Value = item.Floor;
                ws.Cells[string.Format("E{0}", rowstart)].Value = item.Block;
                ws.Cells[string.Format("F{0}", rowstart)].Value = Convert.ToDecimal(item.Price).ToString("#,##0.00");

                rowstart++;//increment rowstart num in every loop, so that after it finish write 1 row, it will move to another row and write
            }//end foreach

            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}", "NewProject.xlsx"));
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();

            return View();
        }

        //display Create pg
        public ActionResult Create()
        {
            return View();
        }
        //Create new project
        [HttpPost]
        public ActionResult Create(M_PropertyUnit createPg)//createPg consist values pass from create page
        {
            var strguid = Guid.NewGuid();
            createPg.ID = strguid.ToString();


            if (HttpContext.Request.Form.AllKeys.Contains("Save"))
            {
                if (createPg != null)
                {
                    //createPg.UnitStatus = "Vacant";
                    string priceComma = Request.Form["Price"].ToString();
                    if (!string.IsNullOrEmpty(priceComma))
                    {
                        priceComma = priceComma.Replace(",", string.Empty);//replace with empty
                        decimal price = decimal.Parse(priceComma);
                        createPg.Price = price;
                    }
                    dbModel.M_PropertyUnit.Add(createPg);

                    dbModel.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            if (HttpContext.Request.Form.AllKeys.Contains("SaveNext"))
            {
                if (createPg != null)
                {

                    string priceComma = Request.Form["Price"].ToString();
                    if (!string.IsNullOrEmpty(priceComma))
                    {
                        priceComma = priceComma.Replace(",", string.Empty);//replace with empty
                        decimal price = decimal.Parse(priceComma);
                        createPg.Price = price;
                    }
                    dbModel.M_PropertyUnit.Add(createPg);

                    dbModel.SaveChanges();
                    ModelState.Clear();
                    // newprojectTbl.UnitNo = "";
                    ViewBag.createSuccess = "New Project is Added";
                    // this will save 
                    return View(createPg);
                }
            }

            return View();
        }
        //display Edit Page
        public ActionResult Edit(string id)
        {
            M_PropertyUnit result = new M_PropertyUnit();
            if (id != null)
            {
                //pull frm database
                result = dbModel.M_PropertyUnit.Where(x => x.ID == id).FirstOrDefault();
                ViewBag.gtPrice = Convert.ToDecimal(result.Price).ToString("#,##0.00");

            }
            else
            {
                //if no Id is detected -> redirect user to dashboard
                return RedirectToAction("Index");
            }

            return View(result);
        }
        //Edit New Project
        [HttpPost]
        public ActionResult Edit(M_PropertyUnit editPg, string Price2)
        {
            //editPg.ID means ID is pass from Edit View
            //it is use to search for the record
            M_PropertyUnit project = dbModel.M_PropertyUnit.Where(x => x.ID == editPg.ID).FirstOrDefault();



            if (project != null)//if record is return
            {
                project.PropertyName = editPg.PropertyName;
                project.UnitNo = editPg.UnitNo;
                project.UnitStatus = editPg.UnitStatus;
                project.Floor = editPg.Floor;
                project.Block = editPg.Block;

                string priceComma1 = Price2;
                priceComma1 = priceComma1.Replace(",", string.Empty);//replace with empty
                decimal price1 = decimal.Parse(priceComma1);
                project.Price = price1;


                dbModel.Entry(project).State = EntityState.Modified;
                dbModel.SaveChanges();
                ViewBag.editSuccess = "Edit Success";
                ViewBag.gtPrice = Convert.ToDecimal(project.Price).ToString("#,##0.00");


            }
            //M_PropertyUnit projectValue = dbModel.M_PropertyUnit.Where(x => x.ID == editPg.ID).FirstOrDefault();

            return View(project);
        }

        //Filter action, also use for search
        public ActionResult Filter(string drpType, string searchString)
        {
            //Get Property Name dropdown values
            ViewBag.DropDownCategories = GetProjectName();
            //get the selected dropdown value -> assign to viewbag
            ViewBag.Categories = drpType;
            //assign to session, this will be use in ExportToExcel() later
            Session["propertyName"] = drpType;
            Session["searchString"] = searchString;
            //record searched string and pass to view so that it will redisplay searched values
            ViewBag.CurrentSearch = searchString;
            //retrieve ALL data from database
            var projDashboard = from s in dbModel.M_PropertyUnit
                                select s;

            if (drpType != null || drpType != "")
            {
                projDashboard = projDashboard.Where(x => x.PropertyName == drpType);
            }
            //if display all property Name
            if (drpType == "all")
            {
                projDashboard = dbModel.M_PropertyUnit;
            }

            if (searchString != null || searchString != "")
            {
                projDashboard = projDashboard.Where(x => x.UnitNo.Contains(searchString) ||
                                                    x.UnitStatus.Contains(searchString) ||
                                                    x.Floor.Contains(searchString) ||
                                                    x.Block.Contains(searchString));
            }

            //This return the data into the view
            return View("Index", projDashboard.ToList());
        }

        //a custome method: use to repopulate dropdown
        public List<string> GetProjectName()
        {
            return dbModel.M_PropertyUnit.Select(x => x.PropertyName).Distinct().ToList();
        }

    }
}