﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZeonPropertiesListing.Models;
namespace ZeonPropertiesListing.Controllers
{
    public class HomeController : Controller
    {
        Overriding_DBEntities db = new Overriding_DBEntities();

        
        public ActionResult Index(string adstitle, string listingtype, string state, string urgent_chck, decimal? pricelessthan400, string apartment)
        {
           
            
            var listing = from l in db.L_ListingTbl select l;
            
            if(!string.IsNullOrEmpty(adstitle))
            {
                listing = listing.Where(l => l.AdsTitle.Contains(adstitle));
            }
            if(!string.IsNullOrEmpty(listingtype))
            {
                listing = listing.Where(l => l.ListingType == listingtype);
            }

            if (!string.IsNullOrEmpty(state))
            {
                listing = listing.Where(l => l.State == state);
            }

            if(!string.IsNullOrEmpty(urgent_chck))
            {
                listing = listing.Where(l => l.UrgentListing == urgent_chck);
            }

            if(pricelessthan400 != null)
            {
                listing = listing.Where(l => l.ListingType == "Sale" && (l.ListingPrice < pricelessthan400 || l.ListingSale < pricelessthan400));
            }

            if(!string.IsNullOrEmpty(apartment))
            {
                listing = listing.Where(l => l.PropertyType == apartment);
            }
           

            ViewBag.GetListingType = getListingType();
            ViewBag.DrpDownState = db.L_State_tb.Where(x => !string.IsNullOrEmpty(x.StateName)).Distinct().ToList();
            return View(listing.Where(x => x.Status == 1 && x.UnitStatus == "Vacant").OrderByDescending(s => s.AddedDate).Take(16).ToList());
        }

       
        public ActionResult ViewProperty(string id)
        {
           
                var db_row = db.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();

                string[] property = { };
                var prop = "";
                var ppt = db_row.PropertyType.Contains('-');
                if (ppt == true)
                {

                    property = db_row.PropertyType.Split('-');
                    prop = property[0];
                }


                if (!string.IsNullOrEmpty(prop))
                {
                    prop = property[0].ToString();
                }
                if (db_row == null)
                {
                    return RedirectToAction("Index");
                }

                ViewBag.gtSinking = Convert.ToDecimal(db_row.Sinking).ToString("#,##0.00");
                ViewBag.gtMaintenance = Convert.ToDecimal(db_row.Maintenance).ToString("#,##0.00");
                ViewBag.gtPrice = Convert.ToDecimal(db_row.ListingPrice).ToString("#,##0");
                ViewBag.convertBuildUpToDecimal = Convert.ToDecimal(db_row.BuildUp).ToString("#,##0");
                ViewBag.convertLandAreaToDecimal = Convert.ToDecimal(db_row.LandArea).ToString("#,##0");

                //Verify propertyType
                if (db_row.PropertyType == "Industrial Land" || db_row.PropertyType == "Development Land"
                                || db_row.PropertyType == "Agriculture Land")//if this post is author's
                {
                    ViewBag.isLandProp = "";
                    ViewBag.ntLandProp = "display:none;";//hide not-land property data

                }
                else if (prop == "Bungalow" || db_row.PropertyType == "Semi Detached" || db_row.PropertyType == "Terrace House" || db_row.PropertyType == "Shop House")
                {

                    ViewBag.isLandProp = "";
                    ViewBag.ntLandProp = "";

                }
                else
                {
                    ViewBag.isLandProp = "display:none;";//hide land property data
                    ViewBag.downloadDoc = "display:none;";
                    ViewBag.ntLandProp = "";
                }

                var usrid = "";
                if (Request.Cookies["isLogin"] != null)
                {
                    usrid = Request.Cookies["isLogin"].Value;
                }

                return View(db_row);
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Properties()
        {
            return View();
        }
        public ActionResult Team()
        {
            return View();
        }
        public ActionResult projects()
        {
            return View();
        }
        public ActionResult News()
        {
            var listblog = db.L_BlogTbl.OrderByDescending(x => x.CreatedDate).ToList();
            return View(listblog);
        }

        public ActionResult News2(int? id)
        {
            
            if (id == null)
            {
                return View("News");

            }

            else
            {
                var blog = db.L_BlogTbl.Where(x => x.Id == id).FirstOrDefault();
                return View(blog);
            }
            
        }

        #region Dropdown
        public List<string> getListingType()
        {
            return db.L_ListingTbl.Where(x => !string.IsNullOrEmpty(x.ListingType)).Select(x => x.ListingType).Distinct().ToList();
        }

        public List<string> getState()
        {
            return db.L_State_tb.Where(x => !string.IsNullOrEmpty(x.StateName)).Select(x => x.StateName).Distinct().ToList();
        }
        #endregion

        public JsonResult loadArea(string stateName)
        {
            var stateID = db.L_State_tb.Where(x => x.StateName == stateName).Select(x => x.StateID).FirstOrDefault();

            return Json(db.L_Area_tb.Where(a => a.StateID == stateID).Select(a => new {
                aName = a.AreaName
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult  Career()
        {
            return View();
        }

        public ActionResult Fb_news()
        {
            return View();
        }

    }
}