﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZeonPropertiesListing.Models;
namespace ZeonPropertiesListing.Controllers
{
    public class AuthenticateController : Controller
    {
        public ActionResult Login(M_User loginPg)//on load page, 
        {
            //check cookies on load
            string username = string.Empty, password = string.Empty;
            if (Request.Cookies["isLogin"] != null)
            { username = Response.Cookies["username"].Value; }



            if (Request.Cookies["isLogin"] != null)//else, cookie has value
            {
                return RedirectToAction("displayData", "Listing");
            }//end else
            else
            {
                return View();
            }
        }

        // GET: Login
        [HttpPost]
        public ActionResult Login(M_User loginPg, string a)
        {
            var id_uname = Request.Form["usrnameid"];
            var pw = Request.Form["usrpw"];
            var rememberme = Request.Form["rememberme"];

            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                var userDetails = dbModel.M_User.Where(x => (x.UserID.ToLower() == id_uname.ToLower() //.ToLower() == makes all string lower cases
                                 || x.UserEmail.ToLower() == id_uname.ToLower())
                                 && x.Password == pw).FirstOrDefault();

                if (userDetails == null)
                {
                    loginPg.LoginErrMessage = "Invalid username/ID or password";
                    return View(loginPg);
                }
                else if (userDetails.Status == "Inactive")
                {
                    loginPg.LoginErrMessage = "Your account is not active. Please contact the admin";
                    return View(loginPg);
                }
                else
                {
                    var remembermeState = Request.Form["rememberme"];

                    if (remembermeState == "chck")
                    {


                        //save to cookie
                        //login id
                        HttpCookie loginstatus = new HttpCookie("isLogin");
                        loginstatus.Expires = DateTime.Now.AddYears(30);
                        loginstatus.Value = userDetails.UserID;
                        Response.SetCookie(loginstatus);//send to browser

                        //position
                        HttpCookie usrPos = new HttpCookie("usrPos");
                        usrPos.Expires = DateTime.Now.AddYears(30);
                        usrPos.Value = userDetails.Position;
                        Response.SetCookie(usrPos);//send to browser

                        //usr firstname lastname
                        HttpCookie loginUsr = new HttpCookie("loginUsr");
                        loginUsr.Expires = DateTime.Now.AddYears(30);
                        loginUsr.Value = userDetails.FirstName + " " + userDetails.LastName;
                        Response.SetCookie(loginUsr);//send to browser

                        //id + usrname
                        HttpCookie Id_Usrnme = new HttpCookie("Id_Usrnme");
                        Id_Usrnme.Expires = DateTime.Now.AddYears(30);
                        Id_Usrnme.Value = userDetails.UserID + " " + userDetails.FirstName + " " + userDetails.LastName;
                        Response.SetCookie(Id_Usrnme);//send to browser

                        //mobile num
                        HttpCookie usrContact = new HttpCookie("usrContact");
                        usrContact.Expires = DateTime.Now.AddYears(30);
                        usrContact.Value = userDetails.MobileCountry + "-" + userDetails.MobileNo;
                        Response.SetCookie(usrContact);//send to browser

                        //isAdmin or not
                        HttpCookie isAdmin = new HttpCookie("isAdmin");
                        isAdmin.Expires = DateTime.Now.AddYears(30);
                        isAdmin.Value = userDetails.Admin.ToString();
                        Response.SetCookie(isAdmin);//send to browser

                        /*Session["isLoginID"] = Request.Cookies["isLogin"].Value;
                        Session["usrPosition"] = Request.Cookies["usrPos"].Value;
                        Session["loginUsrName"] = Request.Cookies["loginUsr"].Value;
                        Session["IdnUsrnme"] = Request.Cookies["Id_Usrnme"].Value;
                        Session["usrContact"] = Request.Cookies["usrContact"].Value;
                        Session["isAdmin"] = Request.Cookies["isAdmin"].Value;*/
                    }
                    else if (remembermeState != "chck")
                    {
                        //destroy cookie

                        if (Request.Cookies["isLogin"] != null)
                        {
                            HttpCookie islogin = new HttpCookie("isLogin");
                            islogin.Expires = DateTime.Now.AddDays(-1d);
                            Response.SetCookie(islogin);

                        }

                        //assign to session
                        Session["isLoginID"] = userDetails.UserID;
                        Session["usrPosition"] = userDetails.Position;//get user position
                        Session["loginUsrName"] = userDetails.FirstName + " " + userDetails.LastName;
                        Session["IdnUsrnme"] = Session["isLoginID"].ToString() + "-" + Session["loginUsrName"].ToString();
                        Session["usrContact"] = userDetails.MobileCountry + "-" + userDetails.MobileNo;
                        Session["isAdmin"] = userDetails.Admin.ToString();
                    }


                    return RedirectToAction("displayData", "Listing");
                }
            }//end using
        }//end action result


        [SessionExpire]
        //logout 
        public ActionResult Logout()
        {
            Session.Abandon();

            string uname = "";
            if (Request.Cookies["isLogin"] != null)//if cookies exist
            {
                uname = Request.Cookies["isLogin"].Value;
                HttpCookie islogin = new HttpCookie("isLogin");
                islogin.Expires = DateTime.Now.AddDays(-1d);
                Response.SetCookie(islogin);
            }

            return RedirectToAction("Login");
        }


        //end logout
    }
}