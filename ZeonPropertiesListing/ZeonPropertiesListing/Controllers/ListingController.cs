﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects; //for EntityFunctions.TruncateTime in DisplayData()
using System.Data.Entity.Validation;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZeonPropertiesListing.Controllers;
using ZeonPropertiesListing.Models;//(1) [projectName][Model]
using ZeonPropertiesListing.ViewModel;
//Go to Nuget -> Browse -> "EPPlus" -> using OfficeOpenXml
using OfficeOpenXml;
using PagedList;
namespace Listing.Controllers
{
    //this is important. This enable multiple ajax to send to controller. 
    //this also affect session. Any session declare in this controller may not function as expected
    [SessionExpire]
    public class ListingController : Controller
    {
        Overriding_DBEntities db = new Overriding_DBEntities();
        // GET: Listing
        //This will route to displayData View when user enter Listing/ only
        //[Route("Listing/")]

        public ActionResult displayData(string searchString, int? page, string PropertyTypeDashB,
                    string agentDashB, string stateDashB, string LTypeDashB, string StatusDashB, string areaDashB)
        {//(2) display every data from database

            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {




                //this is to make sure when another page is clicked, system will remember the enetered/selected values in filter part
                //return searched AdsTitle 
                ViewBag.CurrentFilter = searchString;
                //return property type to textbox etc
                ViewBag.currentpropertyType = PropertyTypeDashB;
                ViewBag.currentAgent = agentDashB;
                ViewBag.currentState = stateDashB;
                ViewBag.currentLType = LTypeDashB;
                ViewBag.currentStatus = StatusDashB;
                ViewBag.currentArea = areaDashB;

                //Note:
                /*
                 currentFilter -> capture from 
                 */
                //Property Type
                List<string> PropertyTypeList = new List<string>();
                PropertyTypeList.Add("Bungalow"); PropertyTypeList.Add("Terrace House");
                PropertyTypeList.Add("Semi Detached"); PropertyTypeList.Add("Apartment");
                PropertyTypeList.Add("Cluster Home"); PropertyTypeList.Add("Factory");
                PropertyTypeList.Add("Industrial Land"); PropertyTypeList.Add("Development Land");
                PropertyTypeList.Add("Agriculture Land"); PropertyTypeList.Add("Shop House");
                PropertyTypeList.Add("Complex Shoplot"); PropertyTypeList.Add("Office Lot");
                PropertyTypeList.Add("Town House"); PropertyTypeList.Add("Others");
                PropertyTypeList.Sort();

                ViewBag.PropertyType = new SelectList(PropertyTypeList, PropertyTypeDashB);

                //return filtered agent
                ViewBag.Agent = agentDashB;

                //return filtered  AddedDateDashB = DateTime.Today;
                var AddedDateDashB = Request.Form["AddedDateDashB"];
                ViewBag.compleDate = AddedDateDashB;

                //return filtered listing type
                List<string> Ltype = new List<string>();
                Ltype.Add("Rent"); Ltype.Add("Sale"); Ltype.Add("Rent & Sale");
                Ltype.Sort();
                ViewBag.Ltype = new SelectList(Ltype, LTypeDashB);

                //return filtered state
                ViewBag.DrpDownState = dbModel.L_State_tb.Where(x => !string.IsNullOrEmpty(x.StateName)).Distinct().ToList();
                //return filtered status
                List<string> StatusType = new List<string>();
                StatusType.Add("Approved"); StatusType.Add("Taken Down"); StatusType.Add("Pending");
                StatusType.Sort();

                //Note: usually a 0 is use to initialize a int var. Since 0 is used as "take down" 
                //hence it is initialzie with 10 instead    
                var statusInt = 10;
                ViewBag.StatusType = new SelectList(StatusType, StatusDashB);
                if (StatusDashB == "Approved") { statusInt = 1; }
                else if (StatusDashB == "Taken Down") { statusInt = 0; }
                else if (StatusDashB == "Pending") { statusInt = -1; }




                //Start search/filter
                var agentDashboard = from s in dbModel.v_listing.Where(x => x.Status != -2)//exclude draft
                                     select s;


                var AllAgents = new List<v_listing>();


                if (!String.IsNullOrEmpty(searchString))
                {
                    string[] agents = searchString.Split(' ');
                    foreach (var name in agents)
                    {
                        agentDashboard = agentDashboard.Where(s => s.FirstName.ToLower().Contains(name) | s.LastName.ToLower().Contains(name) | s.AdsTitle.Contains(searchString) | s.UserID.Contains(searchString) | s.UnitNo.Contains(searchString));
                        AllAgents.AddRange(agentDashboard);
                    }
                    //agentDashboard = agentDashboard.Where(s => s.AdsTitle.Contains(searchString) || s.FirstName.Contains(searchString) || s.LastName.Contains(searchString) || s.UserID.Contains(searchString));
                }
                if (!String.IsNullOrEmpty(PropertyTypeDashB))
                {
                    agentDashboard = agentDashboard.Where(s => s.PropertyType.Contains(PropertyTypeDashB));
                }
                if (!String.IsNullOrEmpty(agentDashB))
                {
                    agentDashboard = agentDashboard.Where(s => s.Agent == agentDashB);
                }
                if (!String.IsNullOrEmpty(LTypeDashB))
                {
                    agentDashboard = agentDashboard.Where(s => s.ListingType == LTypeDashB);
                }
                if (!String.IsNullOrEmpty(stateDashB))
                {
                    agentDashboard = agentDashboard.Where(s => s.State == stateDashB);
                }
                if (statusInt != 10)
                {
                    agentDashboard = agentDashboard.Where(s => s.Status == statusInt);
                }
                if (!String.IsNullOrEmpty(AddedDateDashB))
                {
                    //This retrive AddedDate (Date+Time) -> TruncateTime = Cut out the time part, left the date -> turn to string -> compare by 'Contain'
                    agentDashboard = agentDashboard.Where(s => EntityFunctions.TruncateTime(s.AddedDate).ToString().Contains(AddedDateDashB));

                }


                if (!String.IsNullOrEmpty(areaDashB))
                {
                    agentDashboard = agentDashboard.Where(s => s.Area.Contains(areaDashB));
                }
                //save to session. These session will be use to filtered again before export to Excel
                Session["search"] = searchString;
                Session["propertyType"] = PropertyTypeDashB;
                Session["agent"] = agentDashB;
                Session["AddedDate"] = AddedDateDashB;
                Session["state"] = stateDashB;
                Session["ListingType"] = LTypeDashB;
                Session["status"] = StatusDashB;
                Session["area"] = areaDashB;

                int pageSize = 30;
                int pageNumber = (page ?? 1);

                TempData["currentPg"] = pageNumber;//current page number asign to tempdata




                return View(agentDashboard.Where(x => x.UnitStatus == "Vacant").OrderByDescending(s => s.AddedDate).ToPagedList(pageNumber, pageSize));
            }
        }


        public JsonResult loadArea(string stateName)
        {
            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                var stateID = dbModel.L_State_tb.Where(x => x.StateName == stateName).Select(x => x.StateID).FirstOrDefault();

                return Json(dbModel.L_Area_tb.Where(a => a.StateID == stateID).Select(a => new {
                    aName = a.AreaName
                }).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportToExcel()
        {
            //call session from displayData()
            var search = Session["search"];
            var propertyType = Session["propertyType"];
            var agent = Session["agent"];
            var AddedDate = Session["AddedDate"];
            var state = Session["state"];
            var area = Session["area"];
            var ListingType = Session["ListingType"];
            var status = Session["status"];


            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                //start include filtered data
                var agentDashboard = from s in dbModel.v_listing.Where(x => x.Status != -2)//exclude draft
                                     select s;


                //filtered ads title
                if (!String.IsNullOrEmpty(search as string))
                {
                    agentDashboard = agentDashboard.Where(s => s.AdsTitle.Contains(search.ToString()) | s.UnitNo.Contains(search.ToString()) | s.UserID.Contains(search.ToString())
                    | s.FirstName.Contains(search.ToString()) | s.LastName.Contains(search.ToString()));
                }
                //filtered property type
                if (!String.IsNullOrEmpty(propertyType as string))
                {
                    agentDashboard = agentDashboard.Where(s => s.PropertyType.Contains(propertyType.ToString()));
                }


                //filtered listing type
                if (!String.IsNullOrEmpty(ListingType as string))
                {
                    agentDashboard = agentDashboard.Where(s => s.ListingType == ListingType.ToString());
                }
                //filtered state
                if (!String.IsNullOrEmpty(state as string))
                {
                    agentDashboard = agentDashboard.Where(s => s.State == state.ToString());
                }
                //filtered area
                if (!String.IsNullOrEmpty(area as string))
                {
                    agentDashboard = agentDashboard.Where(s => s.Area == area.ToString());
                }

                //check status string value -> change to int token
                var statusInt = 10;
                if (status == "Approve") { statusInt = 1; }
                else if (status == "Take Down") { statusInt = 0; }
                else if (status == "Pending") { statusInt = -1; }

                //filtered status
                if (statusInt != 10)
                {
                    agentDashboard = agentDashboard.Where(s => s.Status == statusInt);
                }

                //filtered addeddate
                if (!String.IsNullOrEmpty(AddedDate as string))
                {
                    //This retrive AddedDate (Date+Time) -> TruncateTime = Cut out the time part, left the date -> turn to string -> compare by 'Contain'
                    agentDashboard = agentDashboard.Where(s => EntityFunctions.TruncateTime(s.AddedDate).ToString().Contains(AddedDate.ToString()));

                }

                //after filtered -> save to 
                List<v_listing> listinglist = agentDashboard.OrderByDescending(x => x.AddedDate).ToList();

                //normal: export all
                // List<L_ListingTbl> listinglist = dbModel.L_ListingTbl.OrderByDescending(x => x.AddedDate).ToList();

                //Start Exporting to Excel
                var totalColumn = 18;

                ExcelPackage pck = new ExcelPackage();
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

                //Note: .Cells[FromRow, FromColumn, ToRow, ToColumn] //style excel row and column
                ws.Cells[2, 1, 2, totalColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells[2, 1, 2, totalColumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("yellow")));
                ws.Cells[2, 1, 2, totalColumn].Style.Font.Bold = true;

                ws.Cells["A2"].Value = "No.";// 1
                ws.Cells["B2"].Value = "Listing ID";// 2
                ws.Cells["C2"].Value = "AdsTitle"; // 3
                ws.Cells["D2"].Value = "Property Type"; //4
                ws.Cells["E2"].Value = "Unit No";// 5
                ws.Cells["F2"].Value = "Owner Name";// 6
                ws.Cells["G2"].Value = "Owner Contact";// 7
                ws.Cells["H2"].Value = "Owner Contact 2";// 8
                ws.Cells["I2"].Value = "Owner Email";// 9
                ws.Cells["J2"].Value = "Agent"; //10
                ws.Cells["K2"].Value = "Listing Type";// 11
                ws.Cells["L2"].Value = "Price";//12
                ws.Cells["M2"].Value = "State"; //13
                ws.Cells["N2"].Value = "Area";// 14
                ws.Cells["O2"].Value = "Added Date"; //15
                ws.Cells["P2"].Value = "Unit Status"; //16
                ws.Cells["Q2"].Value = "Approve By"; //17
                ws.Cells["R2"].Value = "Approve Status"; //18

                int rowstart = 3; var strStatus = "";
                var NumCount = 0;
                foreach (var item in listinglist)
                {
                    //bold the 7th column
                    ws.Cells[rowstart, 18].Style.Font.Bold = true;

                    if (item.Status == 2)//if property is sold -> change background color
                    {
                        strStatus = "SOLD";//black
                        ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells[rowstart, 1, rowstart, totalColumn].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(string.Format("lightsalmon")));
                    }


                    if (item.Status == 1)
                    {
                        ws.Cells[rowstart, 18].Style.Font.Color.SetColor(Color.Green);//set color
                        strStatus = "Approve";//green
                    }
                    else if (item.Status == 0)
                    {
                        ws.Cells[rowstart, 18].Style.Font.Color.SetColor(Color.IndianRed);//set color
                        strStatus = "Take Down";//red
                    }
                    else if (item.Status == -1)
                    {
                        ws.Cells[rowstart, 18].Style.Font.Color.SetColor(Color.Blue);//set color
                        strStatus = "Pending";//blue
                    }

                    //bold for unitstatus 
                    ws.Cells[rowstart, 16].Style.Font.Bold = true;
                    if (item.UnitStatus == "Vacant")
                    {
                        ws.Cells[rowstart, 16].Style.Font.Color.SetColor(Color.Blue); //set vacant color = blue
                    }

                    else if (item.UnitStatus == "Occupied")
                    {
                        ws.Cells[rowstart, 16].Style.Font.Color.SetColor(Color.Red); //set occupied color = red
                    }
                    //end unitstatus
                    NumCount++;
                    ws.Cells[string.Format("A{0}", rowstart)].Value = NumCount;
                    ws.Cells[string.Format("B{0}", rowstart)].Value = item.Id;
                    ws.Cells[string.Format("C{0}", rowstart)].Value = item.AdsTitle;
                    ws.Cells[string.Format("D{0}", rowstart)].Value = item.PropertyType;
                    ws.Cells[string.Format("E{0}", rowstart)].Value = item.UnitNo;
                    ws.Cells[string.Format("F{0}", rowstart)].Value = item.OwnerName;
                    ws.Cells[string.Format("G{0}", rowstart)].Value = item.OwnerContact;
                    ws.Cells[string.Format("H{0}", rowstart)].Value = item.OwnerContact2;
                    ws.Cells[string.Format("I{0}", rowstart)].Value = item.OwnerEmail;
                    //get agent name from User DB
                    var newName = "Unable detect This Agent from Listing Database";
                    var row = dbModel.v_listing.Where(x => x.UserID == item.Agent).FirstOrDefault();
                    if (row == null)
                    {
                        newName = item.Agent + " - This User is deleted";
                    }
                    else
                    {
                        var name = row.FirstName + " " + row.LastName;
                        var id = row.UserID;
                        newName = id + " - " + name;
                    }


                    ws.Cells[string.Format("J{0}", rowstart)].Value = newName;//change to name
                    ws.Cells[string.Format("K{0}", rowstart)].Value = item.ListingType;
                    ws.Cells[string.Format("L{0}", rowstart)].Value = item.ListingPrice;
                    ws.Cells[string.Format("M{0}", rowstart)].Value = item.State;
                    ws.Cells[string.Format("N{0}", rowstart)].Value = item.Area;
                    ws.Cells[string.Format("O{0}", rowstart)].Value = string.Format("{0:dd MMMM yyyy} at {0:H: mm tt}", item.AddedDate);


                    //get Admin name from User DB
                    var newAdminName = "Unable detect This Agent from Listing Database";

                    if (item.ApproveBy == "Pending")
                    {
                        newAdminName = "Pending";
                    }
                    else
                    {

                        var row2 = dbModel.v_listing.Where(x => x.UserID == item.ApproveBy).FirstOrDefault();
                        if (row2 == null)
                        {
                            newAdminName = item.ApproveBy + " - This Admin is deleted";
                        }
                        else
                        {
                            var name = row2.FirstName + " " + row2.LastName;
                            var id = row2.UserID;
                            newAdminName = id + " - " + name;
                        }
                    }


                    ws.Cells[string.Format("P{0}", rowstart)].Value = item.UnitStatus;
                    ws.Cells[string.Format("Q{0}", rowstart)].Value = newAdminName;
                    ws.Cells[string.Format("R{0}", rowstart)].Value = strStatus;

                    rowstart++;//increment rowstart num
                }//end foreach

                ws.Cells["A:AZ"].AutoFitColumns();
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}", "Listing.xlsx"));
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }//end using


            return View();
        }


        // GET: list of draft
        public ActionResult myDraft()
        {

            Overriding_DBEntities dbModel = new Overriding_DBEntities();
            var userid = "";
            if (Session["isLoginID"] != null)
            {
                userid = Session["isLoginID"].ToString();
            }
            else if (Request.Cookies["isLogin"] != null)
            {
                userid = Request.Cookies["isLogin"].Value;
            }
            else
            {
                return RedirectToAction("displayData");
            }

            var userDraft = dbModel.L_ListingTbl.Where(x => x.Status == -2 && x.Agent == userid.ToString()).ToList();
            return View(userDraft.ToList());
        }



        // GET: Listing/Create
        //(5) Add View using this Create()
        public ActionResult Create()
        {
            //call create dropdown
            populateCreateDropdown();



            return View();
        }



        public JsonResult populate(string selected)//repopulate data, from CreateJS
        {
            List<SelectListItem> PropertyType2 = new List<SelectListItem>();
            if (selected == "Bungalow" || selected == "Semi Detached" || selected == "Terrace House")
            {

                PropertyType2.Add(new SelectListItem { Text = "1 Storey", Value = "1 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "2 Storey", Value = "2 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "2.5 Storey", Value = "2.5 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "3 Storey", Value = "3 Storey" });

            }
            else if (selected == "Shop House")
            {
                PropertyType2.Add(new SelectListItem { Text = "1 Storey", Value = "1 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "2 Storey", Value = "2 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "3 Storey", Value = "3 Storey" });
                PropertyType2.Add(new SelectListItem { Text = "4 Storey", Value = "4 Storey" });
            }
            else
            {
                PropertyType2.Add(new SelectListItem { Text = "no selection", Value = "no selection" });
            }
            return Json(new SelectList(PropertyType2, "Value", "Text"));

        }
        //-------------saveDoc()
        public ActionResult saveDoc(List<HttpPostedFileBase> attachment)//from ajax
        {
            var strr = ""; string tempName = ""; string docNamestr = "";
            Session["docpath"] = "";
            Session["docName"] = "";
            /*save from JS, Add photo to folder first*/
            if (attachment != null)
            {
                foreach (var file in attachment)
                {

                    if (file != null && file.ContentLength > 0)
                    {
                        var attachFile = Path.GetFileNameWithoutExtension(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        attachFile = attachFile + extension;
                        tempName = Guid.NewGuid() + Path.GetExtension(file.FileName);

                        if (attachment.IndexOf(file) == attachment.Count - 1)//count if the file is last file
                        {
                            strr += tempName;// this is the last item, save it without comma
                            docNamestr += attachFile;
                        }
                        else
                        {
                            strr += tempName + ",";//save this into DB column
                            docNamestr += attachFile + ",";
                        }

                        attachFile = Path.Combine(Server.MapPath(Url.Content("~/Attachment/" + tempName)));
                        //attachFile = Path.Combine(Server.MapPath("~/Attachment/"), tempName);
                        file.SaveAs(attachFile);

                    }//end if
                }//end foreach
                 /*  Save img to folder (*2)*/
                if (strr == "") { strr = "empty attachement"; }
                else if (strr == null) { strr = "null attachement"; }
                // save to session
                Session["docpath"] = strr.ToString();
                Session["docName"] = docNamestr.ToString();

            }
            else
            {//In edit pg, user may or may not add new image. 
             //hence, If no new img is added, assign emtpy (clr session)
                Session["docpath"] = ""; Session["docName"] = "";
            }
            return Json(strr);
        }
        //---save image
        //Models.ImgTbl imgimg require for parameter -> insert image. imgimg is frm vaiew model
        public ActionResult saveImage(List<HttpPostedFileBase> image)//capture json
        {

            string strr = ""; string tempName = "";
            Overriding_DBEntities dbModel = new Overriding_DBEntities();
            Session["photopath"] = "";
            /*save from JS, Add photo to folder first*/
            if (image != null)
            {
                foreach (var file in image)
                {
                    if (file != null && file.ContentLength > 0)//file content length refer if there is some byte in the file
                    {
                        /*  Save img to folder (*1)*/

                        var newimg = Path.GetFileNameWithoutExtension(file.FileName);
                        string extension = Path.GetExtension(file.FileName);
                        newimg = newimg + extension;
                        tempName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                        newimg = Path.Combine(Server.MapPath(Url.Content("~/Image/" + tempName)));
                        file.SaveAs(newimg);
                        if (!string.IsNullOrEmpty(tempName))//if no name is detected
                        {
                            if (image.IndexOf(file) == image.Count - 1)//count if the file is last file
                            {
                                strr += tempName;// this is the last item, save it without comma
                            }
                            else
                            {
                                strr += tempName + ",";//save this into DB column
                            }
                        }
                        else
                        {
                            strr = "img name is null";
                        }
                    }//end if
                }//end foreach
                 /*  Save img to folder (*2)*/
                if (strr == "") { strr = "empty"; }
                else if (strr == null) { strr = "null"; }
                //save to session
                Session["photopath"] = strr.ToString();


            }
            else
            {//In edit pg, user may or may not add new image. 
             //hence, If no new img is added, assign emtpy (clr session)
                Session["photopath"] = "";

            }

            return Json(strr);
            //return Json("OK");
        }


        //-------------------------------------------------------------upload multiple image
        [ValidateInput(false)]//require for ckeditor
        [HttpPost]
        public ActionResult Create(L_ListingTbl list, string[] cond_chck, string[] faci_chck, string urgent_chck, L_AttachmentTbl docTbl,
            List<HttpPostedFileBase> image, List<HttpPostedFileBase> attachment, string clickedBtn)//cond_chck = checkbox
        {

            string submitbtn = clickedBtn;
            // try { } catch { }
            // if (ModelState.IsValid) { }
            if (cond_chck != null)
            {
                //get Condition checkbox value
                List<string> chckbx = new List<string>();
                chckbx = cond_chck.ToList();
                var num = cond_chck.Count();
                string temp_var = "";
                foreach (string item in chckbx)
                {
                    if (chckbx.IndexOf(item) == num - 1)//count if the file is last file
                    {
                        temp_var += item;// this is the last item, save it without comma
                    }
                    else
                    {
                        temp_var += item + ",";//save this into DB column
                    }
                }
                list.Condition = temp_var;
            }//end if condition checkbox is null

            if(urgent_chck != null)
            {
                list.UrgentListing = "Urgent";
            }
            if (faci_chck != null)
            {
                //get Condition checkbox value
                List<string> fchck = new List<string>();
                fchck = faci_chck.ToList();
                var num = faci_chck.Count();
                string temp_var = "";
                foreach (string item in fchck)
                {
                    if (fchck.IndexOf(item) == num - 1)//count if the file is last file
                    {
                        temp_var += item;// this is the last item, save it without comma
                    }
                    else
                    {
                        temp_var += item + ",";//save this into DB column
                    }
                }
                list.Facilities = temp_var;
            }//end if condition checkbox is null



            //generate ID
            var docpath = ""; var docName = "";
            string[] guidNameArr = { }; string[] docNameArr = { };

            string newGuid = System.Guid.NewGuid().ToString();
            list.Id = newGuid;

            var dateTime = DateTime.Now;//get today's date
            list.AddedDate = dateTime;//add date into list's AddedDate column
            if (submitbtn == "Draft")
            {
                list.Status = -2;//means the listing is draft
            }
            else if (submitbtn == "Create")
            {
                list.Status = -1;
            }
            //Get Listing Type
            var listingType = Request.Form["ListingType"].ToString();
            //Insert Price depending on selected Listing Type
            if (listingType == "Rent & Sale")
            {
                //get price as string -> double -> database
                string rentPriceComma = Request.Form["RentPrice"].ToString();
                if (!string.IsNullOrEmpty(rentPriceComma))
                {
                    rentPriceComma = rentPriceComma.Replace(",", string.Empty);//replace with empty
                    decimal price = decimal.Parse(rentPriceComma);
                    list.ListingPrice = price;
                }

                //get price as string -> double -> database
                string SalePriceComma = Request.Form["SalePrice"].ToString();
                if (!string.IsNullOrEmpty(rentPriceComma))
                {
                    SalePriceComma = SalePriceComma.Replace(",", string.Empty);//replace with empty
                    decimal price = decimal.Parse(SalePriceComma);
                    list.ListingSale = price;
                }
            }
            else
            {
                //get price as string -> double -> database (NORMAL)
                string priceComma = Request.Form["ListingPrice"].ToString();
                if (!string.IsNullOrEmpty(priceComma))
                {
                    priceComma = priceComma.Replace(",", string.Empty);//replace with empty
                    decimal price = decimal.Parse(priceComma);
                    list.ListingPrice = price;
                }
            }


            list.UnitStatus = "Vacant"; // this is for overriding


            //get dropdown selected value
            string PropertyType = Request.Form["PropertyType"].ToString();//to string
            //insert approveby
            list.ApproveBy = "Pending";

            string PropertyType2 = Request.Form["PropertyType2"].ToString();

            if (PropertyType == "Bungalow" || PropertyType == "Semi Detached" ||
             PropertyType == "Terrace House" || PropertyType == "Shop House")//if storey is not empty (is selected..
            {
                list.PropertyType = PropertyType + "-" + PropertyType2;
            }
            //get completiondate date picker
            if (Request.Form["completionDate"] == "" || Request.Form["completionDate"] == null) { }
            //state, area dropdown
            list.State = Request.Form["comboBoxState"];
            list.Area = Request.Form["comboBoxArea"];

            //get Sinking FUnd
            string sinkingFund = Request.Form["Sinking"].ToString();
            if (sinkingFund != "")
            {
                sinkingFund = sinkingFund.Replace(",", string.Empty);//replace with empty
                decimal sinking = decimal.Parse(sinkingFund);
                list.Sinking = sinking;
            }
            //get Matenance fee 
            string Maintenance = Request.Form["Maintenance"].ToString();
            if (Maintenance != "")
            {
                Maintenance = Maintenance.Replace(",", string.Empty);//replace with empty
                decimal maintenance = decimal.Parse(Maintenance);
                list.Maintenance = maintenance;
            }

            //Assign Id only
            // var usrid = Request.Cookies["isLogin"].Value;
            var usrid = Session["isLoginID"].ToString();
            list.Agent = usrid.ToString();//from login page
                                          //assign photopath to DB


            list.Photo = Session["photopath"].ToString();//assign to selected row

            if (PropertyType == "Industrial Land" || PropertyType == "Development Land" || PropertyType == "Agriculture Land")
            {
                list.Measurement_L = Request.Form["LMeasureUnit"];//capture Land Area unit
            }
            else
            {
                list.Measurement = Request.Form["BMeasureUnit"];//capture Build Area unit
            }

            string cookienull = null;

            //if both session is not empty/null
            if (!string.IsNullOrEmpty(Session["docpath"] as string) && !string.IsNullOrEmpty(Session["docName"] as string))
            {
                cookienull = Session["docpath"].ToString();
            }


            if (cookienull == null)//if docpath completely null
            {
                using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
                {
                    dbModel.L_ListingTbl.Add(list);//add data to listing table
                    dbModel.SaveChanges();
                }
            }
            else //start assign docpath to DB
            {
                using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
                {
                    dbModel.L_ListingTbl.Add(list);//add data to listing table



                    //assign document path and name
                    docpath = Session["docpath"].ToString();//doc guid name (with comma)
                    docName = Session["docName"].ToString();//doc actual name (with comma)
                    guidNameArr = docpath.Split(',');
                    docNameArr = docName.Split(',');
                    if (PropertyType == "Industrial Land" || PropertyType == "Development Land" || PropertyType == "Agriculture Land")
                    {

                        //start add to Attachment Table
                        var i = 0;
                        foreach (var guidName in guidNameArr)
                        {

                            docTbl.guidName = guidName;//document guidName
                            docTbl.docName = docNameArr[i];//docuemnt actual name, by accessing the array

                            docTbl.ListingId = newGuid;//assign the listing guid Id (generated in Create())

                            dbModel.L_AttachmentTbl.Add(docTbl);//add attachment
                            dbModel.SaveChanges();
                            i++;

                        }//end guidName foreach
                    }//end check  land realated
                    else
                    {
                        dbModel.SaveChanges();
                    }
                }//end using dbmodel

            }//end else


            if (submitbtn == "Draft")
            {
                string retrieveid = list.Id;

                return RedirectToAction("Draft", new { id = retrieveid });
            }
            else
            {
                return RedirectToAction("displayData");
            }

        }


        public ActionResult Draft(string id)//id get from  URL 
        {
            Overriding_DBEntities dbModel = new Overriding_DBEntities();

            if (string.IsNullOrEmpty(id))
            {

                return RedirectToAction("myDraft");
            }

            else
            {

                vviewModel twoModel = drafteditaction(id);//call method

                var userid = "";
                if (Session["isLoginID"] != null)//if id != null
                {
                    userid = Session["isLoginID"].ToString();
                    if (twoModel.listingTbl.Agent != userid)//if user is not the author
                    {
                        return RedirectToAction("myDraft");
                    }
                }
                else if (Request.Cookies["isLogin"] != null)
                {
                    userid = Request.Cookies["isLogin"].Value;
                    if (twoModel.listingTbl.Agent != userid)//if user is not the author
                    {
                        return RedirectToAction("myDraft");
                    }
                }

                return View(twoModel);
            }
        }

        [HttpPost]
        [ValidateInput(false)]//require for ckeditor
        public ActionResult Draft(string id, string[] cond_chck, L_AttachmentTbl docTbl, string ListingPrice,
                            string descripEditor, string clickedBtn)//id get from  URL 
        {
            var docpath = ""; var docName = "";
            string[] guidNameArr = { }; string[] docNameArr = { };
            //try
            //{
            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                var selrow = dbModel.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();//id ==  from URL


                selrow.AdsTitle = Request.Form["AdsTitle"];


                //------Update UnitNo

                selrow.UnitNo = Request.Form["UnitNo"];




                //-------Update completiondate date picker
                var a = Request.Form["completionDate"];
                if (!string.IsNullOrEmpty(a))
                {
                    var dt = DateTime.Parse(a);
                    selrow.CompletionDate = dt;
                }
                else
                {
                    selrow.CompletionDate = null;
                }




                //-----Update state, area dropdown

                selrow.State = Request.Form["comboBoxState"];

                selrow.Area = Request.Form["comboBoxArea"];


                //-----Update checkbox
                List<string> chckbx = new List<string>();
                if (cond_chck != null)
                {
                    chckbx = cond_chck.ToList();
                    var num = cond_chck.Count();
                    string temp_var = "";
                    foreach (string item in chckbx)
                    {
                        if (chckbx.IndexOf(item) == num - 1)//count if the file is last file
                        {
                            temp_var += item;// this is the last item, save it without comma
                        }
                        else
                        {
                            temp_var += item + ",";//save this into DB column
                        }
                    }

                    selrow.Condition = temp_var;
                }
                else { selrow.Condition = null; }


                //-----Update "Photo" column if NEW image is added

                var existingPhotoStr = selrow.Photo;//get existing photo string (if have)
                var getphoto = "";

                getphoto = Session["photopath"].ToString();

                var newImgPath = getphoto.ToString();//assign to a var

                if (string.IsNullOrEmpty(existingPhotoStr))//if the Photo column IS null or empty
                {
                    selrow.Photo = newImgPath;//assign to selected row
                }
                // else if DB phot sting exist & newImg string exist
                else if (!string.IsNullOrEmpty(existingPhotoStr) && !string.IsNullOrEmpty(newImgPath))
                {
                    selrow.Photo = existingPhotoStr + "," + newImgPath;
                }
                //------Update AddedDate
                selrow.AddedDate = DateTime.Now;
                //------Update owner Name
                selrow.OwnerName = Request.Form["ownerNme"];
                //-----Update owner Contact
                selrow.OwnerContact = Request.Form["ownerContact"];
                //-----Update owner Contact
                selrow.OwnerContact2 = Request.Form["ownerContact2"];
                //-----Update owner email
                selrow.OwnerEmail = Request.Form["ownerMail"];
                //-----Update owner address
                selrow.Address = Request.Form["address"];
                //-----Update lsiting type
                var ListingTypeVar = Request.Form["ListingType"];
                selrow.ListingType = ListingTypeVar;

                //-----Update listing price
                if (ListingTypeVar == "Rent & Sale")
                {
                    string rentComma = Request.Form["RentPrice"];
                    if (!string.IsNullOrEmpty(rentComma))
                    {
                        rentComma = rentComma.Replace(",", string.Empty);//replace with empty
                        decimal rentPriceComma = decimal.Parse(rentComma);
                        selrow.ListingPrice = rentPriceComma;
                    }
                    else { selrow.ListingPrice = null; }

                    string saleComma = Request.Form["SalePrice"];
                    if (!string.IsNullOrEmpty(rentComma))
                    {
                        saleComma = saleComma.Replace(",", string.Empty);//replace with empty
                        decimal salePriceComma = decimal.Parse(saleComma);
                        selrow.ListingSale = salePriceComma;
                    }
                    else { selrow.ListingSale = null; }

                }
                else
                {
                    string priceComma = ListingPrice;
                    if (!string.IsNullOrEmpty(priceComma))
                    {
                        priceComma = priceComma.Replace(",", string.Empty);//replace with empty
                        decimal price = decimal.Parse(priceComma);
                        selrow.ListingPrice = price;
                    }
                    else { selrow.ListingPrice = null; }
                }



                //-----Update other
                selrow.Other = Request.Form["Other"];
                //-----Update tenure
                selrow.Tenure = Request.Form["tenure"];
                //-----Update title
                selrow.Title = Request.Form["title"];
                //-----Update Remarks
                selrow.Remarks = Request.Form["Remarks"];

                //-----Update description (ckeditor)
                string descript = descripEditor.ToString();
                if (!string.IsNullOrEmpty(descript))
                {
                    selrow.Description = descript;
                }

                var PropertyType = Request.Form["PropertyTypetype"].ToString();//get selectd property
                                                                               //-----Update propertyType & propertyType2

                string PropertyType2 = Request.Form["PropertyType2"].ToString();
                //check selected property
                if (PropertyType == "Bungalow" || PropertyType == "Semi Detached" ||
                 PropertyType == "Terrace House" || PropertyType == "Shop House")
                {
                    selrow.PropertyType = PropertyType + "-" + PropertyType2;

                    //------Update Measurement Unit
                    selrow.Measurement_L = Request.Form["LMeasureUnit"];//capture Land Area unit
                                                                        //-----Update Land Area
                    if (!string.IsNullOrEmpty(Request.Form["LandArea"]))
                    {
                        selrow.LandArea = decimal.Parse(Request.Form["LandArea"]);//success decimal.Parse(Request.Form["LandArea"]);//success
                    }
                    else { selrow.LandArea = 0; }
                }
                else
                {
                    selrow.PropertyType = PropertyType;
                }
                //-----Add more rows in ATTACHMENT TBL when new doc is added


                //----Update status
                if (clickedBtn == "Create")
                {
                    selrow.Status = -1;//pending
                    selrow.ApproveBy = "Pending";
                }

                //check selected property
                if (PropertyType == "Industrial Land" || PropertyType == "Development Land" || PropertyType == "Agriculture Land")
                {
                    //------Update Measurement Unit
                    selrow.Measurement_L = Request.Form["LMeasureUnit"];//capture Land Area unit
                                                                        //-----Update Land Area
                    if (!string.IsNullOrEmpty(Request.Form["LandArea"]))
                    {
                        selrow.LandArea = decimal.Parse(Request.Form["LandArea"]);//success decimal.Parse(Request.Form["LandArea"]);//success
                    }
                    else { selrow.LandArea = 0; }

                    string docpathway = ""; string docnameway = "";
                    if (!string.IsNullOrEmpty(Session["docpath"] as string))
                    {
                        var abc = Session["docpath"].ToString();
                    }

                    //check if session are empty
                    if (!string.IsNullOrEmpty(Session["docpath"] as string) && !string.IsNullOrEmpty(Session["docName"] as string))
                    {
                        docpathway = Session["docpath"].ToString();//doc guid name (with comma)
                        docnameway = Session["docName"].ToString();//doc actual name (with comma)
                    }

                    //if docpath is not empty == document is updated
                    if (!string.IsNullOrEmpty(docpathway))//if session is not null (if new doc is added)
                    {
                        //assign document path and name
                        docpath = docpathway.ToString();//doc guid name (with comma)
                        docName = docnameway.ToString();//doc actual name (with comma)
                        guidNameArr = docpath.Split(',');
                        docNameArr = docName.Split(',');

                        var i = 0;
                        foreach (var guidName in guidNameArr)
                        {
                            docTbl.guidName = guidName;//document guidName
                            docTbl.docName = docNameArr[i];//docuemnt actual name, by accessing the array

                            docTbl.ListingId = id;//assign the listing guid Id (generated in Create())

                            dbModel.L_AttachmentTbl.Add(docTbl);//add attachment
                            dbModel.SaveChanges();
                            i++;
                        }//end guidName foreach
                    }//end if
                    else//else docpath is empty but Land-related property is selected
                    {
                        dbModel.SaveChanges();
                    }
                }//end check  land related
                else //if NOT-LAND property is selected
                {
                    //------Update Measurement Unit
                    selrow.Measurement = Request.Form["BMeasureUnit"];//capture Build Area unit

                    //-----Update bathroom num
                    if (string.IsNullOrEmpty(Request.Form["bathroom"]))
                    { selrow.Bathroom = null; }
                    else { selrow.Bathroom = int.Parse(Request.Form["bathroom"]); }

                    //-----Update room num
                    if (string.IsNullOrEmpty(Request.Form["roomNum"]))
                    { selrow.Room = null; }
                    else { selrow.Room = int.Parse(Request.Form["roomNum"]); }


                    //-----Update carpark num
                    if (string.IsNullOrEmpty(Request.Form["carpark"]))
                    { selrow.Carpark = null; }
                    else { selrow.Carpark = int.Parse(Request.Form["carpark"]); }

                    ////-----Update mantenance
                    string Maintenance = Request.Form["Maintenance"].ToString();

                    if (Maintenance != "")
                    {
                        Maintenance = Maintenance.Replace(",", string.Empty);//replace with empty
                        decimal maintenance = decimal.Parse(Maintenance);
                        selrow.Maintenance = maintenance;
                    }

                    //-----Update sinking fund
                    string sinkingFund = Request.Form["Sinking"].ToString();
                    if (sinkingFund != "")
                    {
                        sinkingFund = sinkingFund.Replace(",", string.Empty);//replace with empty
                        decimal sinking = decimal.Parse(sinkingFund);
                        selrow.Sinking = sinking;
                    }
                    //-----Update Build Area
                    if (string.IsNullOrEmpty(Request.Form["BuildUp"]))
                    { selrow.BuildUp = null; }
                    else { selrow.BuildUp = decimal.Parse(Request.Form["BuildUp"]); }

                }//end else



                dbModel.Entry(selrow).State = EntityState.Modified;
                dbModel.SaveChanges();


                if (clickedBtn == "Create")
                {
                    return RedirectToAction("displayData");
                }

                ViewBag.DraftId = id;
                vviewModel twoModel = drafteditaction(id);//call method
                ViewBag.draftSuccess = "Draft is saved";
                return View(twoModel);

            }

            //}
            //catch
            //{
            //ViewBag.DraftId = id;
            //vviewModel twoModel = drafteditaction(id);//call method
            //return View(twoModel);
            //}

        }







        #region Edit
        // GET: Listing/Edit/5
        public ActionResult Edit(string id)//id get from  URL 
        {

            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("displayData");
            }
            else
            {
                vviewModel twoModel = drafteditaction(id);
                return View(twoModel);
            }
        }

        // POST: Listing/Edit/5


        [HttpPost]
        [ValidateInput(false)]//require for ckeditor
        public ActionResult Edit(string id, string[] cond_chck, string[] faci_chck, string urgent_chck, L_AttachmentTbl docTbl, string ListingPrice,
                            string descripEditor)
        {

            var docpath = ""; var docName = "";
            string[] guidNameArr = { }; string[] docNameArr = { };
            try
            {
                using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
                {
                    //select the row that wan to be edit
                    var selrow = dbModel.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();//id ==  from URL
                                                                                              //------Update AdsTitle
                                                                                              //no 'if Request.Form[AdsTitle] == null' cuz Ads Title is reuired to enter
                    selrow.AdsTitle = Request.Form["AdsTitle"].ToString();


                    //------Update UnitNo
                    if (Request.Form["UnitNo"] == null)
                    {
                        selrow.UnitNo = Request.Form["UnitNo"].ToString();
                    }

                    //-------Update completiondate date picker
                    var a = Request.Form["completionDate"];
                    if (!string.IsNullOrEmpty(a))
                    {
                        var dt = DateTime.Parse(a);
                        selrow.CompletionDate = dt;
                    }
                    else
                    {
                        selrow.CompletionDate = null;
                    }



                    //-----Update state, area dropdown
                    selrow.State = Request.Form["comboBoxState"];
                    selrow.Area = Request.Form["comboBoxArea"];


                    //-----Update checkbox
                    List<string> chckbx = new List<string>();
                    chckbx = cond_chck.ToList();
                    var num = cond_chck.Count();
                    string temp_var = "";
                    foreach (string item in chckbx)
                    {
                        if (chckbx.IndexOf(item) == num - 1)//count if the file is last file
                        {
                            temp_var += item;// this is the last item, save it without comma
                        }
                        else
                        {
                            temp_var += item + ",";//save this into DB column
                        }
                    }

                    selrow.Condition = temp_var;

                    //-Update urgent listing
                    if(urgent_chck == null)
                    {
                        selrow.UrgentListing = urgent_chck;
                    }

                    if(urgent_chck != null)
                    {
                        selrow.UrgentListing = "Urgent";
                    }
                   

                    //-----Update checkbox
                    List<string> fcchckbx = new List<string>();
                    fcchckbx = faci_chck.ToList();
                    var num1 = faci_chck.Count();
                    string temp_var1 = "";
                    foreach (string item in fcchckbx)
                    {
                        if (fcchckbx.IndexOf(item) == num1 - 1)//count if the file is last file
                        {
                            temp_var1 += item;// this is the last item, save it without comma
                        }
                        else
                        {
                            temp_var1 += item + ",";//save this into DB column
                        }
                    }

                    selrow.Facilities = temp_var1;


                    //-----Update "Photo" column if NEW image is added

                    var existingPhotoStr = selrow.Photo;//get existing photo string (if have)
                    var getphoto = "";

                    getphoto = Session["photopath"].ToString();

                    var newImgPath = getphoto.ToString();//assign to a var

                    if (string.IsNullOrEmpty(existingPhotoStr))//if the Photo column IS null or empty
                    {
                        selrow.Photo = newImgPath;//assign to selected row
                    }
                    // else if DB phot sting exist & newImg string exist
                    else if (!string.IsNullOrEmpty(existingPhotoStr) && !string.IsNullOrEmpty(newImgPath))
                    {
                        selrow.Photo = existingPhotoStr + "," + newImgPath;
                    }

                    //------Update owner Name
                    selrow.OwnerName = Request.Form["ownerNme"];
                    //-----Update owner Contact
                    selrow.OwnerContact = Request.Form["ownerContact"];
                    //-----Update owner Contact
                    selrow.OwnerContact2 = Request.Form["ownerContact2"];
                    //-----Update owner email
                    selrow.OwnerEmail = Request.Form["ownerMail"];
                    //-----Update owner address
                    selrow.Address = Request.Form["address"];
                    //-----Update lsiting type
                    var ListingTypeVar = Request.Form["ListingType"];
                    selrow.ListingType = ListingTypeVar;

                    //-----Update listing price
                    if (ListingTypeVar == "Rent & Sale")
                    {
                        string rentComma = Request.Form["RentPrice"];
                        rentComma = rentComma.Replace(",", string.Empty);//replace with empty
                        decimal rentPriceComma = decimal.Parse(rentComma);
                        selrow.ListingPrice = rentPriceComma;

                        string saleComma = Request.Form["SalePrice"];
                        saleComma = saleComma.Replace(",", string.Empty);//replace with empty
                        decimal salePriceComma = decimal.Parse(saleComma);
                        selrow.ListingSale = salePriceComma;
                    }
                    else
                    {
                        string priceComma = ListingPrice;
                        priceComma = priceComma.Replace(",", string.Empty);//replace with empty
                        decimal price = decimal.Parse(priceComma);
                        selrow.ListingPrice = price;
                    }

                    //-----Update other
                    selrow.Other = Request.Form["Other"];
                    //-----Update tenure
                    selrow.Tenure = Request.Form["tenure"];
                    //-----Update title
                    selrow.Title = Request.Form["title"];
                    //-----Update Remarks
                    selrow.Remarks = Request.Form["Remarks"];
                    
                    //Update Other facilities
                    selrow.OtherFacilities = Request.Form["OtherFacilities"].ToString();
                    //Update Other Condition
                    selrow.OtherCondition = Request.Form["OtherCondition"].ToString();
                    //-----Update description (ckeditor)
                    string descript = descripEditor.ToString();
                    if (!string.IsNullOrEmpty(descript))
                    {
                        selrow.Description = descript;
                    }

                    //Update Added Date
                    selrow.AddedDate = DateTime.Now;
                    var PropertyType = Request.Form["PropertyTypetype"].ToString();//get selectd property
                                                                                   //-----Update propertyType & propertyType2

                    string PropertyType2 = Request.Form["PropertyType2"].ToString();
                    //check selected property
                    if (PropertyType == "Bungalow" || PropertyType == "Semi Detached" ||
                     PropertyType == "Terrace House" || PropertyType == "Shop House")
                    {
                        selrow.PropertyType = PropertyType + "-" + PropertyType2;

                        //------Update Measurement Unit
                        selrow.Measurement_L = Request.Form["LMeasureUnit"];//capture Land Area unit
                                                                            //-----Update Land Area
                        if (!string.IsNullOrEmpty(Request.Form["LandArea"]))
                        {
                            selrow.LandArea = decimal.Parse(Request.Form["LandArea"]);//success decimal.Parse(Request.Form["LandArea"]);//success
                        }
                        else { selrow.LandArea = 0; }
                    }
                    else
                    {
                        selrow.PropertyType = PropertyType;
                    }
                    //-----Add more rows in ATTACHMENT TBL when new doc is added


                    //----Update status
                    selrow.Status = -1;//pending
                    selrow.ApproveBy = "Pending";



                    //check selected property
                    if (PropertyType == "Industrial Land" || PropertyType == "Development Land" || PropertyType == "Agriculture Land")
                    {
                        //------Update Measurement Unit
                        selrow.Measurement_L = Request.Form["LMeasureUnit"];//capture Land Area unit
                        //-----Update Land Area
                        if (!string.IsNullOrEmpty(Request.Form["LandArea"]))
                        {
                            selrow.LandArea = decimal.Parse(Request.Form["LandArea"]);//success decimal.Parse(Request.Form["LandArea"]);//success
                        }
                        else { selrow.LandArea = 0; }

                        string docpathway = ""; string docnameway = "";
                        var abc = Session["docpath"].ToString();
                        //check if session are empty
                        if (!string.IsNullOrEmpty(Session["docpath"] as string) && !string.IsNullOrEmpty(Session["docName"] as string))
                        {
                            docpathway = Session["docpath"].ToString();//doc guid name (with comma)
                            docnameway = Session["docName"].ToString();//doc actual name (with comma)
                        }

                        //if docpath is not empty == document is updated
                        if (!string.IsNullOrEmpty(docpathway))//if session is not null (if new doc is added)
                        {
                            //assign document path and name
                            docpath = docpathway.ToString();//doc guid name (with comma)
                            docName = docnameway.ToString();//doc actual name (with comma)
                            guidNameArr = docpath.Split(',');
                            docNameArr = docName.Split(',');

                            var i = 0;
                            foreach (var guidName in guidNameArr)
                            {
                                docTbl.guidName = guidName;//document guidName
                                docTbl.docName = docNameArr[i];//docuemnt actual name, by accessing the array

                                docTbl.ListingId = id;//assign the listing guid Id (generated in Create())

                                dbModel.L_AttachmentTbl.Add(docTbl);//add attachment
                                dbModel.SaveChanges();
                                i++;
                            }//end guidName foreach
                        }//end if
                        else//else docpath is empty but Land-related property is selected
                        {
                            dbModel.SaveChanges();
                        }
                    }//end check  land related
                    else //if NOT-LAND property is selected
                    {
                        //------Update Measurement Unit
                        selrow.Measurement = Request.Form["BMeasureUnit"];//capture Build Area unit

                        //-----Update bathroom num
                        if (string.IsNullOrEmpty(Request.Form["bathroom"]))
                        { selrow.Bathroom = null; }
                        else { selrow.Bathroom = int.Parse(Request.Form["bathroom"]); }

                        //-----Update room num
                        if (string.IsNullOrEmpty(Request.Form["roomNum"]))
                        { selrow.Room = null; }
                        else { selrow.Room = int.Parse(Request.Form["roomNum"]); }


                        //-----Update carpark num
                        if (string.IsNullOrEmpty(Request.Form["carpark"]))
                        { selrow.Carpark = null; }
                        else { selrow.Carpark = int.Parse(Request.Form["carpark"]); }

                        ////-----Update mantenance
                        string Maintenance = Request.Form["Maintenance"].ToString();

                        if (Maintenance != "")
                        {
                            Maintenance = Maintenance.Replace(",", string.Empty);//replace with empty
                            decimal maintenance = decimal.Parse(Maintenance);
                            selrow.Maintenance = maintenance;
                        }

                        //-----Update sinking fund
                        string sinkingFund = Request.Form["Sinking"].ToString();
                        if (sinkingFund != "")
                        {
                            sinkingFund = sinkingFund.Replace(",", string.Empty);//replace with empty
                            decimal sinking = decimal.Parse(sinkingFund);
                            selrow.Sinking = sinking;
                        }
                        //-----Update Build Area
                        if (string.IsNullOrEmpty(Request.Form["BuildUp"]))
                        { selrow.BuildUp = null; }
                        else { selrow.BuildUp = decimal.Parse(Request.Form["BuildUp"]); }

                    }//end else

                    //-----Update L_ListingTbl row
                    dbModel.Entry(selrow).State = EntityState.Modified;
                    dbModel.SaveChanges();
                }//end using

                //this will return user to the page that they had access from. Eg: User access from page 3, after edit -> return user to page 3
                var pageNum = TempData["currentPg"];//call tempdata, return current page value to displayData
                return RedirectToAction("displayData", new { page = pageNum });
            }
            catch
            {
                return RedirectToAction("Login", "Authenticate");
            }
        }
        #endregion

        public ActionResult displayDetails(string id)
        {

            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                //display using viewmodel
                var db_row = dbModel.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();//retrieve only 1 row (listing)

                //if id has no result
                if (db_row == null)
                {
                    return RedirectToAction("displayData");
                }

                string[] property = { };
                var prop = "";
                var ppt = db_row.PropertyType.Contains('-');
                if (ppt == true)
                {

                    property = db_row.PropertyType.Split('-');
                    prop = property[0];
                }


                if (!string.IsNullOrEmpty(prop))
                {
                    prop = property[0].ToString();
                }




                var matchFile = dbModel.L_AttachmentTbl.Where(x => x.ListingId == id).ToList();//retrieve a multiple row (file)

                ViewBag.gtSinking = Convert.ToDecimal(db_row.Sinking).ToString("#,##0.00");
                ViewBag.gtMaintenance = Convert.ToDecimal(db_row.Maintenance).ToString("#,##0.00");
                ViewBag.gtPrice = Convert.ToDecimal(db_row.ListingPrice).ToString("#,##0.00");

                ViewBag.gtSalePrice = Convert.ToDecimal(db_row.ListingSale).ToString("#,##0.00");
                ViewBag.gtLand = Convert.ToDecimal(db_row.LandArea).ToString("#,##0");
                ViewBag.gtBuildUp = Convert.ToDecimal(db_row.BuildUp).ToString("#,##0");


                //Verify propertyType
                if (db_row.PropertyType == "Industrial Land" || db_row.PropertyType == "Development Land"
                                || db_row.PropertyType == "Agriculture Land")//if this post is author's
                {
                    ViewBag.isLandProp = "";
                    ViewBag.ntLandProp = "display:none;";//hide not-land property data
                    ViewBag.downloadDoc = "";
                }


                else if (prop == "Bungalow" || db_row.PropertyType == "Semi Detached" || db_row.PropertyType == "Terrace House" || db_row.PropertyType == "Shop House")
                {

                    ViewBag.isLandProp = "";
                    ViewBag.ntLandProp = "";

                }

                else
                {
                    ViewBag.isLandProp = "display:none;";//hide land property data
                    ViewBag.downloadDoc = "display:none;";
                    ViewBag.ntLandProp = "";
                }
                var usrid = ""; var isAdmin = "";


                if (!string.IsNullOrEmpty(Session["isLoginID"] as string))
                {
                    usrid = Session["isLoginID"].ToString();


                }


                if (!string.IsNullOrEmpty(Session["isAdmin"] as string))// if isAdmin IS NOT null or empty
                {
                    isAdmin = Session["isAdmin"].ToString();

                }


                //Verify visitor
                if (db_row.Agent == usrid.ToString()) //if this post is author's 
                {
                    ViewBag.isAuthorAdmin = "";
                }
                else if (isAdmin == "1")//if this post is admin
                {
                    ViewBag.isAuthorAdmin = "";
                }
                else//else not admin, not author
                {
                    ViewBag.downloadDoc = "display:none;";
                    ViewBag.isAuthorAdmin = "display:none;";
                }
                //calculate number of file

                //Call View Model -> assign the searched result from DB to ViewModel
                var twoModel = new vviewModel
                {
                    listingTbl = db_row,
                    fileAttach = matchFile
                };
                return View(twoModel);
            }

        }


        // POST: Listing/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection, string deldraft)
        {
            try
            {
                if (!string.IsNullOrEmpty(deldraft))
                {
                    id = deldraft;
                }

                string[] splitPhoto = { };

                using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
                {
                    //delete physical IMAGE
                    //get row that want to be deleted
                    var guidId = dbModel.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();

                    if (guidId.Photo != null)
                    {
                        splitPhoto = guidId.Photo.Split(',');//split "Photo" string
                    }

                    foreach (var imgNme in splitPhoto)
                    {
                        // delete physical IMAGE from folder
                        string filePath = Server.MapPath(Url.Content("~/Image/" + imgNme));
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                    }



                    //delete (multiple row) from L_AttachmentTbl and physical file
                    //get row that want to be deleted
                    var delDoc = dbModel.L_AttachmentTbl.Where(x => x.ListingId == id).ToList();
                    if (delDoc != null)
                    {
                        foreach (var item in delDoc)
                        {
                            //delete physical FILE frm folder
                            string filePath = Server.MapPath(Url.Content("~/Attachment/" + item.guidName));
                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }
                            //remove MULTIPLE file row from tbl with foreach
                            dbModel.L_AttachmentTbl.Remove(item);
                        }//end foreach
                    }


                    //delete the row from ListingTbl
                    dbModel.L_ListingTbl.Remove(guidId);
                    dbModel.SaveChanges();
                }//end using()
                if (!string.IsNullOrEmpty(deldraft))//if deldraft not null == delete is execute from myDraft cshtml
                {
                    return RedirectToAction("myDraft");
                }
                var pageNum = TempData["currentPg"];//call tempdata, return current page value to displayData
                return RedirectToAction("displayData", new { page = pageNum });

            }
            catch
            {
                return RedirectToAction("displayData");
            }
        }



        public ActionResult approve(string SubmitButton)
        {


            Overriding_DBEntities dbModel = new Overriding_DBEntities();

            // Session["loginUsr"] = Request.Cookies["loginUsr"].Value;

            if (!string.IsNullOrEmpty(Session["isLoginID"] as string))
            {
                Session["loginUsr"] = Session["isLoginID"].ToString();
            }

            var currentApplication = dbModel.L_ListingTbl.Find(SubmitButton);
            //change ststaus
            currentApplication.Status = 1;
            //input approve by
            if (!string.IsNullOrEmpty(Session["loginUsr"] as string))
            {
                currentApplication.ApproveBy = Session["loginUsr"].ToString();
            }
            else//if user is null
            {
                currentApplication.ApproveBy = "Unable to detect user.";
            }


            dbModel.SaveChanges();

            //Save Record and Redirect
            var pageNum = TempData["currentPg"];//call tempdata, return current page value to displayData
            return RedirectToAction("displayData", new { page = pageNum });


        }


        public ActionResult t_down(string SubmitButton)
        {
            Overriding_DBEntities dbModel = new Overriding_DBEntities();



            if (!string.IsNullOrEmpty(Session["isLoginID"] as string))
            {
                Session["loginUsr"] = Session["isLoginID"].ToString();
            }
            try
            {
                var currentApplication = dbModel.L_ListingTbl.Find(SubmitButton);
                //change status
                currentApplication.Status = 0;
                //input approve by
                if (!string.IsNullOrEmpty(Session["loginUsr"] as string))
                {
                    currentApplication.ApproveBy = Session["loginUsr"].ToString();
                }
                else//if user is null
                {
                    currentApplication.ApproveBy = "Unable to detect user.";
                }

                dbModel.SaveChanges();


                //Save Record and Redirect
                var pageNum = TempData["currentPg"];
                return RedirectToAction("displayData", new { page = pageNum });
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        string message = string.Format("{0}:{1}",
                      validationErrors.Entry.Entity.ToString(),
                      validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);

                    }
                }
                throw raise;
            }
        }//end public

        //cascading drop down json function 
        //public JsonResult loadArea(string stateName)
        //{
        //     Overriding_DBEntities dbModel = new Overriding_DBEntities();
        //    var stateID = dbModel.L_State_tb.Where(x => x.StateName == stateName)
        //                .Select(x => x.StateID).FirstOrDefault();

        //    return Json(dbModel.L_Area_tb.Where(a => a.StateID == stateID).Select(a => new {
        //        aName = a.AreaName
        //    }).ToList(), JsonRequestBehavior.AllowGet);
        //}


        public ActionResult NoJS()
        {
            return View();
        }

        //delete image (pass from ajax editJS.js)
        public string delImgAction(string imgNme, string ID)
        {

            string[] splitPhoto = { };
            string newImgString = "";
            string html = "";
            string[] splitNewImg = { };
            //update DB "Photo" column
            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                var row = dbModel.L_ListingTbl.Where(x => x.Id == ID).Distinct().FirstOrDefault();
                splitPhoto = row.Photo.Split(',');//split "Photo" string

                //delete physical img frm folder
                string filePath = Server.MapPath(Url.Content("~/Image/" + imgNme));
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                //find the photoname that want to be delete in the array
                for (var i = 0; i < splitPhoto.Length; i++)
                {
                    if (splitPhoto[i] == imgNme)// if found the imgName
                    {
                        splitPhoto = splitPhoto.Where((source, index) => index != i).ToArray();//remove  from array
                    }
                }

                //reform string with comma as new values
                for (var i = 0; i < splitPhoto.Length; i++)
                {
                    if (!string.IsNullOrEmpty(splitPhoto[i]))//if value != "" or null
                    {
                        //detect if last item, assign name without comma
                        if (i == splitPhoto.Length - 1)//count if the string is last strng
                        {
                            newImgString += splitPhoto[i];// this is the last item, save it without comma
                        }
                        else
                        {
                            newImgString += splitPhoto[i] + ",";//save imgname with comma
                        }
                    }//end if
                }//end for

                splitNewImg = newImgString.Split(',');
                //display photo again after delete
                for (var i = 0; i < splitNewImg.Length; i++)
                {
                    if (!string.IsNullOrEmpty(splitNewImg[i]))//if value != "" or null
                    {
                        html += "<div class='col-md-3' style='margin-right: 10px;'>";
                        html += "<img src='" + Url.Content("~/Image/" + splitNewImg[i].ToString()) + "' style='width: 250px; height: 150px;' />";
                        //html += "<p>"+ splitNewImg[i].ToString() + "</p>";
                        html += "<p></p>";
                        html += "<button type='button' value='" + splitNewImg[i].ToString() + "' style='height: 40px; width: 100px;'";
                        html += "class='btn btn-default btn-danger' onclick='delImg(this)'>";
                        html += "Delete </button>";
                        html += "</div>";
                    }
                    else
                    {
                        html = "<input style='display: none;' id='imgNumTxt' type='text' value='0' />";
                    }
                }

                //update new values to DB "Photo" col
                row.Photo = newImgString;
                dbModel.SaveChanges();

            }//end using dbModel
            return html;

        }

        public string delDocAction(string docNme, string ID)
        {
            string html = "";
            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {

                //delete (multiple row) from L_AttachmentTbl and physical file
                //get row that want to be deleted
                var delDoc = dbModel.L_AttachmentTbl.Where(x => x.ListingId == ID).ToList();



                foreach (var item in delDoc)
                {
                    //if found the file that want to be delete
                    if (item.guidName == docNme)
                    {
                        //delete physical FILE frm folder
                        string filePath = Server.MapPath(Url.Content("~/Attachment/" + item.guidName));
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                        //remove MULTIPLE file row from tbl with foreach
                        dbModel.L_AttachmentTbl.Remove(item);
                        dbModel.SaveChanges();

                    }//end if found file
                    else //else, display normal
                    {


                        html += "<div class='col-md-6'> <div class='row'>";
                        html += "<div class='col-md-8'>";
                        if (string.IsNullOrEmpty(item.guidName))  //if item IS NOT null or empty
                        {//assign html with below
                            html += "<p>Unable load file name < br />";
                            html += "{ Err: File exist in the server. Possible database is not store with file name.}";
                            html += "</p>";
                        }
                        //else, assign as below
                        else { html += item.docName.ToString(); }
                        html += "</div>";
                        html += "<div class='col-md-12'> <div style='margin-right:15px;' class='row'>";
                        //if guidName is null/empty
                        if (string.IsNullOrEmpty(item.guidName))
                        {
                            html += "<p> Unable detect this file.Unavaliable for download <br/>"
                                    + "{ Err: File exist in the server. Possiblly the database is not store with file guidName.} </p>";
                        }
                        else
                        {
                            html += "<div class='col-md-6'>"
                                    + "<button type='button' value='" + item.guidName.ToString() + "' style='height: 40px; width: 80px;'"
                                    + "class='btn btn-default btn-danger' onclick='delDoc(this)'>  Delete   </button> </div> ";
                            html += "<div class='col-md-6'>"
                                   + "<a href='" + Url.Action("DownloadFile", "Listing", new { item.guidName }) + "'"
                                     + "class='btn btn-primary' title='View'>Download</a> </div>";
                        }
                        html += "</div> </div>";
                        html += "</div> </div>";

                    }//end else

                    //if there is no more row in delDoc
                    if ((delDoc.Count) - 1 == 0)//using count method
                    {
                        html = "<input id='docNumTxt' style='display: none;' type='text' value='0' />";
                        return html;
                    }
                }//end foreach
            }//end using dbModel

            //redisplay
            return html;
        }



        public FileResult DownloadFile(string guidName)
        {
            return File(Url.Content("~/Attachment/" + guidName), System.Net.Mime.MediaTypeNames.Application.Octet, guidName);
        }


        //----------------------- Reusable Method ------------------------------------------
        //a custom method, use to repopulate dropdown on first load the page
        // Reuse in Create() and [httppost] Create()
        public void populateCreateDropdown()
        {

            List<string> PropertyType = new List<string>();
            PropertyType.Add("Bungalow"); PropertyType.Add("Terrace House");
            PropertyType.Add("Semi Detached"); PropertyType.Add("Apartment");
            PropertyType.Add("Cluster Home"); PropertyType.Add("Factory");
            PropertyType.Add("Industrial Land"); PropertyType.Add("Development Land");
            PropertyType.Add("Agriculture Land"); PropertyType.Add("Shop House");
            PropertyType.Add("Complex Shoplot"); PropertyType.Add("Office Lot");
            PropertyType.Add("Town House"); PropertyType.Add("Others");
            PropertyType.Sort();
            ViewBag.PropertyType = PropertyType;

            List<string> Condition = new List<string>();
            Condition.Add("Hill View"); Condition.Add("Road View"); Condition.Add("Original");
            Condition.Add("Fully Furnished"); Condition.Add("Partial Furnished");
            Condition.Add("Unfurnished"); Condition.Add("Seaview"); Condition.Add("Corner");
            Condition.Add("Partially Renovated"); Condition.Add("Intermediate"); Condition.Add("Fully Renovated");
            Condition.Add("Town View");
            Condition.Sort();
            ViewBag.Condition = Condition;

            List<string> Facilities = db.L_SystemDropDown.Where(x => x.Dropdown_Type == "Facilities").Select(x => x.Dropdown_Value).Distinct().ToList();
            ViewBag.GetFacilities = Facilities;
            List<string> Ltype = new List<string>();
            Ltype.Add("Rent"); Ltype.Add("Sale"); Ltype.Add("Rent & Sale");
            Ltype.Sort();
            ViewBag.Ltype = Ltype;

            List<string> other = new List<string>();
            other.Add("Bumi Lot"); other.Add("Non-Bumi Lot"); other.Add("Malay Reserved");
            other.Sort();
            ViewBag.other = other;

            List<string> measure = new List<string>();
            measure.Add("SF"); measure.Add("Acre");
            ViewBag.measureUnit = measure;

            //Fill data for dropdown Step (1). Step (2) Go to Create.cshtml
            Overriding_DBEntities dbModel = new Overriding_DBEntities();
            //state dropdown 
            ViewBag.DrpDownState = dbModel.L_State_tb.Where(x => !string.IsNullOrEmpty(x.StateName)).Distinct().ToList();
        }

        //return viewmodel to populate selected values when display in View
        //reuse for Draft() and Edit()
        public vviewModel drafteditaction(string id)
        {
            using (Overriding_DBEntities dbModel = new Overriding_DBEntities())
            {
                //------PROPERTY TYPE 1 & 2 : DROPDOWN
                var db_popertyType1 = ""; var db_popertyType2 = "";
                string[] splitProperty = { };//array

                //Retrieve 1 entire row from L_ListingTbl according to Id
                var db_row = dbModel.L_ListingTbl.Where(x => x.Id == id).FirstOrDefault();//retrieve only 1 row


                var matchFile = dbModel.L_AttachmentTbl.Where(x => x.ListingId == id).ToList();//retrieve a multiple row

                //Call View Model -> assign the searched result from DB to ViewModel
                var twoModel = new vviewModel
                {
                    listingTbl = db_row,
                    fileAttach = matchFile
                };
                if (db_row != null)
                {

                    //display listing price
                    ViewBag.gtPrice = Convert.ToDecimal(db_row.ListingPrice).ToString("#,##0.00");
                    //display listing sale price
                    ViewBag.gtSalePrice = Convert.ToDecimal(db_row.ListingSale).ToString("#,##0.00");
                    //displat sale price
                    ViewBag.salePrice = Convert.ToDecimal(db_row.ListingSale).ToString("#,##0.00");
                    //display mantenance
                    ViewBag.gtMaintenance = Convert.ToDecimal(db_row.Maintenance).ToString("#,##0.00");

                    //display sinking fund
                    ViewBag.gtSinking = Convert.ToDecimal(db_row.Sinking).ToString("#,##0.00");

                    //display Build Area
                    ViewBag.gtBuild = db_row.BuildUp;
                    //display completionDate
                    ViewBag.compleDate = db_row.CompletionDate;


                    //Populate propertyType dropdown
                    List<string> PropertyType = new List<string>();
                    PropertyType.Add("Bungalow"); PropertyType.Add("Terrace House");
                    PropertyType.Add("Semi Detached"); PropertyType.Add("Apartment");
                    PropertyType.Add("Cluster Home"); PropertyType.Add("Factory");
                    PropertyType.Add("Industrial Land"); PropertyType.Add("Development Land");
                    PropertyType.Add("Agriculture Land"); PropertyType.Add("Shop House");
                    PropertyType.Add("Complex Shoplot"); PropertyType.Add("Office Lot");
                    PropertyType.Add("Town House"); PropertyType.Add("Others");
                    PropertyType.Sort();

                    //Get the property Type -> Split it. On Split, there will be 2 parts (Refer DB)
                    //First part: Property Name, Second part: Storey

                    splitProperty = db_row.PropertyType.Split('-');//split into array
                    db_popertyType1 = splitProperty[0];//array consisting the First part (property name), assign to var
                    if (splitProperty.Length == 2)// if there are 2 part in array
                    {
                        db_popertyType2 = splitProperty[1];//assign consisting the 2nd part (Storey)
                        ViewBag.PropType2 = db_popertyType2;
                    }
                    else { ViewBag.PropType2 = ""; }
                    //Populate PropertyType dropdown, display selected value from DB
                    ViewBag.PropertyType = new SelectList(PropertyType, db_popertyType1);
                    //-----CONDITION : CHECKBOX    
                    string[] splitCondition = { };//array
                    List<string> db_Condition = new List<string>();
                    List<string> Condition = new List<string>();
                    Condition.Add("Hill View"); Condition.Add("Road View"); Condition.Add("Original");
                    Condition.Add("Fully Furnished"); Condition.Add("Partial Furnished");
                    Condition.Add("Unfurnished"); Condition.Add("Seaview"); Condition.Add("Corner");
                    Condition.Add("Partially Renovated"); Condition.Add("Intermediate"); Condition.Add("Fully Renovated");
                    Condition.Add("Town View");
                    Condition.Sort();
                    //Populate checkbox
                    ViewBag.Condition = Condition;

                    if (db_row.Condition != null)
                    {
                        //get value from DB -> Split it
                        splitCondition = db_row.Condition.Split(',');
                        //assign DB selected condition into a List
                        foreach (var item in splitCondition)
                        {
                            db_Condition.Add(item);
                        }
                    }//end if dbrow-Condition is null


                    //Assign selected checkbox value
                    ViewBag.selCondition = db_Condition;


                    //-----CONDITION : CHECKBOX    
                    string[] splitFacilities = { };//array
                    List<string> db_Facilities = new List<string>();
                    List<string> Facilities = db.L_SystemDropDown.Where(x => x.Dropdown_Type == "Facilities").Select(x => x.Dropdown_Value).Distinct().ToList();

                    //Populate checkbox
                    ViewBag.GetFacilities = Facilities;

                    if (db_row.Facilities != null)
                    {
                        //get value from DB -> Split it
                        splitFacilities = db_row.Facilities.Split(',');
                        //assign DB selected condition into a List
                        foreach (var item in splitFacilities)
                        {
                            db_Facilities.Add(item);
                        }
                    }//end if dbrow-Condition is null


                    //Assign selected checkbox value
                    ViewBag.selFacilities = db_Facilities;



                    //-----LISTING TYPE : DROPDOWN
                    List<string> Ltype = new List<string>();
                    Ltype.Add("Rent"); Ltype.Add("Sale"); Ltype.Add("Rent & Sale");
                    Ltype.Sort();
                    //Get selected ListingType from DB row
                    var db_ListingType = db_row.ListingType;
                    // populate Lsiting Type & assign ListingType from DB
                    ViewBag.Ltype = new SelectList(Ltype, db_ListingType);

                    //-----OTHERS : DROPDOWN
                    List<string> otherDrpdown = new List<string>();
                    otherDrpdown.Add("Bumi Lot"); otherDrpdown.Add("Non-Bumi Lot"); otherDrpdown.Add("Malay Reserved");
                    otherDrpdown.Sort();
                    //Get selected Others from DB
                    var db_others = db_row.Other;
                    // ViewBag.other = other;
                    ViewBag.otherVal = new SelectList(otherDrpdown, db_others);

                    //-----STATE : DROPDOWN 
                    ViewBag.DrpDownState = dbModel.L_State_tb.Where(x => !string.IsNullOrEmpty(x.StateName))
                                                        .Distinct().ToList();
                    ViewBag.dbState = db_row.State;//get State value frm DB
                                                   //-----Get Area from DB
                    ViewBag.dbArea = db_row.Area;

                    //-----Get Tenure (Not Require, it will auto show checked in radio btn form)
                    //-----Get Title from DB

                    //-----Populate measurement dropdwon
                    var db_measureUnit = db_row.Measurement;

                    List<string> measure = new List<string>();
                    measure.Add("SF"); measure.Add("Acre");

                    ViewBag.measureUnit = new SelectList(measure, db_measureUnit);

                    var db_LmeasureUnit = db_row.Measurement_L;

                    List<string> measureL = new List<string>();
                    measureL.Add("SF"); measureL.Add("Acre");

                    ViewBag.measureUnitL = new SelectList(measureL, db_LmeasureUnit);


                }//end if = null

                return twoModel;
            }//end using dbModel
        }

        



    }//end controllrt
}
