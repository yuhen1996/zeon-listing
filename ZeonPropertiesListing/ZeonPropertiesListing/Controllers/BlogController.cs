﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZeonPropertiesListing.Models;
using PagedList;
using System.Data.Entity;

namespace ZeonPropertiesListing.Controllers
{
    [SessionExpire]
    public class BlogController : Controller
    {
        Overriding_DBEntities db = new Overriding_DBEntities();
        // GET: Blog
        #region Display Blog
        public ActionResult DisplayBlog(string searchString, int? page)
        {
            ViewBag.CurrentFilter = searchString;
            var displayBlog = from b in db.L_BlogTbl
                                 select b;
            if (!String.IsNullOrEmpty(searchString))
            {

                displayBlog = displayBlog.Where(b => b.BlogTitle.ToLower().Contains(searchString));
                   
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(displayBlog.OrderByDescending(s => s.CreatedDate).ToPagedList(pageNumber, pageSize));
        }

        #endregion

        #region Delete Blog
        [HttpPost]
        public ActionResult DeleteBlog(int id)
        {
            var blogID = db.L_BlogTbl.Where(x => x.Id == id).FirstOrDefault();

            if (blogID != null)
            {
                if (blogID.Photo != null)
                {
                    var img = blogID.Photo;
                    // delete physical IMAGE from folder
                    string filePath = Server.MapPath(Url.Content("~" + img));
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

                db.L_BlogTbl.Remove(blogID);
                db.SaveChanges();


            }
            

            return RedirectToAction("DisplayBlog");
        }
        #endregion

        #region Edit
        public ActionResult Edit(int? id)//id get from  URL 
        {

            if (id == null)
            {
                return RedirectToAction("DisplayBlog");
            }
            else
            {
                var blog = db.L_BlogTbl.Where(x => x.Id == id).FirstOrDefault();
                return View(blog);
            }
        }


        [HttpPost]
        [ValidateInput(false)]//require for ckeditor
        public ActionResult Edit(L_BlogTbl blg, string descripEditor1, string descripEditor2, string descripEditor3)
        {



            if (ModelState.IsValid)
            {



                blg.CreatedDate = DateTime.Now;
                string descript1 = descripEditor1.ToString();
                if (!string.IsNullOrEmpty(descript1))
                {
                    blg.Description1 = descript1;
                }

                string descript2 = descripEditor2.ToString();
                if (!string.IsNullOrEmpty(descript2))
                {
                    blg.Description2 = descript2;
                }

                string descript3 = descripEditor3.ToString();
                if (!string.IsNullOrEmpty(descript3))
                {
                    blg.Description3 = descript3;
                }
                db.Entry(blg).State = EntityState.Modified;
                db.SaveChanges();

                Session["Photo"] = blg.Photo;



                return RedirectToAction("DisplayBlog");
            }

            return View(blg);

            #endregion
        }
        #region create blog
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]//require for ckeditor
        public ActionResult Index(L_BlogTbl blog)
        {
            L_BlogTbl create = new L_BlogTbl();

            create.BlogTitle = blog.BlogTitle;
            create.Description1 = blog.Description1;
            create.Description2 = blog.Description2;
            create.Description3 = blog.Description3;
            create.PostedBy = blog.PostedBy;
            create.CreatedDate = DateTime.Now;
            create.Photo = blog.Photo;

            db.L_BlogTbl.Add(create);
            db.SaveChanges();
            return View();
        }

        #endregion

        #region Upload Image 
        [HttpPost]
        public string UploadBlogImage()
        {
            string path;
            var insertedById = System.Web.HttpContext.Current.Request.Params["InsertedById"];
            var original = System.Web.HttpContext.Current.Request.Params["Original"];

            //Get the uploaded attachment from the Files collection
            HttpPostedFile httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedAttached"];

            // Precaution in case if no excel files found
            if (httpPostedFile == null)
            {
                var msg = original;

                return msg;
            }

            var fileName = System.IO.Path.GetFileName(httpPostedFile.FileName);
            string strExtensionName = System.IO.Path.GetExtension(fileName);

            //generate random number
            Random generator = new Random();
            long r = generator.Next(0, 999999999);
            string s = r.ToString("D6");
            fileName = s + strExtensionName;

            for (var i = 0; i < 99; i++)
            {
                FileInfo fileInfo = new FileInfo(Path.Combine(Server.MapPath("/BlogImage/"), fileName));
                if (fileInfo.Exists)
                {
                    //generate random number
                    Random generator2 = new Random();
                    long r2 = generator2.Next(0, 999999999);
                    string s2 = r2.ToString("D6");
                    fileName = s2 + strExtensionName;
                }

                else
                {
                    break;
                }
            }

            // Get the complete file path for temp file
            path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("/BlogImage"), fileName);

            // Save the uploaded file as temp file
            httpPostedFile.SaveAs(path);

            return "/BlogImage/" + fileName;
        }
        #endregion

        #region Edit Image
        [HttpPost]
        public string EditImage()
        {

            string path;
            var insertedById = System.Web.HttpContext.Current.Request.Params["InsertedById"];
            var original = System.Web.HttpContext.Current.Request.Params["Original"];

            //Get the uploaded attachment from the Files collection
            HttpPostedFile httpPostedFile = System.Web.HttpContext.Current.Request.Files["UploadedAttached"];

            // Precaution in case if no excel files found
            if (httpPostedFile == null)
            {
                var msg = original;

                return msg;
            }

            var fileName = System.IO.Path.GetFileName(httpPostedFile.FileName);
            string strExtensionName = System.IO.Path.GetExtension(fileName);

            //generate random number
            Random generator = new Random();
            long r = generator.Next(0, 999999999);
            string s = r.ToString("D6");
            fileName = s + strExtensionName;

            for (var i = 0; i < 99; i++)
            {
                FileInfo fileInfo = new FileInfo(Path.Combine(Server.MapPath("/BlogImage/"), fileName));
                if (fileInfo.Exists)
                {
                    //generate random number
                    Random generator2 = new Random();
                    long r2 = generator2.Next(0, 999999999);
                    string s2 = r2.ToString("D6");
                    fileName = s2 + strExtensionName;
                }

                else
                {
                    break;
                }
            }

            // Get the complete file path for temp file
            path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("/BlogImage"), fileName);

            // Save the uploaded file as temp file
            httpPostedFile.SaveAs(path);

            return "/BlogImage/" + fileName;
        }

        #endregion
    }
}