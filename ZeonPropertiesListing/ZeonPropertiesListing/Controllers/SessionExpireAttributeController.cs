﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZeonPropertiesListing.Controllers
{
    /*
         usage
         1. Can put at the start controller so that every action inside the controller will have this session expire atrribute

         [SessionExpire]
         public class LalaController : Controller
         {
             .....
         }

         -OR-

         2. Put the attribute at a specific action only that you want to check if session expire 

         public class LalaController : Controller
         {
             [SessionExpire]
             public ActionResult Index()
             {
                 ...
             }
         } 
     */

    public class SessionExpireAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            if (ctx.Session["isLoginID"] == null)
            {
                if (ctx.Request.Cookies["isAdmin"] != null)
                {
                    ctx.Session["isAdmin"] = ctx.Request.Cookies["isAdmin"].Value;
                }

                if (ctx.Request.Cookies["isLogin"] != null)
                {
                    ctx.Session["isLoginID"] = ctx.Request.Cookies["isLogin"].Value;
                    ctx.Session["usrPosition"] = ctx.Request.Cookies["usrPos"].Value;
                    ctx.Session["loginUsrName"] = ctx.Request.Cookies["loginUsr"].Value;
                    ctx.Session["IdnUsrnme"] = ctx.Request.Cookies["Id_Usrnme"].Value;
                    ctx.Session["usrContact"] = ctx.Request.Cookies["usrContact"].Value;
                    

                }

                if (ctx.Session["isLoginID"] == null && ctx.Session["inactive"] == null)
                {
                    if (filterContext.HttpContext.Request.QueryString.ToString() != null && filterContext.HttpContext.Request.QueryString.ToString() != "")
                    {
                        var path = filterContext.HttpContext.Request.RawUrl;
                        filterContext.Result = new RedirectResult("~/Authenticate/Login?url=" + path);
                        return;
                    }
                    else
                    {
                        var path = filterContext.HttpContext.Request.RawUrl;
                        filterContext.Result = new RedirectResult("~/Authenticate/Login?url=" + path);
                        return;
                        //filterContext.Result = new RedirectResult("~/Home/Index");
                        //return;
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}